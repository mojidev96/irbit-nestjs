import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, Double } from 'typeorm';
import { Users } from '../users/users.entity';

@Entity()
export class Locations extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userId: number;

    @Column({ type: 'double precision', array: true })
    location: Double[]

    @Column()
    dateCreate: Date;

    @ManyToOne(type => Users, user => user.location)
    user: Users;

}