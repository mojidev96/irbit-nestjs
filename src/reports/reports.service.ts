
import { Injectable, NotFoundException, ConflictException } from '@nestjs/common';
import { Users } from '../users/users.entity';
import { ReportDTO } from './reports.dto';
import { Reports } from './reports.entity';
import { getManager } from 'typeorm';

@Injectable()
export class ReportsService {

    async sendReport(user: Users, reportDTO: ReportDTO): Promise<{ message: string }> {

        const reportBefore = await getManager().findOne(Reports, { fromUserId: user.id, contentId: reportDTO.contentId, contentType: reportDTO.contentType })

        if (reportBefore) {
            throw new ConflictException('کاربر گرامی؛ ضمن سپاس از اطلاع رسانی. شما پیش از این نیز این محتوا را گزارش کرده بودید. لطفا برای اطلاعات بیشتر به قسمت آموزش مراجعه نمایید.')
        }

        const newReport = new Reports()
        newReport.contentId = reportDTO.contentId
        newReport.contentType = reportDTO.contentType
        newReport.fromUserId = user.id
        newReport.dateCreate = new Date()
        newReport.reportType = reportDTO.reportType

        await newReport.save()

        var reportPlus = 1
        // not real, fake, lie, scam
        if (newReport.reportType == 2) {
            reportPlus = 10
        }

        // if (newReport.contentType == 1) {
        //     await getManager().update(Products, { id: reportDTO.contentId }, { reportsCount: () => '"reportsCount" + ' + reportPlus })
        // } else if (newReport.contentType == 2) {
        //     await getManager().update(Users, { id: reportDTO.contentId }, { reportsCount: () => '"reportsCount" + ' + reportPlus })
        // } else if (newReport.contentType == 3) {
        //     await getManager().update(Stores, { id: reportDTO.contentId }, { reportsCount: () => '"reportsCount" + ' + reportPlus })
        // }

        return { message: 'با موفقیت ثبت شد و رسیدگی می شود.' }

    }

    async deleteReport(user: Users, reportId: number): Promise<{ message: string }> {

        const reportBefore = await getManager().findOne(Reports, {
            id: reportId, fromUserId: user.id, status: 0
        })

        if (reportBefore) {
            reportBefore.status = 2
            reportBefore.dateRemoved = new Date()
            await reportBefore.save()
            return { message: 'حذف شد.' }
        } else {
            throw new NotFoundException('این گزارش قابل حذف نیست.')
        }
    }

    async getUserReports(user: Users): Promise<Reports[]> {
        const reports = await getManager().createQueryBuilder(Reports, 'r')
            .where('r.fromUserId = ' + user.id + ' and status != 2')
            .getMany()

        return reports
    }

}
