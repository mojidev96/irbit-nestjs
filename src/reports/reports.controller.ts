import { Controller, Post, UseGuards, Body, Delete, Get } from '@nestjs/common';
import { ReportsService } from './reports.service';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/users/getuser.decorator';
import { Users } from '../users/users.entity';
import { ReportDTO } from './reports.dto';
import { Reports } from './reports.entity';

@Controller('reports')
export class ReportsController {

    constructor(private reportService: ReportsService) { }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    sendReport(
        @GetUser() user: Users,
        @Body() reportDTO: ReportDTO
    ): Promise<{ message: string }> {
        return this.reportService.sendReport(user, reportDTO)
    }

    @Delete()
    @UseGuards(AuthGuard('jwt'))
    deleteReport(
        @GetUser() user: Users,
        @Body('reportId') reportId: number
    ): Promise<{ message: string }> {
        return this.reportService.deleteReport(user, reportId)
    }

    // sended from this user
    @Get()
    @UseGuards(AuthGuard('jwt'))
    getUserReports(
        @GetUser() user: Users,
    ): Promise<Reports[]> {
        return this.reportService.getUserReports(user)
    }
}
