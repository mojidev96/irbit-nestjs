import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';

@Entity()
export class Reports extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    fromUserId: number;

    @Column()
    contentId: number;

    @Column()
    dateCreate: Date;

    //by sender
    @Column()
    dateRemoved: Date;

    // 1 = product
    // 2 = profile
    // 3 = store
    @Column()
    contentType: number;

    // 1 = content
    // 2 = not real, lie, scam
    @Column()
    reportType: number;

    // 0 = waiting
    // 1 = answered
    // 2 = remove by owner
    @Column()
    status: number;

    // @ManyToOne(type => Users, user => user.reports)
    // user: Users;

    // @ManyToOne(type => Stores, store => store.reports)
    // store: Stores;

    // @ManyToOne(type => Products, p => p.reports)
    // product: Products;
}