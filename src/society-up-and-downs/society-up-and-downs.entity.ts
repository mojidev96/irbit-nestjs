import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('societyUpAndDowns')
export class SocietyUpAndDowns extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    contentId: number

    @Column()
    userId: number

    @Column('bool')
    isUp: boolean

    // 0 = category
    // 1 = topic
    // 2 = message
    @Column()
    contentType: number

}