import { SocietyMessages } from 'src/society-messages/society-messages.entity';
import { SocietyTopics } from './../society-topics/society-topics.entity';
import { SocietyCategories } from './../society-categories/society-categories.entity';
import { getManager } from 'typeorm';
import { SocietyUpAndDowns } from './society-up-and-downs.entity';
import { Users } from './../users/users.entity';
import { Injectable, NotFoundException, Logger } from '@nestjs/common';

@Injectable()
export class SocietyUpAndDownsService {

    async upOrDown(user: Users, isUp: boolean, isForgive: boolean, contentId: number, contentType: number): Promise<any> {
        const em = getManager()
        var data = null
        if (contentType == 0) {
            data = await em.findOne(SocietyCategories, { id: contentId })
        } else if (contentType == 1) {
            data = await em.findOne(SocietyTopics, { id: contentId })
        } else if (contentType == 2) {
            data = await em.findOne(SocietyMessages, { id: contentId })
        }

        if (!data) {
            throw new NotFoundException('پیدا نشد.')
        }

        const upOrDownBefore = await em.findOne(SocietyUpAndDowns, { userId: user.id, contentId, contentType })
        var isBeforeUp = null


        if (String(isForgive) == 'true') {
            // پاک کن اونی که قبلا زده بودمو
            if (upOrDownBefore) {
                isBeforeUp = upOrDownBefore.isUp
                if (upOrDownBefore.isUp == isUp) {
                    // اگه همونطوری که میگی هست حذفش میکنیم
                    await upOrDownBefore.remove()
                }
            } else {
                throw new NotFoundException('پیدا نشد اینکه قبلا زده باشیش...')
            }
        } else {
            // ثبت مورد جدید چه آپ چه داون
            if (upOrDownBefore) {
                isBeforeUp = upOrDownBefore.isUp
                // اگه قبلا بوده فقط حالتش عوض میشه یعنی آپ بوده میشه داون و بالعکس
                upOrDownBefore.isUp = String(isUp) == 'true'
                await upOrDownBefore.save()
            } else {
                const newUpOrDown = new SocietyUpAndDowns()
                newUpOrDown.userId = user.id
                newUpOrDown.contentId = contentId
                newUpOrDown.isUp = isUp
                newUpOrDown.contentType = contentType
                await newUpOrDown.save()
            }
        }

        // کم و زیاد شدن کل آمارا اینجا انجام میشه
        if (upOrDownBefore && isBeforeUp == true && String(isUp) == 'true' && String(isForgive) == 'true') {
            await em.update(contentType == 0 ? SocietyCategories : contentType == 1 ? SocietyTopics : SocietyMessages, { id: contentId }, { ups: () => 'ups - 1' })
        } else if (upOrDownBefore && isBeforeUp == false && String(isUp) == 'false' && String(isForgive) == 'true') {
            await em.update(contentType == 0 ? SocietyCategories : contentType == 1 ? SocietyTopics : SocietyMessages, { id: contentId }, { downs: () => 'downs - 1' })
        } else if (upOrDownBefore && isBeforeUp == true && String(isUp) == 'false') {
            await em.update(contentType == 0 ? SocietyCategories : contentType == 1 ? SocietyTopics : SocietyMessages, { id: contentId }, { downs: () => 'downs + 1', ups: () => 'ups - 1' })
        } else if (upOrDownBefore && isBeforeUp == false && String(isUp) == 'true') {
            await em.update(contentType == 0 ? SocietyCategories : contentType == 1 ? SocietyTopics : SocietyMessages, { id: contentId }, { downs: () => 'downs - 1', ups: () => 'ups + 1' })
        } else if (!upOrDownBefore && String(isUp) == 'false') {
            await em.update(contentType == 0 ? SocietyCategories : contentType == 1 ? SocietyTopics : SocietyMessages, { id: contentId }, { downs: () => 'downs + 1' })
        } else if (!upOrDownBefore && String(isUp) == 'true') {
            await em.update(contentType == 0 ? SocietyCategories : contentType == 1 ? SocietyTopics : SocietyMessages, { id: contentId }, { ups: () => 'ups + 1' })
        }

        return { message: 'انجام شد.' }
    }

}
