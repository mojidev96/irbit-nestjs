import { Users } from './../users/users.entity';
import { SocietyUpAndDownsService } from './society-up-and-downs.service';
import { AuthGuard } from '@nestjs/passport';
import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { GetUser } from 'src/users/getuser.decorator';

@Controller('societyUpAndDowns')
export class SocietyUpAndDownsController {

    constructor(private upAndDownService: SocietyUpAndDownsService) { }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    upOrDown(
        @GetUser() user: Users,
        @Body('isUp') isUp: boolean,
        @Body('isForgive') isForgive: boolean,
        @Body('contentId') contentId: number,
        @Body('contentType') contentType: number,
    ): Promise<any> {
        return this.upAndDownService.upOrDown(user, isUp, isForgive, contentId, contentType)
    }
}
