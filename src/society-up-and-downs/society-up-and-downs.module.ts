import { UsersModule } from 'src/users/users.module';
import { Module } from '@nestjs/common';
import { SocietyUpAndDownsService } from './society-up-and-downs.service';
import { SocietyUpAndDownsController } from './society-up-and-downs.controller';

@Module({
  imports: [UsersModule],
  providers: [SocietyUpAndDownsService],
  controllers: [SocietyUpAndDownsController]
})
export class SocietyUpAndDownsModule {}
