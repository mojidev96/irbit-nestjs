import { AppService } from './app.service';
import { CoinMessagesService } from './coin-messages/coin-messages.service';

import { BlockListService } from './block-list/block-list.service';

import { listen, Server, Socket } from 'socket.io';
import { MessagesService } from './messages/messages.service';
import { Logger, ConflictException, HttpService } from '@nestjs/common';
import { WebSocketGateway, WebSocketServer, SubscribeMessage, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { UsersService } from './users/users.service';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Coins } from './coins/coins.entity';
import { getManager } from 'typeorm';

@WebSocketGateway()
export class AppGateway implements OnGatewayConnection, OnGatewayDisconnect {

    constructor(private messagesService: MessagesService,
        private usersService: UsersService,
        private coinMessagesService: CoinMessagesService,
        private appService: AppService,
        private http: HttpService,
        private blockService: BlockListService) { }

    @WebSocketServer() public server: Server;

    afterInit(server: Server) {
        this.messagesService.socket = server;
        this.appService.socket = server;
    }

    async handleConnection() {
        Logger.log('connected');
    }

    async handleDisconnect(client: Socket) {
        try {
            await this.usersService.setClientDisconected(client.id)

            const clients = this.server.nsps['/'].adapter.rooms['coin-24681']
            Logger.log('wwwf32:dis: ' + Object.keys(clients).length)
        } catch (error) {
            Logger.log('discounnectError: ' + error)
        }
    }

    @SubscribeMessage('leave-all')
    async leaveAll(client: Socket) {
        client.leaveAll()
    }

    // پشمام کسی که اینو مینویسه بار اولشه که اینو مینویسه :)
    @SubscribeMessage('isTyping')
    async isTyping(client: Socket, data: any) {
        client.broadcast.to(data.toUserId.toString()).emit('isTyping', { "toUserId": data.toUserId, "fromUserId": data.fromUserId });
    }

    // این مال صفحه ویرایش پروفایل هست
    // وقتیکه میخواد یوزرنیم جدید انتخاب کنه
    // این چک میکنه که تکراری نباشه
    @SubscribeMessage('check-username')
    async checkUsername(client: Socket, data: any) {
        const isAvailable: boolean = await this.usersService.usernameIsAvailable(data.username)
        Logger.log('wefwe ' + isAvailable)
        this.server.to(client.id).emit('check-username', { "isAvailable": isAvailable });
    }

    // با جوین شدن به چنلی که ایدیت هست این امکان فراهم میشه
    // که هرکس هرچی میخواد برات بفرسته ما برای اینکه امکان شنود
    // به کسی ندیم با توکن یوزر آیدی چک میکنیم
    @SubscribeMessage('join')
    async joinChat(client: Socket, data: any) {
        const isValid = await this.usersService.isUserValidUserId(data.userId, data.token)
        if (isValid) {
            client.join(data.userId.toString()) //.setMaxListeners(1)
            await this.usersService.setClientIdToUser(data.userId, client.id)
            Logger.log('client ' + client.id + ' join to : ' + data.userId)
        } else {
            this.server.to(client.id).emit('join', { "needLogin": true })
            Logger.log('client ' + client.id + ' couldnt join to : ' + data.userId)
        }
    }

    // هرکس جوین یه چنل میشه برا خودش که توش میگه انلاین هست یا نیست
    // بعد هرکس بخواد بدونه که اون هست یا نیست میتونه به اون کانالش جوین شه
    // البته خبرو فقط کسی میتونه تو گروه بفرسته که مالکش باشه
    // یعنی ایدی و توکنشو بفرسته تا ما به بقیه بگیم که انلاین هست یا نه
    @SubscribeMessage('online-users')
    async onlineUsers(client: Socket, data: any) {
        const date = new Date()
        const canShowLastOnline = await
            this.usersService.changeOnlineStatement(data.userId, data.token, data.isOnline, date)
        try {
            if (data.isOnline) {
                // this.onlineUsersList.push({ clientId: client.id, userId: data.userId })
            } else {
                // delete this.onlineUsersList[this.onlineUsersList.findIndex(item => item.userId == data.userId)];
            }
        } catch (error) {
            Logger.log('onlineError: ' + error)
        }
        client.broadcast.to(data.userId.toString() + '-online')
            .emit('online-users',
                { "userId": data.userId, "isOnline": data.isOnline, "dateLastOnline": (canShowLastOnline ? date : null) })
        Logger.log('work fine')
    }

    // این شماره تو شبکه ما هست یا خیر
    @SubscribeMessage('check-in-com')
    async isInCommunity(client: Socket, data: any) {
        const callback = await this.usersService.checkPhoneIsInCommunity(data.phone)
        this.server.to(client.id).emit('check-in-com', { phone: data.phone, id: callback.userId == null ? 0 : callback.userId, avatar: callback.avatar, isOnline: callback.isOnline });
    }

    //تو پایینی من جوین میشم به چنل کسی که انلاین بودنشو توش میگه
    // ضمن جوین اخرین وضعیت طرف هم برای من ارسال میشه
    @SubscribeMessage('join-online-users')
    async joinToKnowAboutWhoIsOnline(client: Socket, data: any) {
        client.join(data.userId.toString() + '-online')
        // اگر این ترو باشه یعنی همین الان بهم بگو وضعیتش چیه اگر هم نباشه نمیده!
        if (data.getBackResult) {
            const result = await
                this.usersService.getLatestOnlineStatementOfUser(data.userId)
            this.server.to(client.id)
                .emit('online-users',
                    { "userId": data.userId, "isOnline": result.isOnline, "dateLastOnline": (result.canShowingLastOnline ? result.dateLastOnline : null) })
        }
    }

    @SubscribeMessage('messages')
    async onChat(client: Socket, message: any) {
        const mes = await this.messagesService.sendMessage(message)
        delete message.senderToken
        message.id = mes.id
        message.chatId = mes.chatId
        client.broadcast.to(message.getterUserId.toString()).emit('messages', message);
        Logger.log('efwef3:: ' + message.getterUserId.toString())
        // یدونه هم برای خودش میفرستیم تا بدونه ارسال پیامش انجام شده
        this.server.to(client.id).emit('messages', message);
    }

    @SubscribeMessage('seen')
    async setSeen(client: Socket, message: any) {
        // این میگه سین شد پیامی که گرفتم تو دیتابیسم بروز میشه
        await this.messagesService.setSeenToMessage(message.id)
        client.broadcast.to(message.getterUserId.toString()).emit('seen', { isSeen: true, messageId: message.id, getterUserId: message.getterUserId });
    }

    @SubscribeMessage('edit-message')
    async editMessage(client: Socket, data: any) {
        const mes = await this.messagesService.ediMessages(data.token, data.messageId, data.newText)
        // تاریخ اولین پیام رو هم برمیگردونیم که تو لیستی وقتی ایدیش صفر شده بتونه متوجه اعمال شدنش تو سرور بشه
        // و از حالت لودینگ پیامو خارج کنه
        const callback = { messageId: mes.id, fromUserId: mes.senderUserId, toUserId: mes.getterUserId, newText: mes.message, isEdited: true, dateCreate: mes.dateCreate }
        this.server.to(client.id).emit('edit-message', callback);
        client.broadcast.to(mes.getterUserId.toString()).emit('edit-message', callback);
    }

    @SubscribeMessage('deleteMessage')
    async deleteMessage(client: Socket, data: any) {
        const friendIds = await this.messagesService.deleteMessage(data.token, data.messageId)
        client.broadcast.to(friendIds.friendOfSender.toString()).emit('deleteMessage',
            { messageId: data.messageId, friendId: friendIds.friendOfGetter, status: friendIds.status });
        this.server.to(client.id).emit('deleteMessage',
            { messageId: data.messageId, friendId: friendIds.friendOfSender, status: friendIds.status });
    }

    @SubscribeMessage('forwardMessage')
    async forwardMessage(client: Socket, data: any) {
        const mes = await this.messagesService.forwardMessage(data.token, data.forwardMessageId, data.toUserId, data.dateCreate)
        mes.dateCreate = data.dateCreate
        delete mes.dateSeen
        delete mes.dateRemoved
        client.broadcast.to(data.toUserId.toString()).emit('messages', mes);
        // یدونه هم برای خودش میفرستیم تا بدونه ارسال پیامش انجام شده
        this.server.to(client.id).emit('messages', mes);
    }

    @SubscribeMessage('blockUnblock')
    async blockUnblock(client: Socket, data: any) {
        const backData = await this.blockService.blockUnblockUser(data.token, data.blockUserId)
        const message = { isBlocked: backData.isBlocked, fromUserId: backData.fromUserId, toUserId: data.blockUserId }
        Logger.log('gewrgw:: ' + message.isBlocked + ' ---- ' + message.fromUserId + ' -- ' + message.toUserId)
        client.broadcast.to(data.blockUserId.toString()).emit('blockUnblock', message);
        this.server.to(client.id).emit('blockUnblock', message);
    }

    // وقتی میخواد یه پیامی رو فوروارد کنه به یکی اسمشو که تایپ کنه میاد اینجا ما براش تو یوزرنیم و نام میگردیم
    @SubscribeMessage('search-forward-users')
    async searchUsersToForward(client: Socket, data: any) {
        const users = await this.messagesService.getUsersToForwardForward(null, data.word)
        this.server.to(client.id).emit('search-forward-users', users);
    }

    //-------------------------coins room::::::::::::::::::::::::::::::::::::::::::::::::
    @SubscribeMessage('joinPrices')
    async joinPrices(client: Socket, data: any) {
        client.join('coin-p-' + data.coinId)
    }

    @SubscribeMessage('joinCoin')
    async joinCoin(client: Socket, data: any) {
        client.join('coin-' + data.coinId)
        const callback = { coinId: data.coinId, onlineCount: this.server.nsps['/'].adapter.rooms['coin-' + data.coinId].length }
        client.broadcast.to('coin-' + data.coinId).emit('coinMessages', callback)
        this.server.to(client.id).emit('coinMessages', callback);
    }

    @SubscribeMessage('leaveCoin')
    async leaveCoin(client: Socket, data: any) {
        client.leave('coin-' + data.coinId)
        const callback = { coinId: data.coinId, onlineCount: this.server.nsps['/'].adapter.rooms['coin-' + data.coinId].length }
        client.broadcast.to('coin-' + data.coinId).emit('coinMessages', callback)
        this.server.to(client.id).emit('coinMessages', callback);
    }

    @SubscribeMessage('isTypingCoin')
    async isTypingCoin(client: Socket, data: any) {
        client.broadcast.to('coin-' + data.coinId).emit('isTypingCoin', { "coinId": data.coinId, "writerName": data.writerName });
    }

    @SubscribeMessage('coinMessages')
    async coinMessages(client: Socket, message: any) {
        const mes = await this.coinMessagesService.sendMessage(message)
        delete message.senderToken
        message.id = mes.id
        client.broadcast.to('coin-' + mes.coinId).emit('coinMessages', message);
        this.server.to(client.id).emit('coinMessages', message);
    }

    @SubscribeMessage('deleteCoinMessage')
    async deleteCoinMessage(client: Socket, data: any) {
        const coinId = await this.coinMessagesService.deleteMessage(data.token, data.messageId)
        client.broadcast.to('coin-' + coinId).emit('deleteCoinMessage',
            { messageId: data.messageId, coinId });
        this.server.to(client.id).emit('deleteCoinMessage',
            { messageId: data.messageId, coinId });
    }

    @SubscribeMessage('editCoinMessage')
    async editCoinMessage(client: Socket, data: any) {
        const mes = await this.coinMessagesService.ediMessages(data.token, data.messageId, data.newText)
        const callback = { messageId: mes.id, fromUserId: mes.senderUserId, toCoinId: mes.coinId, newText: mes.message, isEdited: true, dateCreate: mes.dateCreate }
        this.server.to(client.id).emit('editCoinMessage', callback);
        client.broadcast.to('coin-' + mes.coinId.toString()).emit('edit-message', callback);
    }

    async updatePrices(coinId: string, price: number, oneDayChange: number): Promise<boolean> {
        try {
            // Logger.log('wegwegw: ' + coinId)
            const listenersCount = this.server.nsps['/'].adapter.rooms['coin-p-' + coinId].length
            if (listenersCount > 0) {
                // Logger.log('egwegwe:: ' + coinId)
                this.server.emit('coinMessages', { coinId, price, oneDayChange }).to('coin-p-' + coinId)
            }
        } catch (e) { }
        return true
    }

    @Cron(CronExpression.EVERY_5_SECONDS)
    async updateMarkets() {
        try {
            const url = 'https://api.nomics.com/v1/currencies/ticker?key=52b81a662e5ba27f1dc60ebf89a6ee51&interval=1d,7d,30d&convert=USD&per-page=200&page=1'
            this.http.get(encodeURI(url)).subscribe(async (res) => {
                if (res.status && res.status == 200 && res.data.length > 0) {
                    for await (const data of res.data) {
                        const coinBefore = await getManager().findOne(Coins, { name: data.name })
                        const infoJSON = '{' + (data['1d'] ? ('"1d": ' + (JSON.stringify(data['1d']))) : '') +
                            (data['7d'] ? ((data['1d'] ? ',' : '') + '"7d": ' + (JSON.stringify(data['7d']))) : '') +
                            (data['30d'] ? (((data['7d'] || data['1d']) ? ',' : '') + '"30d": ' + (JSON.stringify(data['30d']))) : '') + '}';
                        if (coinBefore) {
                            const oneDay = data['1d'] ? data['1d']['price_change'] : 0
                            const pri = data.price
                            const id = coinBefore.id.toString()
                            const lastPrice = coinBefore.price
                            coinBefore.info = JSON.parse(infoJSON)
                            coinBefore.name = data.name
                            coinBefore.symbol = data.symbol
                            coinBefore.price = data.price
                            coinBefore.logoUrl = data.logo_url

                            coinBefore.high = data.high
                            coinBefore.marketCap = data.market_cap
                            coinBefore.highTimestamp = data.high_timestamp

                            coinBefore.priceDate = data['price_date']
                            coinBefore.oneDayChange = oneDay
                            await coinBefore.save()
                            if(lastPrice != data.price){
                                await this.updatePrices(id, pri, oneDay)
                            }
                        } else {
                            const newCoin = new Coins()
                            newCoin.info = JSON.parse(infoJSON)
                            newCoin.name = data.name
                            newCoin.logoUrl = data.logo_url
                            newCoin.symbol = data.symbol
                            newCoin.price = data.price
                            newCoin.priceDate = data['price_date']
                            newCoin.oneDayChange = data['1d'] ? data['1d']['price_change'] : 0
                            await newCoin.save()
                        }

                    }
                }
            });
        } catch (error) { Logger.error('nomics error:: ' + error) }
    }
    //-------------------------end of coins room::::::::::::::::::::::::::::::::::::::::::::::::

}