import { Chats } from './../chats/chats.entity';
import { Controller, Post, UseGuards, UseInterceptors, Body, Logger, UploadedFiles, Get, Delete, UnauthorizedException } from '@nestjs/common';
import { MessagesService } from './messages.service';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/users/getuser.decorator';
import { Users } from '../users/users.entity';
import { FilesInterceptor, FileInterceptor, FileFieldsInterceptor } from '@nestjs/platform-express';
import { Messages } from './messages.entity';
import { SendMessageDTO } from './messages.dto';
import { diskStorage } from 'multer'

@Controller('messages')
export class MessagesController {

    constructor(private messageService: MessagesService) { }

    // با توجه به اینکه پیاما تو گیت میاد اینجا فقط اگه فایلی باشه آپلود
    // میکنیم بعد آدرساشو برمیگردونیم تا بزاره تو پیامی که میخواد بفرصته واسه رفیقش
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'image', maxCount: 1 },
        { name: 'file', maxCount: 1 },
        { name: 'voice', maxCount: 1 },
    ], { storage: diskStorage({ destination: './messagesFiles', limits: { fieldSize: 25600000 } }) }))
    async uploadMessageFiles(
        @GetUser() user: Users,
        @UploadedFiles() files): Promise<any> {
        if (!user) {
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.');
        }
        var imagePath: string = ''
        var voicePath: string = ''
        var filePath: string = ''
        var file_filename: string = ''
        var file_fileSize: number = 0

        if (files && files.image && files.image.length > 0) {
            imagePath = files.image[0].path
        }
        if (files && files.voice && files.voice.length > 0) {
            voicePath = files.voice[0].path
            // درست کار نکرد
            // var mp3Duration = await require('mp3-duration');
            // await mp3Duration(voicePath, function (err, duration) {
            //     if (err) return console.log(err.message);
            //     Logger.log('oooii:: ' + duration)
            //     voiceDuration = duration.toFixed(0)
            // });
        }
        if (files && files.file && files.file.length > 0) {
            filePath = files.file[0].path
            file_filename = files.file[0].originalname
            file_fileSize = files.file[0].size
        }

        Logger.log('sfasdas :: ' + voicePath)

        return { imagePath, voicePath, filePath, 'filename': file_filename, 'filesize': file_fileSize }

    }

    @Post('forwardUsers')
    @UseGuards(AuthGuard('jwt'))
    getUsersToForward(@GetUser() user: Users,
        @Body('nameOrUsername') nameOrUsername: string,
    ): Promise<Users[]> {
        return this.messageService.getUsersToForwardForward(user, nameOrUsername ?? '')
    }

    @Post('friendInfo')
    getFriendInfo(@Body('friendId') friendId: number
    ): Promise<any> {
        return this.messageService.getFriendInfo(friendId)
    }

    @Post('conversation')
    @UseGuards(AuthGuard('jwt'))
    getConversationMessages(@GetUser() user: Users,
        @Body('chatId') chatId: number,
        @Body('byUserId') byUserId: number,
        @Body('page') page: number): Promise<{ blockStatus: number, messages: Messages[] }> {
        return this.messageService.getConversationMessages(user, chatId, byUserId, page)
    }

    // یه وقتی هست که کاربر از اپ میاد بیرون طوریکه دوباره میتونه برگرده تو صفحه
    // ما اون تایم نمیتونم پیام هاش رو بروز کنیم پس با تابع پایینی وقتی برگشت و درواقع انلاین
    // شد تو اون صفحه چک میکنیم از طرف مقابلش هر چند تا پیام جدید که گرفته بود میگیریم و بهش نشون میدیم
    @Post('getNewMessages')
    @UseGuards(AuthGuard('jwt'))
    getNewMessages(@GetUser() user: Users,
        @Body('chatId') chatId: number,
        @Body('byUserId') byUserId: number
    ): Promise<{ blockStatus: number, messages: Messages[] }> {
        return this.messageService.getNewMessages(user, chatId, byUserId)
    }

    @Post('getById')
    @UseGuards(AuthGuard('jwt'))
    getMessageById(@GetUser() user: Users,
        @Body('messageId') messageId: number,
    ): Promise<Messages> {
        return this.messageService.getMessageById(user, messageId)
    }

    @Post('chats')
    @UseGuards(AuthGuard('jwt'))
    getChats(@GetUser() user: Users,
        @Body('page') page: number): Promise<Chats[]> {
        return this.messageService.getChats(user, page)
    }

    // @Delete('delete')
    // @UseGuards(AuthGuard('jwt'))
    // deleteMessage(@GetUser() user: Users,
    //     @Body('messageId') messageId: number): Promise<{ message: string }> {
    //     return this.messageService.deleteMessage(user, messageId)
    // }

    @Delete('deleteChat')
    @UseGuards(AuthGuard('jwt'))
    deleteChat(@GetUser() user: Users,
        @Body('friendId') friendId: number): Promise<{ message: string }> {
        return this.messageService.deleteChat(user, friendId)
    }

    // @Post('block')
    // @UseGuards(AuthGuard('jwt'))
    // blockUser(@GetUser() user: Users,
    //     @Body('page') page: number): Promise<Messages[]> {
    //     return this.messageService.getChats(user, page)
    // }
}
