import { Chats } from './../chats/chats.entity';
import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne } from 'typeorm';
import { Users } from '../users/users.entity';

@Entity()
export class Messages extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    chatId: number;

    @Column()
    senderUserId: number;

    @Column()
    getterUserId: number;

    @Column()
    message: string

    @Column()
    dateCreate: Date;

    @Column()
    dateSeen: Date;

    @Column()
    isForward: boolean;

    @Column()
    forwardSenderId: number;

    @Column()
    forwardMessageId: number;

    @Column()
    forwardSenderName: string;

    @Column()
    dateRemoved: Date;

    @Column('bool')
    isSeen: boolean;

    @Column('bool')
    isReply: boolean;

    @Column()
    filePath: string;

    @Column()
    filename: string;

    @Column()
    fileSize: number;

    @Column()
    imagePath: string;

    @Column()
    voicePath: string;

    @Column()
    replyMessage: string;

    @Column()
    isEdited: boolean;

    @Column()
    dateLastEdit: Date;

    @Column()
    voiceDuration: number;
    // 1 = available
    // 2 = deleted
    // 0 = banned and not showing
    // 3 = showing just for sender
    // 4 = یکی از طرفا پیاماشو کلا پاک کرده و حالا فقط طرف مقابلیش میتونه ببینه این الگوریتمش تو گرفن چت و حذف گفتگو استفاده شده
    @Column()
    status: number;

    // which is the message's reply | group
    @Column()
    replyMessageId: number;

    @ManyToOne(type => Users)
    senderUser: Users

    @ManyToOne(type => Users)
    getterUser: Users

    @ManyToOne(type => Chats, chat => chat.messages)
    chat: Chats

}