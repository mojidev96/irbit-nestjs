export class SendMessageDTO {
    getterUserId: number
    senderUserId: number
    senderToken: string
    message: string
    replyMessageId?: number
    replyMessage?: string
    imagePath?: string
    voicePath?: string
    voiceDuration?: number
    filePath?: string
    filename?: string
    fileSize?: number
    dateCreate: string
}

export class UserIds {
    socketId: string
    userId: number
}