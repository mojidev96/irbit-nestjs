import { WebSocketGateway, WebSocketServer, ConnectedSocket } from '@nestjs/websockets';
import { Socket } from 'socket.io';
import { UsersModule } from './../users/users.module';
import { AppGateway } from '../app.gateway';
import { Module } from '@nestjs/common';
import { MessagesService } from './messages.service';
import { MessagesController } from './messages.controller';

@Module({
  imports: [UsersModule,],
  providers: [MessagesService],
  controllers: [MessagesController],
  // exports: [MessagesService]
})
export class MessagesModule { }
