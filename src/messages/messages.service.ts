import { Tokens } from './../tokens/tokens.entity';
import { firebaseServerKey } from './../app.module';
import { Chats } from './../chats/chats.entity';
import { Injectable, ConflictException, UnauthorizedException, Logger, NotFoundException, HttpService } from '@nestjs/common';
import { Users } from '../users/users.entity';
import { SendMessageDTO } from './messages.dto';
import { Messages } from './messages.entity';
import { getManager } from 'typeorm';
import { BlockList } from '../block-list/block-list.entity';
import { Socket, Server } from 'socket.io';
import axios from 'axios'
import { UsersRepository } from 'src/users/users.repository';
@Injectable()
export class MessagesService {

    constructor(private http: HttpService) { }

    public socket: Server = null;

    async sendMessage(
        messageDTO: SendMessageDTO,): Promise<any> {

        const em = getManager()
        // Logger.log('jhg hghk::::: ' + messageDTO.senderToken)
        const isValidUser = await em.getCustomRepository(UsersRepository).getUserByToken(messageDTO.senderToken)// await getManager().findOne(Tokens, { uid: messageDTO.senderUserId, token: messageDTO.senderToken })
        if (!isValidUser) {
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.')
        }

        const sender = await em.findOne(Users, { id: messageDTO.senderUserId })

        const isSenderBlock = await em
            .createQueryBuilder(BlockList, 'b')
            .where('(b.userId = ' + messageDTO.senderUserId + ' and b.blockUserId = ' + messageDTO.getterUserId + ') or (b.blockUserId = ' + messageDTO.senderUserId + ' and b.userId = ' + messageDTO.getterUserId + ')')
            .getOne()
        if (isSenderBlock) {
            throw new ConflictException('مشکل بلاک دارید.')
        }

        var chatId = 0

        const chatBefore = await em.createQueryBuilder(Chats, 'c')
            .where('((c.senderUserId = ' + messageDTO.senderUserId + ' and c.getterUserId = ' + messageDTO.getterUserId + ') or (c.senderUserId = ' + messageDTO.getterUserId + ' and c.getterUserId = ' + messageDTO.senderUserId + '))')
            .getOne()

        if (sender.dateBlockReleased > new Date() && !chatBefore) {
            throw new ConflictException()
        }

        const newM = new Messages()

        const dateCreate = new Date(messageDTO.dateCreate)
        newM.dateCreate = dateCreate
        if (!chatBefore) {
            const newChat = new Chats()
            newChat.senderUserId = messageDTO.senderUserId
            newChat.getterUserId = messageDTO.getterUserId
            newChat.dateCreate = dateCreate
            await newChat.save()
            chatId = newChat.id
        } else {
            chatId = chatBefore.id
        }

        newM.chatId = chatId
        newM.message = messageDTO.message
        newM.senderUserId = messageDTO.senderUserId
        newM.getterUserId = messageDTO.getterUserId
        if (messageDTO.imagePath && messageDTO.imagePath.length > 0) {
            newM.imagePath = messageDTO.imagePath
        }
        if (messageDTO.voicePath && messageDTO.voicePath.length > 0) {
            newM.voicePath = messageDTO.voicePath
            newM.voiceDuration = messageDTO.voiceDuration
        }
        if (messageDTO.filePath && messageDTO.filePath.length > 0) {
            newM.filePath = messageDTO.filePath
            newM.filename = messageDTO.filename
            newM.fileSize = messageDTO.fileSize
        }
        if (messageDTO.replyMessageId && messageDTO.replyMessageId != 0) {
            newM.isReply = true
            newM.replyMessageId = messageDTO.replyMessageId
            newM.replyMessage = messageDTO.replyMessage
        }

        await newM.save()

        const getter = await em.createQueryBuilder(Users, 'u')
            .select('u.fcmToken', 'token')
            .addSelect('u.webFCMToken', 'webFCMToken')
            .addSelect('u.isHideNotifContent', 'isHide')
            .where('u.id = ' + messageDTO.getterUserId)
            .getRawOne()

        Logger.log('erger:: ' + firebaseServerKey)

        const notifTitle = (String(getter.isHide) == 'true') ? 'یک پیام جدید دارید.' : sender.name
        const notifBody = (String(getter.isHide) == 'true') ? 'برای مشاهده کلیک کنید.' : newM.message
        axios.post('https://fcm.googleapis.com/fcm/send', {
            'notification': {
                'body': notifBody,
                'title': notifTitle
            },
            'priority': 'high', 'data': {
                'click_action': 'FLUTTER_NOTIFICATION_CLICK',
                'type': 'message',
                'fromId': sender.id
            },
            'to': getter.token
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': firebaseServerKey,
            }
        }).catch(error => {
            Logger.error('fcm:: ' + error)
        })

        axios.post('https://fcm.googleapis.com/fcm/send', {
            'notification': {
                'body': notifBody,
                'title': notifTitle
            },
            'priority': 'high', 'data': {
                'click_action': 'FLUTTER_NOTIFICATION_CLICK',
                'type': 'message',
                'fromId': sender.id
            },
            'to': getter.webFCMToken
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': firebaseServerKey,
            }
        }).catch(error => {
            Logger.error('fcm:: ' + error)
        })

        return newM
    }

    async getFriendInfo(friendId: number): Promise<any> {
        const data = await getManager().createQueryBuilder(Users, 'u')
            .select('u.name', 'name')
            .addSelect('u.avatar', 'avatar')
            .addSelect('case when u.isShowingLastOnline then u.isOnline else null end as "isOnline"')
            .addSelect('case when u.isShowingLastOnline then u.dateLastOnline else null end as "dateLastOnline"')
            .where('u.id = ' + friendId)
            .getRawOne()
        return data
    }

    async forwardMessage(token: string, forwardMessageId: number, toUserId: number, dateCreate: string): Promise<Messages> {
        const em = getManager()
        const user = await em.getCustomRepository(UsersRepository).getUserByToken(token) //em .findOne(Users, { token: token })
        if (user) {
            const messageToForward = await em.findOne(Messages, { id: forwardMessageId })
            if (messageToForward) {
                const mainSenderUser = await em.findOne(Users, { id: messageToForward.senderUserId })
                if (mainSenderUser) {
                    const dateCreateToDate = new Date(dateCreate)

                    messageToForward.isForward = true
                    messageToForward.forwardMessageId = messageToForward.id
                    messageToForward.forwardSenderId = messageToForward.senderUserId
                    messageToForward.forwardSenderName = mainSenderUser.name

                    messageToForward.senderUserId = user.id
                    messageToForward.getterUserId = toUserId
                    messageToForward.dateCreate = dateCreateToDate

                    const newForwardMessage = messageToForward
                    newForwardMessage.id = null
                    newForwardMessage.isSeen = false

                    var chatId = 0
                    const chatBefore = await getManager().findOne(Chats, {
                        where: [
                            { senderUserId: user.id, getterUserId: toUserId },
                            { senderUserId: toUserId, getterUserId: user.id }
                        ]
                    })
                    if (!chatBefore) {
                        const newChat = new Chats()
                        newChat.senderUserId = user.id
                        newChat.getterUserId = toUserId
                        newChat.dateCreate = dateCreateToDate
                        await newChat.save()
                        chatId = newChat.id
                    } else {
                        chatId = chatBefore.id
                    }

                    newForwardMessage.chatId = chatId

                    await newForwardMessage.save()
                    return newForwardMessage
                } else {
                    throw new NotFoundException()
                }
            } else {
                throw new NotFoundException()
            }
        } else {
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.')
        }
    }

    async getMessageById(user: Users, messageId: number): Promise<Messages> {
        const mes = await getManager().findOne(Messages, {
            where: [
                { id: messageId, getterUserId: user.id },
                { id: messageId, senderUserId: user.id }
            ]
        })
        return mes
    }

    async setSeenToMessage(messageId: number): Promise<boolean> {
        await getManager().update(Messages, { id: messageId }, { isSeen: true })
        return true;
    }

    //blockStatus: 1=none block, 2=block by user.id, 3=block by friend
    async getConversationMessages(user: Users, chatId: number, byUserId: number, page: number): Promise<any> {

        // اگه از صفحه چت پیاما رو نمیخوای صفر بده
        // اینجا یا یه چت میسازیم یا قبلیشو میدیم
        if (chatId == 0) {
            const chatBefore = await getManager().findOne(Chats, {
                where: [
                    { senderUserId: user.id, getterUserId: byUserId },
                    { senderUserId: byUserId, getterUserId: user.id }
                ]
            })
            // اگه زیاد بلاک شده به بقیه کاربرا گند نزنه
            if (user.dateBlockReleased > new Date() && !chatBefore) {
                Logger.log('weww')
                return { dateBlockReleased: user.dateBlockReleased, blockCountInMonth: user.blockCountInMonth }
            }
            if (!chatBefore) {
                const newChat = new Chats()
                newChat.senderUserId = user.id
                newChat.getterUserId = byUserId
                newChat.dateCreate = new Date()
                await newChat.save()
                chatId = newChat.id
            } else {
                chatId = chatBefore.id
            }
        }

        const em = getManager()
        var blockStatus = 1

        const isBlockedByMe = await em
            .createQueryBuilder(BlockList, 'b')
            .where('b.userId = ' + user.id + ' and b.blockUserId = ' + byUserId)
            .getOne()

        const isBlockedByFriend = await em
            .createQueryBuilder(BlockList, 'b')
            .where('b.userId = ' + byUserId + ' and b.blockUserId = ' + user.id)
            .getOne()

        if (isBlockedByMe) {
            blockStatus = 2
        }
        if (isBlockedByFriend) {
            blockStatus = 3
        }

        const messages = await em.createQueryBuilder(Messages, 'm')
            .select('m.id', 'id')
            .addSelect('m.senderUserId', 'senderUserId')
            .addSelect('m.getterUserId', 'getterUserId')
            .addSelect('m.message', 'message')
            .addSelect('m.dateCreate', 'dateCreate')
            .addSelect('m.isSeen', 'isSeen')
            .addSelect('m.isReply', 'isReply')
            .addSelect('m.filePath', 'filePath')
            .addSelect('m.filename', 'filename')
            .addSelect('m.fileSize', 'fileSize')
            .addSelect('m.imagePath', 'imagePath')
            .addSelect('m.voicePath', 'voicePath')
            .addSelect('m.isEdited', 'isEdited')

            .addSelect('m.isForward', 'isForward')
            .addSelect('m.forwardSenderId', 'forwardSenderId')
            .addSelect('m.forwardMessageId', 'forwardMessageId')
            .addSelect('m.forwardSenderName', 'forwardSenderName')

            .addSelect('m.voiceDuration', 'voiceDuration')
            .addSelect('m.replyMessageId', 'replyMessageId')
            .addSelect('m.replyMessage', 'replyMessage')
            // .addSelect(subQuery => {
            //     return subQuery
            //         .select("mes.message", "replyMessage")
            //         .from(Messages, "mes")
            //         .where('mes.id = m.replyMessageId')
            // }, "replyMessage")
            .where('m.chatId = ' + chatId + ' and m.status != 2 and (case when m.status = 3 then (m.senderUserId = ' + user.id + ' and m.getterUserId = ' + byUserId + ') else true end)')
            // .andWhere('m.senderUserId = :senderId and m.getterUserId = :getterId', { senderId: user.id, getterId: byUserId })
            // .orWhere('m.senderUserId = :getterId and m.getterUserId = :senderId', { senderId: user.id, getterId: byUserId })
            .orderBy('m.id', 'DESC')
            .limit(20)
            .offset(20 * (page - 1))
            .getRawMany()

        return { blockStatus, messages }
    }

    // یه وقتی هست که کاربر از اپ میاد بیرون طوریکه دوباره میتونه برگرده تو صفحه
    // ما اون تایم نمیتونم پیام هاش رو بروز کنیم پس با تابع پایینی وقتی برگشت و درواقع انلاین
    // شد تو اون صفحه چک میکنیم از طرف مقابلش هر چند تا پیام جدید که گرفته بود میگیریم و بهش نشون میدیم
    async getNewMessages(user: Users, chatId: number, byUserId: number): Promise<{ blockStatus: number, messages: Messages[] }> {

        // اگه از صفحه چت پیاما رو نمیخوای صفر بده
        // اینجا یا یه چت میسازیم یا قبلیشو میدیم
        if (chatId == 0) {
            const chatBefore = await getManager().findOne(Chats, {
                where: [
                    { senderUserId: user.id, getterUserId: byUserId },
                    { senderUserId: byUserId, getterUserId: user.id }
                ]
            })
            if (!chatBefore) {
                const newChat = new Chats()
                newChat.senderUserId = user.id
                newChat.getterUserId = byUserId
                newChat.dateCreate = new Date()
                await newChat.save()
                chatId = newChat.id
            } else {
                chatId = chatBefore.id
            }
        }

        const em = getManager()
        var blockStatus = 1

        const isBlockedByMe = await em
            .createQueryBuilder(BlockList, 'b')
            .where('b.userId = ' + user.id + ' and b.blockUserId = ' + byUserId)
            .getOne()

        const isBlockedByFriend = await em
            .createQueryBuilder(BlockList, 'b')
            .where('b.userId = ' + byUserId + ' and b.blockUserId = ' + user.id)
            .getOne()

        if (isBlockedByMe) {
            blockStatus = 2
        }
        if (isBlockedByFriend) {
            blockStatus = 3
        }

        const messages = await em.createQueryBuilder(Messages, 'm')
            .select('m.id', 'id')
            .addSelect('m.senderUserId', 'senderUserId')
            .addSelect('m.getterUserId', 'getterUserId')
            .addSelect('m.message', 'message')
            .addSelect('m.dateCreate', 'dateCreate')
            .addSelect('m.isSeen', 'isSeen')
            .addSelect('m.isReply', 'isReply')
            .addSelect('m.filePath', 'filePath')
            .addSelect('m.filename', 'filename')
            .addSelect('m.isEdited', 'isEdited')

            .addSelect('m.isForward', 'isForward')
            .addSelect('m.forwardSenderId', 'forwardSenderId')
            .addSelect('m.forwardMessageId', 'forwardMessageId')
            .addSelect('m.forwardSenderName', 'forwardSenderName')

            .addSelect('m.fileSize', 'fileSize')
            .addSelect('m.imagePath', 'imagePath')
            .addSelect('m.voicePath', 'voicePath')
            .addSelect('m.voiceDuration', 'voiceDuration')
            .addSelect('m.replyMessageId', 'replyMessageId')
            .addSelect(subQuery => {
                return subQuery
                    .select("mes.message", "replyMessage")
                    .from(Messages, "mes")
                    .where('mes.id = m.replyMessageId')
            }, "replyMessage")
            .where('m.chatId = ' + chatId + ' and m.status != 2 and m.isSeen = false and m.senderUserId = ' + byUserId + ' and m.getterUserId = ' + user.id)
            .orderBy('m.id', 'DESC')
            .limit(50)
            .getRawMany()

        return { blockStatus, messages }
    }

    async ediMessages(token: string, messageId: number, text: string): Promise<Messages> {
        const em = getManager()
        const user = await em.getCustomRepository(UsersRepository).getUserByToken(token) //em .findOne(Users, { token: token })
        if (!user) {
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.')
        }
        const message = await em.findOne(Messages, { id: messageId, senderUserId: user.id })
        if (!message) {
            throw new NotFoundException()
        } else {
            message.dateLastEdit = new Date()
            message.isEdited = true
            message.message = text
            await message.save()
            return message
        }
    }

    async getUsersToForwardForward(user: Users, nameOrUsername: string): Promise<Users[]> {

        if (!nameOrUsername || nameOrUsername.length == 0) {
            return []
        }
        nameOrUsername = nameOrUsername.replace("'", '')

        var users = []
        if (user) {
            users = await getManager().createQueryBuilder(Messages, 'm')
                .leftJoin('m.getterUser', 'getter')
                .leftJoin('m.senderUser', 'sender')
                .select('(case when (sender.id = ' + user.id + ') then getter.id else sender.id end) as id')
                .addSelect('(case when (sender.id = ' + user.id + ') then getter.avatar else sender.avatar end) as avatar')
                .addSelect('(case when (sender.id = ' + user.id + ') then getter.city else sender.city end) as city')
                .addSelect('(case when (sender.id = ' + user.id + ') then getter.name else sender.name end) as name')
                .addSelect('(case when (sender.id = ' + user.id + ') then getter.username else sender.username end) as username')
                .addSelect('(case when (sender.id = ' + user.id + ') then (case when getter.isShowingLastOnline then getter.isOnline else null end) else (case when sender.isShowingLastOnline then sender.isOnline else null end) end) as "isOnline"')
                .addSelect('(case when (sender.id = ' + user.id + ') then getter.district else sender.district end) as district')
                .addSelect('(case when (sender.id = ' + user.id + ') then (case when getter."isShowingLastOnline" is true THEN getter."dateLastOnline" ELSE null end) else (case when sender."isShowingLastOnline" is true THEN sender."dateLastOnline" ELSE null end) end) as "dateLastOnline"')
                .where('getter.status = 1 and sender.status = 1 and (m.senderUserId = ' + user.id + ' or m.getterUserId = ' + user.id + ")")
                .andWhere('(case when (sender.id = ' + user.id + ") then (getter.name like '" + nameOrUsername + "%' or getter.username like '" + nameOrUsername + "%') else (sender.name like '" + nameOrUsername + "%' or sender.username like '" + nameOrUsername + "%') end)")
                .andWhere(subQuery => {
                    const sub = subQuery.subQuery()
                        .select("max(mm.id)", "maxId")
                        .from(Messages, "mm")
                        .where('mm.chatId = m.chatId')
                        .getQuery()
                    return 'm.id = ' + sub
                })
                .orderBy('m.id', 'DESC')
                .limit(20)
                .getRawMany()
        }

        if (users.length < 10 && nameOrUsername.length > 0) {
            const more = await getManager().createQueryBuilder(Users, 'u')
                .select('u.id', 'id')
                .addSelect('u.avatar', 'avatar')
                .addSelect('u.city', 'city')
                .addSelect('u.name', 'name')
                .addSelect('u.status', 'status')
                .addSelect('u.username', 'username')
                .addSelect('case when u.isShowingLastOnline then u.isOnline else null end as "isOnline"')
                .addSelect('u.district', 'district')
                .addSelect('case when u."isShowingLastOnline" is true THEN u."dateLastOnline" ELSE null end as "dateLastOnline"')
                .where("u.status = 1 and u.name like '" + nameOrUsername + "%' or u.username like '" + nameOrUsername + "%'")
                .limit(10)
                .getRawMany()

            more.forEach(element => {
                Logger.log('eeee ::: ' + element.status)
                if (element.status == 1) {
                    users.push(element)
                }
            });
        }
        return users
    }

    async getChats(user: Users, page: number): Promise<Chats[]> {
        const chats = await getManager().createQueryBuilder(Chats, 'c')
            .leftJoin('c.messages', 'm')
            .leftJoin('c.senderUser', 'sender')
            .leftJoin('c.getterUser', 'getter')
            .select('c.id', 'id')
            .addSelect('c.getterUserId', 'getterUserId')
            .addSelect('c.senderUserId', 'senderUserId')
            .addSelect('(case when (sender.id = ' + user.id + ') then getter.avatar else sender.avatar end) as avatar')
            .addSelect('(case when (sender.id = ' + user.id + ') then getter.city else sender.city end) as city')
            .addSelect('(case when (sender.id = ' + user.id + ') then getter.name else sender.name end) as name')
            .addSelect('(case when (sender.id = ' + user.id + ') then getter.isOnline else sender.isOnline end) as "isOnline"')
            .addSelect('(case when (sender.id = ' + user.id + ') then getter.district else sender.district end) as district')
            .addSelect('(case when (sender.id = ' + user.id + ') then (case when getter."isShowingLastOnline" is true THEN getter."dateLastOnline" ELSE null end) else (case when sender."isShowingLastOnline" is true THEN sender."dateLastOnline" ELSE null end) end) as "dateLastOnline"')
            .addSelect('m.message', 'message')
            .addSelect('m.dateCreate', 'dateCreate')
            .addSelect('(select count(id) from messages where "isSeen" = false and "getterUserId" = ' + user.id + ' and "senderUserId" = (case when c.senderUserId = ' + user.id + ' then c.getterUserId else c.senderUserId end) and status = 1) as "newMessageCount"')
            // .addSelect(subQuery => {
            //     return subQuery
            //         .select("count(mes.id)", "newMessageCount")
            //         .from(Messages, "mes")
            //         .where('case when mes.status = 3 then mes.senderUserId = ' + user.id + ' else (mes.senderUserId = ' + user.id + ' or mes.getterUserId = ' + user.id + ') end')
            //         .andWhere('mes.status = 1 and (mes.senderUserId = case when (m.senderUserId = ' + user.id + ') then m.getterUserId else m.senderUserId end) and mes.getterUserId = ' + user.id)
            //         .andWhere('mes.isSeen = false')
            // }, "newMessageCount")
            .where('getter.status = 1 and sender.status = 1 and m.status != 2 and (c.senderUserId = ' + user.id + ' or c.getterUserId = ' + user.id + ')')
            .andWhere(subQuery => {
                const sub = subQuery.subQuery()
                    .select("max(mm.id)", "maxId")
                    .from(Messages, "mm")
                    .where('mm.chatId = m.chatId and mm.status != 2 and case when mm.status = 3 then mm.senderUserId = ' + user.id + ' else true end')
                    .getQuery()
                return 'm.id = ' + sub
            })
            .orderBy('"dateCreate"', 'DESC')
            .limit(10)
            .offset(10 * (page - 1))
            .getRawMany()
        return chats
    }

    async deleteMessage(token: string, messageId: number): Promise<{ status: number, friendOfSender: number, friendOfGetter: number }> {
        const em = getManager()
        const user = await em.getCustomRepository(UsersRepository).getUserByToken(token)//e m.findOne(Users, { token: token })
        if (!user) {
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.')
        }
        const message = await em.findOne(Messages, { id: messageId })
        var friendOfSender = user.id == message.senderUserId ? message.getterUserId : message.senderUserId
        var friendOfGetter = user.id != message.senderUserId ? message.getterUserId : message.senderUserId

        // Logger.log('ergeerge:: 000 ' + message.isStartedMessage)
        // if (message.isStartedMessage == 1 && message.senderUserId == user.id) {
        //     Logger.log('ergeerge:: 111 ' + message.isStartedMessage)
        //     const isMoreThenThis = await em.createQueryBuilder(Messages, 'm')
        //         .select('count(m.id) > 1 as isit')
        //         .where('((m.senderUserId = ' + message.senderUserId + ' and m.getterUserId = ' + message.getterUserId + ') or (m.senderUserId = ' + message.getterUserId + ' and m.getterUserId = ' + message.senderUserId + ')) and m.status != 2 and m.isStartedMessage != 4')
        //         .getRawOne()
        //     if (String(isMoreThenThis.isit) == 'true') {
        //         const status = message.senderUserId == user.id ? 2 : 3
        //         if (message.senderUserId == user.id || message.getterUserId == user.id) {
        //             message.status = status
        //             await message.save()
        //             return { status, friendOfSender, friendOfGetter }
        //         } else {
        //             throw new ConflictException('این پیام متعلق به شما نیست.')
        //         }
        //     } else {
        //         await em.update(Messages, {
        //             where: [
        //                 { senderUserId: message.senderUserId, getterUserId: message.getterUserId },
        //                 { senderUserId: message.getterUserId, getterUserId: message.senderUserId }]
        //         },
        //             { status: 2, isStartedMessage: 4 })
        //         Logger.log('iiwegweg');
        //         return { status: 2, friendOfSender, friendOfGetter }
        //     }
        // }
        if (message) {
            const status = message.senderUserId == user.id ? 2 : 3
            if (message.senderUserId == user.id || message.getterUserId == user.id) {
                message.status = status
                await message.save()
                return { status, friendOfSender, friendOfGetter }
            } else {
                throw new ConflictException('این پیام متعلق به شما نیست.')
            }
        } else {
            throw new ConflictException('این پیام پیدا نشد.')
        }
    }

    async deleteChat(user: Users, friendId: number): Promise<{ message: string }> {
        await getManager().update(Messages,
            { getterUserId: user.id, senderUserId: friendId, status: 1 },
            { status: 3 })
        await getManager().update(Messages,
            { senderUserId: user.id, getterUserId: friendId },
            { status: 2 })

        return { message: 'انجام شد.' }

    }



}
