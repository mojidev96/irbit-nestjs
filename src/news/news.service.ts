import { Comments } from './../comments/comments.entity';
import { NewsCategories } from './../news-categories/news-categories.entity';
import { getManager } from 'typeorm';
import { Injectable, Logger } from '@nestjs/common';
import { News } from './news.entity';
import { UsersRepository } from 'src/users/users.repository';

@Injectable()
export class NewsService {

    async getNews(token: string, page: number, categoryId: number, sortType: number, word: string): Promise<any> {

        const em = getManager()

        var whereCategory = 'true'
        if (categoryId && categoryId > 0) {
            whereCategory = 'n.categoryId = ' + categoryId
        }

        var sort = 'n.id'
        if (sortType && sortType == 1) { // most ups
            sort = 'n.ups'
        } else if (sortType && sortType == 2) { // most seen
            sort = 'n.seenCount'
        }

        var whereWord = 'true'
        if (word && word.length > 0) {
            whereWord = "n.title ilike '%" + word + "%' or c.title ilike '%" + word + "%'"
        }

        var selectIUp = 'false as "iUp"'
        var selectIDown = 'false as "iDown"'

        const user = await em.getCustomRepository(UsersRepository).getUserByToken(token)
        if (user) {
            selectIUp = '(select "isUp" from "upAndDowns" where "userId" = ' + user.id + ' and "contentType" = 1 and "contentId" = n.id and "isUp" is true) as "iUp"'
            selectIDown = '(select "isUp" is false from "upAndDowns" where "userId" = ' + user.id + ' and "contentType" = 1 and "contentId" = n.id and "isUp" is false) as "iDown"'
        }

        const news = await em.createQueryBuilder(News, 'n')
            .leftJoin(NewsCategories, 'c', 'n.categoryId = c.id')
            .select('n.id', 'id')
            .addSelect('n.title', 'title')
            .addSelect('n.description', 'description')
            .addSelect('n.content', 'content')
            .addSelect('n.author', 'author')
            .addSelect('n.sourceUrl', 'sourceUrl')
            .addSelect('n.dateCreate', 'dateCreate')
            .addSelect('n.seenCount', 'seenCount')
            .addSelect('n.ups', 'ups')
            .addSelect('n.downs', 'downs')
            .addSelect('n.poster', 'poster')
            .addSelect('c.title', 'categoryName')
            .addSelect('(select count(*) from comments where "contentType" = 2 and "isDeleted" is false and "contentId" = n.id) as "commentsCount"')
            .addSelect(selectIUp)
            .addSelect(selectIDown)
            .where(whereCategory)
            .andWhere(whereWord)
            .orderBy(sort, 'DESC')
            .offset(15 * (page - 1))
            .limit(15)
            .getRawMany()

        return news
    }

}
