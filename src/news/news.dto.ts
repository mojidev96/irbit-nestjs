export class NewsDTO {
    author: string
    title: string
    description: string
    content: string
    categoryId: number
    sourceUrl: string
}