import { Body, Controller, Post, Headers } from '@nestjs/common';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {

    constructor(private newsService: NewsService) { }

    @Post()
    getNews(
        @Body('page') page: number,
        @Body('categoryId') categoryId: number,
        @Body('sortType') sortType: number,
        @Body('word') word: string,
        @Headers('token') token: string,
    ): Promise<any> {
        return this.newsService.getNews(token, page, categoryId, sortType, word)
    }
}
