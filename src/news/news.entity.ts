import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn, Double } from 'typeorm';


@Entity('news')
export class News extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    author: string;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    sourceUrl: string;

    @Column()
    content: string;

    @Column()
    poster: string;

    @Column()
    categoryId: number;

    @Column()
    ups: number

    @Column()
    downs: number

    @Column()
    seenCount: number

    @Column()
    dateCreate: Date
}