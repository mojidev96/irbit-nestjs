import { AppGateway } from './app.gateway';
import { Coins } from './coins/coins.entity';
import { Plans } from './plans/plans.entity';
import { Planers } from './planers/planers.entity';
import { PlanParts } from './plan-parts/planParts.entity';
import { HttpService, Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { getManager, MoreThan, MoreThanOrEqual, LessThan } from 'typeorm';
// import { DueDates } from './due-dates/due-dates.entity';
import { Users } from './users/users.entity';
import axios from 'axios'
import { firebaseServerKey } from './app.module';
import { Socket, Server } from 'socket.io';
import { Posts } from './posts/posts.entity';
import { Comments } from './comments/comments.entity';

@Injectable()
export class AppService {

  public socket: Server = null;

  constructor(private http: HttpService) { }

  getHello(): string {
    return 'Hello World!';
  }

  // ارسال نوتیف به فروشگاه برای تغییر حالات مثلا معلق شدن
  sendFCMForStoreState(title: string, body: string, storeStatus: number, getterFCMToken: string) {
    // حتما تایتل و بادی تو دیتا باشه چون ممکنه تو لانچ از بکگراند دیتاش پارس نشه
    axios.post('https://fcm.googleapis.com/fcm/send', {
      'notification': {
        'body': body,
        'title': title
      },
      'priority': 'high', 'data': {
        'click_action': 'FLUTTER_NOTIFICATION_CLICK',
        'type': 'store-state',
        'status': storeStatus,
        'fromId': 0,
        'body': body,
        'title': title
      },
      'to': getterFCMToken
    }, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': firebaseServerKey,
      }
    }).catch(error => {
      Logger.error('fcm:: ' + error)
    })
  }

  // افرادی که بیش از حد مجاز بلاک شده باشن در یک روز مشمول بگایی میشن
  @Cron(CronExpression.EVERY_HOUR)
  async blockController() {
    const em = getManager()
    await em.createQueryBuilder()
      .update(Users)
      .set({
        dateBlockReleased: () => 'Date(CURRENT_DATE) + 3'
      })
      .where('blockCountInDay >= 2 and blockCountInMonth < 6')
      .execute()

    await em.createQueryBuilder()
      .update(Users)
      .set({
        dateBlockReleased: () => 'Date(CURRENT_DATE) + 5'
      })
      .where('blockCountInDay >= 2 and blockCountInMonth < 12 and blockCountInMonth >= 6')
      .execute()

    await em.createQueryBuilder()
      .update(Users)
      .set({
        dateBlockReleased: () => 'Date(CURRENT_DATE) + 7'
      })
      .where('blockCountInDay >= 2 and blockCountInMonth < 18 and blockCountInMonth >= 12')
      .execute()

    await em.createQueryBuilder()
      .update(Users)
      .set({
        dateBlockReleased: () => 'Date(CURRENT_DATE) + 10'
      })
      .where('blockCountInDay >= 2 and blockCountInMonth < 24 and blockCountInMonth >= 18')
      .execute()

    await em.createQueryBuilder()
      .update(Users)
      .set({
        dateBlockReleased: () => 'Date(CURRENT_DATE) + 20'
      })
      .where('blockCountInDay >= 2 and blockCountInMonth < 30 and blockCountInMonth >= 24')
      .execute()

    await em.createQueryBuilder()
      .update(Users)
      .set({
        dateBlockReleased: () => 'Date(CURRENT_DATE) + 30'
      })
      .where('blockCountInDay >= 2 and blockCountInMonth > 30')
      .execute()
  }

  // ما بسته به تعداد دفعات بلاک شدن در یک ماه هر کاربر اونو بلاک میکنیم پس باید هر ماه اون تاریخا بروز شن نسبت به خودشون
  // این اونکارو میکنه
  @Cron(CronExpression.EVERY_12_HOURS)
  async resetBlockCounterInMonth() {
    await getManager().createQueryBuilder()
      .update(Users)
      .set({
        dateBlockCounterReset: () => 'Date(CURRENT_DATE) + 30',
        blockCountInMonth: 0,
        blockCountInDay: 0
      })
      .where('dateBlockCounterReset <= Date(CURRENT_DATE)')
      .execute()
  }

  // روزی یبار همه شماره های بلاک در روزو صفر می کنیم
  @Cron(CronExpression.EVERY_DAY_AT_1AM)
  async resetBlockCountInDays() {
    await getManager().createQueryBuilder()
      .update(Users)
      .set({
        blockCountInDay: 0
      })
      .execute()
  }


  // ارسال نوتیف به فروشگاه
  sendStoreFCM(title: string, body: string, type: string, fcmToken: string, webFCMToken: string) {
    // حتما تایتل و بادی تو دیتا باشه چون ممکنه تو لانچ از بکگراند دیتاش پارس نشه
    axios.post('https://fcm.googleapis.com/fcm/send', {
      'notification': {
        'body': body,
        'title': title
      },
      'priority': 'high', 'data': {
        'click_action': 'FLUTTER_NOTIFICATION_CLICK',
        'type': type,
        'fromId': 0,
        'body': body,
        'title': title
      },
      'to': fcmToken
    }, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': firebaseServerKey,
      }
    }).catch(error => {
      Logger.error('fcm:: ' + error)
    })

    if (webFCMToken)
      axios.post('https://fcm.googleapis.com/fcm/send', {
        'notification': {
          'body': body,
          'title': title
        },
        'priority': 'high', 'data': {
          'click_action': 'FLUTTER_NOTIFICATION_CLICK',
          'type': type,
          'fromId': 0,
          'body': body,
          'title': title
        },
        'to': webFCMToken
      }, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': firebaseServerKey,
        }
      }).catch(error => {
        Logger.error('fcm:: ' + error)
      })
  }

  @Cron(CronExpression.EVERY_MINUTE)
  async updateCommentsCount() {
    const em = getManager()
    const posts = await em.createQueryBuilder(Posts, 'p')
      .select('p.id', 'id')
      .getRawMany()
    for await (const post of posts) {
      const comCount = await em.createQueryBuilder(Comments, 'c')
        .select('count(*)', 'count')
        .where('c.contentType = 3 and c.contentId = ' + post.id + ' and c.isAccepted is true and c.isDeleted is false')
        .getRawOne()
      await em.update(Posts, { id: post.id }, { commentsCount: comCount.count ?? 0 })
    }
  }


}
