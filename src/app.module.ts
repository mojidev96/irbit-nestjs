
import { BlockListService } from './block-list/block-list.service';
import { JwtService, JwtModule } from '@nestjs/jwt';
import { UsersRepository } from './users/users.repository';
import { MessagesService } from './messages/messages.service';
import { UsersService } from './users/users.service';
import { AppGateway } from './app.gateway';
import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './typeorm.config';
import { BannersModule } from './banners/banners.module';
import { MessagesModule } from './messages/messages.module';
import { LocationsModule } from './locations/locations.module';
import { ReportsModule } from './reports/reports.module';
import { AdminModule } from './admin/admin.module';
import { PaymentsModule } from './payments/payments.module';
import { ScheduleModule } from '@nestjs/schedule';
import { BlockListModule } from './block-list/block-list.module';
import { JwtStrategy } from './users/jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { MulterModule } from '@nestjs/platform-express';
import { ChatsModule } from './chats/chats.module';
import { UpdatesModule } from './updates/updates.module';
import { TokensModule } from './tokens/tokens.module';
import { SocietyCategoriesModule } from './society-categories/society-categories.module';
import { SocietyTopicsModule } from './society-topics/society-topics.module';
import { SocietyMessagesModule } from './society-messages/society-messages.module';
import { SocietyUpAndDownsModule } from './society-up-and-downs/society-up-and-downs.module';
import { PlansModule } from './plans/plans.module';
import { PlanersModule } from './planers/planers.module';
import { PlanPartsModule } from './plan-parts/plan-parts.module';
import { FreePlanCodesModule } from './free-plan-codes/free-plan-codes.module';
import { SamplePlansModule } from './sample-plans/sample-plans.module';
import { SampleCafesModule } from './sample-cafes/sample-cafes.module';
import { NewsModule } from './news/news.module';
import { NewsCategoriesModule } from './news-categories/news-categories.module';
import { TutorialsModule } from './tutorials/tutorials.module';
import { TutorialCategoriesModule } from './tutorial-categories/tutorial-categories.module';
import { CommentsModule } from './comments/comments.module';
import { UpAndDownsModule } from './up-and-downs/up-and-downs.module';
import { CoinsModule } from './coins/coins.module';
import { CoinMessagesModule } from './coin-messages/coin-messages.module';
import { CoinMessagesService } from './coin-messages/coin-messages.service';
import { PostsModule } from './posts/posts.module';
import { FollowsModule } from './follows/follows.module';
import { PostLikesModule } from './post-likes/post-likes.module';

export const badWords = 'کیری-کونی-کونده-کیر-کص-کون-خایه-خایه مال-خایه مالی-مادرجنده-جنده-زنازاده-زانی-عمتو-گاییدم-گا-گای-بگا-بگای-پدرسگ-سگ پدر-مادرسگ- مادر سگ-خواهرتو-مادرتو-جنده پولی-بی ناموس-لاشی-کصکش-کسکش-کص دست-کصمغز-عنتر-کیونی-کیونده-مامله-ممه-پستون-تخم حروم-تخمه سگ-کیرم دهنت-کیرم-کونت-مریض-منگل-مرده شور-مرض-کثافط-کثافت-کسافت-کسافط-خفه-بیلاخ-بیلخ'.split('-')
export const firebaseServerKey = 'key=AAAAYvsITkY:APA91bHEwpdn-Tk2wpGKug9WCC7jFHOr02bThNBYXYEwzLUmY4FioB22uthP49DrvcF4s0F8VwVVd2evsJvKxoMrfvGpnMPYHVMFrdahk3Edjf_rvvY3STYWQG6gxo1bzDnC3ki4X0yy'
@Module({
  imports: [UsersModule,
    TypeOrmModule.forRoot(typeOrmConfig),
    BannersModule,
    MessagesModule,
    LocationsModule,
    ReportsModule,
    AdminModule,
    PaymentsModule,
    ScheduleModule.forRoot(),
    BlockListModule,
    JwtModule.register({
      secret: 'mojidev1996'
    }),
    PassportModule.register({
      defaultStrategy: 'jwt'
    }),
    MulterModule.register({
      dest: './files',
    }),
    ChatsModule,
    UpdatesModule,
    TokensModule,
    SocietyCategoriesModule,
    SocietyTopicsModule,
    SocietyMessagesModule,
    SocietyUpAndDownsModule,
    PlansModule,
    PlanersModule,
    PlanPartsModule,
    FreePlanCodesModule,
    SamplePlansModule,
    SampleCafesModule,
    NewsModule,
    NewsCategoriesModule,
    TutorialsModule,
    TutorialCategoriesModule,
    CommentsModule,
    UpAndDownsModule,
    CoinsModule,
    CoinMessagesModule,
    PostsModule,
    FollowsModule,
    PostLikesModule
  ],
  controllers: [AppController],
  providers: [AppService, AppGateway, MessagesService, CoinMessagesService, UsersService, UsersRepository, JwtStrategy, BlockListService,],
  exports: [UsersService, MessagesService, CoinMessagesService, JwtStrategy, BlockListService,]
})
export class AppModule { }
