import { PlanPartsService } from './plan-parts.service';
import { Body, Controller, Post, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('planParts')
export class PlanPartsController {

    constructor(private planPartsService: PlanPartsService) { }

    @Post('join')
    @UseInterceptors(FileInterceptor('avatar'))
    join(
        @Body('planId') planId: number,
        @Body('name') name: string,
        @Body('about') about: string,
        @Body('phone') phone: string,
        @Body('freeCode') freeCode: string,
        @UploadedFile() file
    ): Promise<any> {
        return this.planPartsService.join(planId, name, about, phone, file, freeCode)
    }
}
