import { FreePlanCodes } from './../free-plan-codes/free-plan-codes.entity';
import { Plans } from './../plans/plans.entity';
import { Planers } from './../planers/planers.entity';
import { getManager } from 'typeorm';
import { PlanParts } from './planParts.entity';
import { Injectable, NotFoundException, ConflictException, Logger, HttpService } from '@nestjs/common';

@Injectable()
export class PlanPartsService {

    constructor(
        private http: HttpService
    ) { }

    async join(planId: number, name: string, about: string, phone: string, file: any, freeCode: string): Promise<any> {
        const em = getManager()
        var planBefore = await em.findOne(Plans, { id: planId })
        const plan = await getManager().findOne(Plans, { id: planId })

        var freeCodeData = null
        if (freeCode) {
            freeCodeData = await em.findOne(FreePlanCodes, { code: freeCode })
        }

        if (!planBefore) {
            throw new NotFoundException('برنامه پیدا نشد...')
        }
        if (plan.membersCount >= plan.maxMembers) {
            throw new ConflictException('ظرفیت تکمیل شده است')
        }
        var planerBefore = await em.findOne(Planers, { phone: phone })
        if (!planerBefore) {
            const newPlaner = new Planers()
            newPlaner.about = about
            if (file && file.path) {
                newPlaner.avatar = file.path
            }
            newPlaner.name = name
            newPlaner.phone = phone
            await newPlaner.save()

            planerBefore = await em.findOne(Planers, { phone: phone })
        } else {
            planerBefore.name = name
            planerBefore.about = about
            if (file && file.path) {
                planerBefore.avatar = file.path
            }
            await planerBefore.save()

            planerBefore = await em.findOne(Planers, { phone: phone })
        }
        const part = new PlanParts()
        part.isPayed = freeCodeData != null || plan.ticketPrice == 0
        part.planId = planId
        part.amount = plan.ticketPrice
        part.planerId = planerBefore.id
        await part.save()

        if (freeCodeData || plan.ticketPrice == 0) {
            if (freeCodeData) {
                freeCodeData.useTimes = freeCodeData.useTimes + 1
                await freeCodeData.save()
            }

            plan.membersCount = plan.membersCount + 1
            await plan.save()

            const tickerNumber = Math.floor(Math.random() * 99999) + 10000
            this.http.get(encodeURI('https://api.kavenegar.com/v1/4362695A3879324943585372326F7A493574483068413D3D/verify/lookup.json?receptor=' + planerBefore.phone + '&token=' + plan.title.split(' ').join('_') + '&token2=' + tickerNumber + '&token3=' + plan.id + '&template=verifyTicket')).subscribe((res) => {
                Logger.log('kavenegarBarname: ' + res);
            });

            // اگر به حد نصاب رسیده به بقیه اطلاع میدیم
            if (plan.membersCount == plan.minMembers) {
                const planParts = await em.createQueryBuilder(PlanParts, 'pp')
                    .leftJoin(Planers, 'planer', 'planer.id = pp.planerId')
                    .leftJoin(Plans, 'plan', 'plan.id = pp.planId')
                    .select('planer.phone', 'phone')
                    .addSelect('plan.title', 'planTitle')
                    .addSelect('plan.id', 'planId')
                    .where('p.plan <= Date(CURRENT_DATE) + 2 and plan.date >= now()')
                    .getRawMany()
                for await (const p of planParts) {
                    this.http.get(encodeURI('https://api.kavenegar.com/v1/4362695A3879324943585372326F7A493574483068413D3D/verify/lookup.json?receptor=' + p.phone + '&token=' + p.title.split(' ').join('_') + '&token2=' + p.planId + '&template=planQuorum')).subscribe((res) => {
                        Logger.log('kavenegar: ' + res);
                    });
                }
            }
        }

        return { payLink: 'https://forusha.com/payTicket.php?uid=' + part.planerId + '&partId=' + part.id + '&phone=' + planerBefore.phone + '&amount=' + plan.ticketPrice + '&description=خریدـبلیتـازـفروشا' + '&planName=' + plan.title + '&planId=' + plan.id, planerId: planerBefore.id, isPayed: freeCodeData != null || plan.ticketPrice == 0, planId: plan.id }

    }

}
