import { Module } from '@nestjs/common';
import { PlanPartsService } from './plan-parts.service';
import { PlanPartsController } from './plan-parts.controller';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [UsersModule],
  providers: [PlanPartsService],
  controllers: [PlanPartsController]
})
export class PlanPartsModule { }
