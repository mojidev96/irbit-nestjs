import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from 'typeorm';


@Entity('planParts')
export class PlanParts extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    planId: number;

    @Column()
    planerId: number;

    @Column()
    amount: number;

    @Column()
    payTime: Date;

    @Column()
    transcode: string;

    @Column('bool')
    isPayed: boolean;
}