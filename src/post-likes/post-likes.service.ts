import { Injectable } from '@nestjs/common';
import { Posts } from 'src/posts/posts.entity';
import { Users } from 'src/users/users.entity';
import { getManager } from 'typeorm';
import { PostLikes } from './post-likes.entity';

@Injectable()
export class PostLikesService {

    async likeUnlike(user: Users, id: number): Promise<any> {
        const em = getManager()
        const likeBefore = await em.findOne(PostLikes, { userId: user.id, postId: id })
        if (likeBefore) {
            await likeBefore.remove()
            await em.update(Posts, { id }, { likesCount: () => '"likesCount" - 1' })
        } else {
            const newLike = new PostLikes()
            newLike.postId = id
            newLike.userId = user.id
            newLike.dateCreate = new Date()
            await newLike.save()
            await em.update(Posts, { id }, { likesCount: () => '"likesCount" + 1' })
        }
        return { message: 'انجام شد' }
    }
}
