import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn, Double } from 'typeorm';


@Entity('postLikes')
export class PostLikes extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userId: number;

    @Column()
    postId: number;
    
    @Column()
    dateCreate: Date
}