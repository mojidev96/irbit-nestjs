import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/users/getuser.decorator';
import { Users } from 'src/users/users.entity';
import { PostLikesService } from './post-likes.service';

@Controller('postLikes')
export class PostLikesController {

    constructor(private likesService: PostLikesService) {}

    @Post()
    @UseGuards(AuthGuard('jwt'))
    likeUnlike(
        @GetUser() user: Users,
        @Body('id') id: number
    ): Promise<any>{
        return this.likesService.likeUnlike(user, id)
    }
}
