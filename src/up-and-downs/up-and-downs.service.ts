import { getManager } from 'typeorm';
import { Users } from 'src/users/users.entity';
import { UpAndDowns } from './up-and-downs.entity';
import { Comments } from './../comments/comments.entity';
import { Tutorials } from './../tutorials/tutorials.entity';
import { News } from './../news/news.entity';
import { Injectable, NotFoundException } from '@nestjs/common';

@Injectable()
export class UpAndDownsService {

    async upOrDown(user: Users, isUp: boolean, isForgive: boolean, contentId: number, contentType: number): Promise<any> {
        const em = getManager()
        var data = null
        if (contentType == 1) { // news
            data = await em.findOne(News, { id: contentId })
        } else if (contentType == 2) { // tutorials
            data = await em.findOne(Tutorials, { id: contentId })
        } else if (contentType == 3) { // comments
            data = await em.findOne(Comments, { id: contentId })
        }

        if (!data) {
            throw new NotFoundException('پیدا نشد.')
        }

        const upOrDownBefore = await em.findOne(UpAndDowns, { userId: user.id, contentId, contentType })
        var isBeforeUp = null


        if (String(isForgive) == 'true') {
            // پاک کن اونی که قبلا زده بودمو
            if (upOrDownBefore) {
                isBeforeUp = upOrDownBefore.isUp
                if (upOrDownBefore.isUp == isUp) {
                    // اگه همونطوری که میگی هست حذفش میکنیم
                    await upOrDownBefore.remove()
                }
            } else {
                throw new NotFoundException('پیدا نشد اینکه قبلا زده باشیش...')
            }
        } else {
            // ثبت مورد جدید چه آپ چه داون
            if (upOrDownBefore) {
                isBeforeUp = upOrDownBefore.isUp
                // اگه قبلا بوده فقط حالتش عوض میشه یعنی آپ بوده میشه داون و بالعکس
                upOrDownBefore.isUp = String(isUp) == 'true'
                await upOrDownBefore.save()
            } else {
                const newUpOrDown = new UpAndDowns()
                newUpOrDown.userId = user.id
                newUpOrDown.contentId = contentId
                newUpOrDown.isUp = isUp
                newUpOrDown.dateCreate = new Date()
                newUpOrDown.contentType = contentType
                await newUpOrDown.save()
            }
        }

        // کم و زیاد شدن کل آمارا اینجا انجام میشه
        if (upOrDownBefore && isBeforeUp == true && String(isUp) == 'true' && String(isForgive) == 'true') {
            await em.update(contentType == 1 ? News : contentType == 2 ? Tutorials : Comments, { id: contentId }, { ups: () => 'ups - 1' })
        } else if (upOrDownBefore && isBeforeUp == false && String(isUp) == 'false' && String(isForgive) == 'true') {
            await em.update(contentType == 1 ? News : contentType == 2 ? Tutorials : Comments, { id: contentId }, { downs: () => 'downs - 1' })
        } else if (upOrDownBefore && isBeforeUp == true && String(isUp) == 'false') {
            await em.update(contentType == 1 ? News : contentType == 2 ? Tutorials : Comments, { id: contentId }, { downs: () => 'downs + 1', ups: () => 'ups - 1' })
        } else if (upOrDownBefore && isBeforeUp == false && String(isUp) == 'true') {
            await em.update(contentType == 1 ? News : contentType == 2 ? Tutorials : Comments, { id: contentId }, { downs: () => 'downs - 1', ups: () => 'ups + 1' })
        } else if (!upOrDownBefore && String(isUp) == 'false') {
            await em.update(contentType == 1 ? News : contentType == 2 ? Tutorials : Comments, { id: contentId }, { downs: () => 'downs + 1' })
        } else if (!upOrDownBefore && String(isUp) == 'true') {
            await em.update(contentType == 1 ? News : contentType == 2 ? Tutorials : Comments, { id: contentId }, { ups: () => 'ups + 1' })
        }

        return { message: 'انجام شد.' }
    }
}
