import { Users } from 'src/users/users.entity';
import { UpAndDownsService } from './up-and-downs.service';
import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/users/getuser.decorator';

@Controller('upAndDowns')
export class UpAndDownsController {

    constructor(private upAndDownsService: UpAndDownsService){}

    @Post()
    @UseGuards(AuthGuard('jwt'))
    upOrDown(
        @GetUser() user: Users,
        @Body('isUp') isUp: boolean,
        @Body('isForgive') isForgive: boolean,
        @Body('contentId') contentId: number,
        @Body('contentType') contentType: number,
    ): Promise<any> {
        return this.upAndDownsService.upOrDown(user, isUp, isForgive, contentId, contentType)
    }
}
