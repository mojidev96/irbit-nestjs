import { Module } from '@nestjs/common';
import { UpAndDownsService } from './up-and-downs.service';
import { UpAndDownsController } from './up-and-downs.controller';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [UsersModule],
  providers: [UpAndDownsService],
  controllers: [UpAndDownsController]
})
export class UpAndDownsModule {}
