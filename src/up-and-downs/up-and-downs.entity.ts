import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn, Double } from 'typeorm';


@Entity('upAndDowns')
export class UpAndDowns extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userId: number;

    @Column()
    contentId: number;

    // 1 = news
    // 2 = tutorials
    // 3 = comments
    @Column()
    contentType: number;

    @Column('bool')
    isUp: boolean;

    @Column()
    dateCreate: Date;
}