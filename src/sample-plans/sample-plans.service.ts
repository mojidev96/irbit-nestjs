import { SamplePlans } from './sample-plans.entity';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { SamplePlan } from 'src/sample-cafes/sample-cafes.controller';
import { Users } from 'src/users/users.entity';

@Injectable()
export class SamplePlansService {

    async addSamplePlan(user: Users, sample: SamplePlan, file: any): Promise<any> {
        if (user.phone == '09393022461') {
            const newSample = new SamplePlans()
            newSample.category = sample.category
            newSample.description = sample.description
            newSample.maxMembers = sample.maxMembers
            newSample.minMembers = sample.minMembers
            newSample.poster = file.path
            newSample.planTime = sample.planTime
            newSample.ticketPrice = sample.ticketPrice
            newSample.title = sample.title
            await newSample.save()
            return { message: 'job done' }
        } else {
            throw new UnauthorizedException('just939')
        }
    }
}
