import { Module } from '@nestjs/common';
import { SamplePlansService } from './sample-plans.service';
import { SamplePlansController } from './sample-plans.controller';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [UsersModule],
  providers: [SamplePlansService],
  controllers: [SamplePlansController]
})
export class SamplePlansModule {}
