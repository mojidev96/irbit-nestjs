import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { SamplePlansService } from './sample-plans.service';
import { Body, Controller, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { SamplePlan } from 'src/sample-cafes/sample-cafes.controller';
import { SampleCafesService } from 'src/sample-cafes/sample-cafes.service';
import { GetUser } from 'src/users/getuser.decorator';
import { Users } from 'src/users/users.entity';

@Controller('samplePlans')
export class SamplePlansController {
    constructor(private samplePlansService: SamplePlansService) { }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileInterceptor('poster'))
    addNewSamplePlan(
        @GetUser() user: Users,
        @Body() sample: SamplePlan,
        @UploadedFile() file
    ): Promise<any> {
        return this.samplePlansService.addSamplePlan(user, sample, file)
    }
}
