import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, Double } from 'typeorm';

@Entity('samplePlans')
export class SamplePlans extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    poster: string;

    @Column()
    minMembers: number;

    @Column()
    maxMembers: number;

    @Column()
    ticketPrice: number;

    @Column()
    category: string;

    @Column()
    planTime: number;
}