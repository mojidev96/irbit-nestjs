import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity({ name: 'tokens' })
export class Tokens extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    uid: number;

    @Column()
    token: string;

}