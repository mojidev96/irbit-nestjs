import { Messages } from './../messages/messages.entity';
import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { Users } from '../users/users.entity';

@Entity()
export class Chats extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    senderUserId: number;

    @Column()
    getterUserId: number;

    @Column()
    dateCreate: Date;

    @ManyToOne(type => Users)
    senderUser: Users

    @ManyToOne(type => Users)
    getterUser: Users

    @OneToMany(type => Messages, message => message.chat)
    messages: Messages[];
}