import { UsersModule } from 'src/users/users.module';
import { Module } from '@nestjs/common';
import { SocietyTopicsService } from './society-topics.service';
import { SocietyTopicsController } from './society-topics.controller';

@Module({
  imports: [UsersModule],
  providers: [SocietyTopicsService],
  controllers: [SocietyTopicsController]
})
export class SocietyTopicsModule {}
