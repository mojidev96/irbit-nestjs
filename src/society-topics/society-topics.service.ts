import { SocietyUpAndDowns } from './../society-up-and-downs/society-up-and-downs.entity';
import { SocietyMessages } from 'src/society-messages/society-messages.entity';
import { SocietyCategories } from './../society-categories/society-categories.entity';
import { SocietyTopics } from './society-topics.entity';
import { getManager } from 'typeorm';
import { Users } from 'src/users/users.entity';
import { Injectable, ConflictException, NotFoundException, UnauthorizedException, Logger } from '@nestjs/common';
import { TopicDTO } from './society-topics.dto';
import { UsersRepository } from 'src/users/users.repository';

@Injectable()
export class SocietyTopicsService {

    async addNewTopic(user: Users, addDTO: TopicDTO, filePath: string): Promise<any> {
        const em = getManager()
        const topicBefore = await em.createQueryBuilder(SocietyTopics, 't')
            .where('t.makerId = ' + user.id + ' and t.dateCreate > Date(CURRENT_DATE) - 1')
            .getOne()

        var canAddNewOne = true
        if (topicBefore && user.id != 1) {
            canAddNewOne = false
            throw new ConflictException('در ۲۴ ساعت گذشته بیش از یک موضوع نمی توان افزود.')
        } else {
            const topic = new SocietyTopics()
            topic.dateCreate = new Date()
            topic.title = addDTO.title
            topic.description = addDTO.description
            if (filePath) {
                topic.banner = filePath
            }
            topic.catId = addDTO.catId
            topic.makerId = user.id
            await topic.save()

            await getManager().update(SocietyCategories, { id: addDTO.catId }, { topicsCount: () => '"topicsCount" + 1' })

            return topic
        }
    }

    async getTopics(token: string, page: number, catId: number, word: string, orderType: number): Promise<any> {

        var selectIUp = 'false as "iUp"'
        var selectIDown = 'false as "iDown"'
        const em = getManager()

        const user = await em.getCustomRepository(UsersRepository).getUserByToken(token)
        if (user) {
            Logger.log('erwe:: ' + token)
            selectIUp = '(select "isUp" from "societyUpAndDowns" where "userId" = ' + user.id + ' and "contentType" = 1 and "contentId" = st.id and "isUp" is true) as "iUp"'
            selectIDown = '(select "isUp" is false from "societyUpAndDowns" where "userId" = ' + user.id + ' and "contentType" = 1 and "contentId" = st.id and "isUp" is false) as "iDown"'
        }

        var where = 'true'
        if (word && word.length > 0) {
            where = "st.title ilike '%" + word + "%'"
        }

        var order = 'st.id'
        if (orderType == 0) {//newests
            order = 'st.id'
        } else if (orderType == 1) {//oldest
            order = 'st.id'
        } else if (orderType == 2) {//upppss
            order = 'st.ups - st.downs'
        }

        const category = await getManager().findOne(SocietyCategories, { id: catId })

        const topics = await getManager().createQueryBuilder(SocietyTopics, 'st')
            .leftJoin(Users, 'u', 'u.id = st.makerId')
            .select('st.id', 'id')
            .addSelect('st.catId', 'catId')
            .addSelect('st.title', 'title')
            .addSelect('st.description', 'description')
            .addSelect('st.banner', 'banner')
            .addSelect('st.dateCreate', 'dateCreate')
            .addSelect('st.ups', 'ups')
            .addSelect('st.downs', 'downs')
            .addSelect('st.messagesCount', 'messagesCount')
            .addSelect('st.seenCount', 'seenCount')
            .addSelect('st.makerId', 'makerId')
            .addSelect('u.name', 'makerName')
            .addSelect('u.avatar', 'makerAvatar')
            .addSelect('u.job', 'makerJob')
            .addSelect('u.city', 'makerCity')
            .addSelect(selectIUp)
            .addSelect(selectIDown)
            .where(where)
            .andWhere('st.isAvailable is true')
            .andWhere('st.catId = ' + catId)
            .limit(20)
            .offset((page - 1) * 20)
            .orderBy(order, orderType == 0 ? 'DESC' : orderType == 1 ? 'ASC' : 'DESC')
            .getRawMany()

        return { category, topics }
    }

    async getTopicById(token: string, id: number, page: number, orderType: number): Promise<any> {

        var selectIUp = 'false as "iUp"'
        var selectIDown = 'false as "iDown"'
        const em = getManager()
        const user = await em.getCustomRepository(UsersRepository).getUserByToken(token)

        const topic = await em.findOne(SocietyTopics, { id, isAvailable: true })
        topic.seenCount += 1
        await topic.save()

        var iUpTopic = false
        var iDownTopic = false

        if (user) {
            // این ۲ تا مربوط به پیاما میشه
            selectIUp = '(select "isUp" from "societyUpAndDowns" where "userId" = ' + user.id + ' and "contentType" = 2 and "contentId" = sm.id and "isUp" is true) as "iUp"'
            selectIDown = '(select "isUp" is false from "societyUpAndDowns" where "userId" = ' + user.id + ' and "contentType" = 2 and "contentId" = sm.id and "isUp" is false) as "iDown"'
            // اینام مربوط به خود تاپیک میشه
            const d = await em.createQueryBuilder(SocietyUpAndDowns, 'ud')
                .select('ud."isUp"', 'isUp')
                .where('ud."userId" = ' + user.id + ' and ud."contentId" = ' + topic.id + ' and ud."contentType" = 1')
                .getRawOne()
            if (d) {
                iUpTopic = d.isUp == true
                iDownTopic = d.isUp == false
            }
        }

        if (topic) {
            var order = 'sm.id'
            if (orderType == 0) {//newests
                order = 'sm.id'
            } else if (orderType == 1) {//oldest
                order = 'sm.id'
            } else if (orderType == 2) {//upppss
                order = 'sm.ups'
            }
            const messages = await em.createQueryBuilder(SocietyMessages, 'sm')
                .leftJoin(Users, 'u', 'u.id = sm.userId')
                .select('sm.id', 'id')
                .addSelect('sm.userId', 'userId')
                .addSelect('sm.message', 'message')
                .addSelect('sm.image', 'image')
                .addSelect('sm.dateCreate', 'dateCreate')
                .addSelect('sm.ups', 'ups')
                .addSelect('sm.downs', 'downs')

                .addSelect('sm.replyId', 'replyId')
                .addSelect('sm.replyCreatorName', 'replyCreatorName')
                .addSelect('sm.replyCreatorId', 'replyCreatorId')
                .addSelect('sm.replyMessage', 'replyMessage')
                .addSelect('sm.replyImage', 'replyImage')

                .addSelect('u.name', 'senderName')
                .addSelect('u.avatar', 'senderAvatar')
                .addSelect('u.city', 'senderCity')
                .addSelect('u.job', 'senderJob')
                .addSelect(selectIUp)
                .addSelect(selectIDown)
                .where('sm.topicId = ' + id)
                .orderBy(order, orderType == 0 ? 'DESC' : orderType == 1 ? 'ASC' : 'DESC')
                .limit(20)
                .offset((page - 1) * 20)
                .getRawMany()
            return { topic: { iUpTopic, iDownTopic, ...topic }, messages }
        } else {
            throw new NotFoundException('موضوع مورد نظر پیدا نشد.')
        }
    }

    async getUserTopics(user: Users, page: number, word: string): Promise<any> {
        const selectIUp = '(select "isUp" from "societyUpAndDowns" where "userId" = ' + user.id + ' and "contentType" = 1 and "contentId" = st.id and "isUp" is true) as "iUp"'
        const selectIDown = '(select "isUp" is false from "societyUpAndDowns" where "userId" = ' + user.id + ' and "contentType" = 1 and "contentId" = st.id and "isUp" is false) as "iDown"'

        var where = 'true'
        if (word && word.length > 0) {
            where = "st.title like '%" + word + "%'"
        }

        const topics = await getManager().createQueryBuilder(SocietyTopics, 'st')
            .leftJoin(Users, 'u', 'u.id = st.makerId')
            .select('st.id', 'id')
            .addSelect('st.catId', 'catId')
            .addSelect('st.title', 'title')
            .addSelect('st.description', 'description')
            .addSelect('st.banner', 'banner')
            .addSelect('st.dateCreate', 'dateCreate')
            .addSelect('st.ups', 'ups')
            .addSelect('st.downs', 'downs')
            .addSelect('st.messagesCount', 'messagesCount')
            .addSelect('st.seenCount', 'seenCount')
            .addSelect('st.makerId', 'makerId')
            .addSelect('u.name', 'makerName')
            .addSelect('u.avatar', 'makerAvatar')
            .addSelect('u.job', 'makerJob')
            .addSelect('u.city', 'makerCity')
            .addSelect(selectIUp)
            .addSelect(selectIDown)
            .where('st.makerId = ' + user.id + ' and st.isAvailable is true')
            .andWhere(where)
            .limit(20)
            .offset((page - 1) * 20)
            .orderBy('st.newMessagesCount', 'DESC')
            .getRawMany()

        return topics
    }

    async removeTopic(user: Users, id: number): Promise<any> {
        const topicBefore = await getManager().findOne(SocietyTopics, { id: id, makerId: user.id })
        if (!topicBefore) {
            throw new NotFoundException('این تاپیک ز بر شما یافت نشد.')
        } else {
            topicBefore.isAvailable = false
            topicBefore.removeDate = new Date()
            await topicBefore.save()
        }
    }

}
