import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, Double, JoinColumn } from 'typeorm';


@Entity('societyTopics')
export class SocietyTopics extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    catId: number;

    @Column()
    makerId: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    dateCreate: Date;

    @Column()
    banner: string;

    @Column()
    ups: number;

    @Column()
    downs: number;

    @Column()
    seenCount: number;

    @Column()
    messagesCount: number;

    // for creator knowing
    @Column()
    newMessagesCount: number;

    @Column('bool')
    isAvailable: boolean;

    @Column()
    removeDate: Date;
}