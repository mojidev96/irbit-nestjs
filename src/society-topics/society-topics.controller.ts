import { SocietyTopicsService } from './society-topics.service';
import { Users } from 'src/users/users.entity';
import { AuthGuard } from '@nestjs/passport';
import { Body, Controller, Header, Post, UploadedFile, UseGuards, UseInterceptors, Headers, Delete } from '@nestjs/common';
import { GetUser } from 'src/users/getuser.decorator';
import { TopicDTO } from './society-topics.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('societyTopics')
export class SocietyTopicsController {

    constructor(private topicService: SocietyTopicsService) { }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileInterceptor('image'))
    addNewTopic(
        @GetUser() user: Users,
        @Body() addDTO: TopicDTO,
        @UploadedFile() file
    ): Promise<any> {
        return this.topicService.addNewTopic(user, addDTO, file == null ? '' : file.path)
    }

    @Post('list')
    getTopics(
        @Body('page') page: number,
        @Body('catId') catId: number,
        @Body('word') word: string,
        @Body('orderType') orderType: number,
        @Headers('token') token: string,
    ): Promise<any> {
        return this.topicService.getTopics(token, page, catId, word, orderType)
    }

    @Post('mine')
    @UseGuards(AuthGuard('jwt'))
    getUserTopics(
        @Body('page') page: number,
        @Body('word') word: string,
        @GetUser() user: Users,
    ): Promise<any> {
        return this.topicService.getUserTopics(user, page, word)
    }

    @Delete()
    @UseGuards(AuthGuard('jwt'))
    removeTopic(
        @GetUser() user: Users,
        @Body('id') id: number,
    ): Promise<any> {
        return this.topicService.removeTopic(user, id)
    }

    @Post('byId')
    getTopicById(
        @Body('id') id: number,
        @Body('page') page: number,
        @Body('orderType') orderType: number,
        @Headers('token') token: string
    ): Promise<any> {
        return this.topicService.getTopicById(token, id, page, orderType)
    }

}
