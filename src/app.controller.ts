import { Controller, Get, Param, Res } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('files/:fileId')
  async serveAvatar(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'files' });
  }

  @Get('legalFiles/:fileId')
  async serveLegals(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'legalFiles' });
  }

  @Get('markets/:fileId')
  async serveMarkets(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'markets' });
  }

  @Get('messagesFiles/:fileId')
  async serveMessagesFiles(@Param('fileId') fileId, @Res() res): Promise<any> {
    res.sendFile(fileId, { root: 'messagesFiles' });
  }

  @Get('test')
  test() {
    return new Date('2021-02-13T17:40:00.000Z')
    // return 'mojtaba mohebi 2'.split(' ').join('_')
  }
}
