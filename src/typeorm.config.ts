import { Posts } from './posts/posts.entity';
import { CoinMessages } from './coin-messages/coin-messages.entity';
import { Coins } from './coins/coins.entity';
import { Comments } from './comments/comments.entity';
import { UpAndDowns } from './up-and-downs/up-and-downs.entity';
import { Tutorials } from './tutorials/tutorials.entity';
import { TutorialCategories } from './tutorial-categories/tutorials-categories.entity';
import { NewsCategories } from './news-categories/news-categories.entity';
import { News } from './news/news.entity';
import { SampleCafes } from './sample-cafes/samle-cafes.entity';
import { SamplePlans } from './sample-plans/sample-plans.entity';
import { FreePlanCodes } from './free-plan-codes/free-plan-codes.entity';
import { PlanParts } from './plan-parts/planParts.entity';
import { Planers } from './planers/planers.entity';
import { Plans } from './plans/plans.entity';
import { SocietyUpAndDowns } from './society-up-and-downs/society-up-and-downs.entity';
import { SocietyTopics } from './society-topics/society-topics.entity';
import { SocietyMessages } from './society-messages/society-messages.entity';
import { SocietyCategories } from './society-categories/society-categories.entity';
import { Tokens } from './tokens/tokens.entity';
import { Updates } from './updates/updates.entity';
import { Chats } from './chats/chats.entity';
import { Users } from './users/users.entity';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Banners } from './banners/banners.entity';
import { Messages } from './messages/messages.entity';
import { Reports } from './reports/reports.entity';
import { Locations } from './locations/locations.entity';
import { Admin } from './admin/admin.entity';
import { Payments } from './payments/payments.entity';
import { BlockList } from './block-list/block-list.entity';
import * as dotenv from 'dotenv';
import { Follows } from './follows/follows.entity';
import { PostLikes } from './post-likes/post-likes.entity';

// type: 'postgres',
// host: 'localhost',
// port: 5432,
// username: 'test',
// password: 'test',
// database: 'postgres',

dotenv.config();
export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'postgres',
    host: process.env.DB_HOST || 'localhost',
    port: parseInt(process.env.DB_PORT) || 5432,
    username: process.env.DB_USERNAME || 'mojidev96', //postgres//test//mojidev96
    password: process.env.DB_PASSWORD || 'mspqrt256Byte', //postgres//test//mspqrt256Byte
    database: process.env.DB_NAME || 'satooshi',
    entities: [
        Banners,
        Users,
        Messages,
        Reports,
        Locations,
        Admin,
        Payments,
        BlockList,
        Chats,
        Updates,
        Tokens,
        SocietyCategories,
        SocietyMessages,
        SocietyTopics,
        SocietyUpAndDowns,
        Plans,
        Planers,
        PlanParts,
        FreePlanCodes,
        SamplePlans,
        SampleCafes,
        Posts,
        Follows,
        PostLikes,
        News, NewsCategories, TutorialCategories, Tutorials, UpAndDowns, Comments, Coins, CoinMessages],
    synchronize: false,
    migrations: ['dist/migration/*.js']
};
// __dirname + '/../**/*.entity.ts'
