import { TutorialCategories } from './../tutorial-categories/tutorials-categories.entity';
import { Tutorials } from './tutorials.entity';
import { Injectable, Logger } from '@nestjs/common';
import { UsersRepository } from 'src/users/users.repository';
import { getManager } from 'typeorm';

@Injectable()
export class TutorialsService {

    async getTutorials(token: string, page: number, categoryId: number, sortType: number, word: string): Promise<any> {

        const em = getManager()

        var whereCategory = 'true'
        if (categoryId && categoryId > 0) {
            whereCategory = 'n.categoryId = ' + categoryId
        }

        var sort = 'n.id'
        if (sortType && sortType == 1) { // most ups
            sort = 'n.ups'
        } else if (sortType && sortType == 2) { // most seen
            sort = 'n.seenCount'
        }

        var whereWord = 'true'
        if (word && word.length > 0) {
            whereWord = "n.title ilike '%" + word + "%'"
        }

        var selectIUp = 'false as "iUp"'
        var selectIDown = 'false as "iDown"'

        const user = await em.getCustomRepository(UsersRepository).getUserByToken(token)
        if (user) {
            selectIUp = '(select "isUp" from "upAndDowns" where "userId" = ' + user.id + ' and "contentType" = 2 and "contentId" = n.id and "isUp" is true) as "iUp"'
            selectIDown = '(select "isUp" is false from "upAndDowns" where "userId" = ' + user.id + ' and "contentType" = 2 and "contentId" = n.id and "isUp" is false) as "iDown"'
        }

        const news = await em.createQueryBuilder(Tutorials, 'n')
            .leftJoin(TutorialCategories, 'c', 'n.categoryId = c.id')
            .select('n.id', 'id')
            .addSelect('n.title', 'title')
            .addSelect('n.description', 'description')
            .addSelect('n.content', 'content')
            .addSelect('n.creator', 'creator')
            .addSelect('n.dateCreate', 'dateCreate')
            .addSelect('n.seenCount', 'seenCount')
            .addSelect('n.ups', 'ups')
            .addSelect('n.downs', 'downs')
            .addSelect('n.poster', 'poster')
            .addSelect('c.title', 'categoryName')
            .addSelect(selectIUp)
            .addSelect(selectIDown)
            .where(whereCategory)
            .andWhere(whereWord)
            .orderBy(sort, 'DESC')
            .offset(10 * (page - 1))
            .limit(10)
            .getRawMany()

        return news
    }
}
