import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn, Double } from 'typeorm';


@Entity()
export class Tutorials extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    poster: string;

    @Column()
    creator: string;

    @Column()
    categoryId: number;

    @Column()
    ups: number

    @Column()
    downs: number;

    @Column()
    content: string

    @Column()
    dateCreate: Date

    @Column()
    seenCount: number
}