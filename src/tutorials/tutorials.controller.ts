import { TutorialsService } from './tutorials.service';
import { Body, Controller, Post, Headers } from '@nestjs/common';

@Controller('tutorials')
export class TutorialsController {

    constructor(private tutorialsService: TutorialsService) { }

    @Post()
    getTutorials(
        @Body('page') page: number,
        @Body('categoryId') categoryId: number,
        @Body('sortType') sortType: number,
        @Body('word') word: string,
        @Headers('token') token: string,
    ): Promise<any> {
        return this.tutorialsService.getTutorials(token, page, categoryId, sortType, word)
    }
}
