export class TutorialDTO {
    creator: string
    title: string
    description: string
    content: string
    categoryId: number
}