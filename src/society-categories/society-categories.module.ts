import { UsersModule } from 'src/users/users.module';
import { Module } from '@nestjs/common';
import { SocietyCategoriesService } from './society-categories.service';
import { SocietyCategoriesController } from './society-categories.controller';

@Module({
  imports: [UsersModule],
  providers: [SocietyCategoriesService],
  controllers: [SocietyCategoriesController]
})
export class SocietyCategoriesModule {}
