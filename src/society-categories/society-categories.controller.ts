import { Controller, Post, Get, Headers } from '@nestjs/common';
import { SocietyCategoriesService } from './society-categories.service';

@Controller('societyCategories')
export class SocietyCategoriesController {

    constructor(private categoriesService: SocietyCategoriesService) { }

    @Get()
    getCats(
        @Headers('token') token: string
    ): Promise<any> {
        return this.categoriesService.getCats(token)
    }

}
