import { SocietyTopics } from './../society-topics/society-topics.entity';
import { SocietyCategories } from './society-categories.entity';
import { getManager } from 'typeorm';
import { Injectable, Logger } from '@nestjs/common';
import { SocietyMessages } from 'src/society-messages/society-messages.entity';
import { UsersRepository } from 'src/users/users.repository';
import { isUppercase } from 'class-validator';

@Injectable()
export class SocietyCategoriesService {

    async getCats(token: string): Promise<any> {

        var selectIUp = 'false as "iUp"'
        var selectIDown = 'false as "iDown"'
        const em = getManager()
        const user = await em.getCustomRepository(UsersRepository).getUserByToken(token)
        if (user) {
            selectIUp = '(select "isUp" from "societyUpAndDowns" where "userId" = ' + user.id + ' and "contentType" = 0 and "contentId" = sc.id and "isUp" is true) as "iUp"'
            selectIDown = '(select "isUp" is false from "societyUpAndDowns" where "userId" = ' + user.id + ' and "contentType" = 0 and "contentId" = sc.id and "isUp" is false) as "iDown"'
        }

        const cats = await getManager().createQueryBuilder(SocietyCategories, 'sc')
            .leftJoin(SocietyTopics, 'st', 'st.catId = sc.id')
            .leftJoin(SocietyMessages, 'sm', 'sm.topicId = st.id')
            .select('sc.id', 'id')
            .addSelect('sc.title', 'title')
            .addSelect('sc.icon', 'icon')
            .addSelect('sc.description', 'description')
            .addSelect('count(st)', 'topicsCount')
            .addSelect('count(sm)', 'messagesCount')
            .addSelect('case when sum(st.seenCount) is null then 0 else sum(st.seenCount) end', 'seenCount')
            .addSelect('sc.ups', 'ups')
            .addSelect('sc.downs', 'downs')
            .addSelect(selectIUp)
            .addSelect(selectIDown)
            .groupBy('sc.title, sc.index, sc.id, sc.icon, sc.description, sc.ups, sc.downs')
            .orderBy('sc.index', 'ASC')
            .getRawMany()

        return cats
    }

}
