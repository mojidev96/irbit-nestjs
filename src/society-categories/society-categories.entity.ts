import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, Double, JoinColumn } from 'typeorm';


@Entity('societyCategories')
export class SocietyCategories extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    topicsCount: number;

    @Column()
    icon: string;

    @Column()
    description: string;

    @Column()
    index: number

    @Column()
    ups: number

    @Column()
    downs: number

}