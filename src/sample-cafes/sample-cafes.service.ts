import { SampleCafes } from './samle-cafes.entity';
import { Users } from './../users/users.entity';
import { SampleCafe, SamplePlan } from './sample-cafes.controller';
import { Injectable, UnauthorizedException } from '@nestjs/common';

@Injectable()
export class SampleCafesService {

    async addSampleCafe(user: Users, sample: SampleCafe): Promise<any> {
        if (user.phone == '09393022461') {
            const newSample = new SampleCafes()
            newSample.address = sample.address
            newSample.city = sample.city
            newSample.location = sample.location
            newSample.placeName = sample.placeName
            await newSample.save()
            return { message: 'job done' }
        } else {
            throw new UnauthorizedException('just939')
        }
    }
}
