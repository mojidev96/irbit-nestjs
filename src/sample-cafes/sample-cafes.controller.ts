import { Double } from 'typeorm';
import { SampleCafesService } from './sample-cafes.service';
import { Users } from './../users/users.entity';
import { AuthGuard } from '@nestjs/passport';
import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { GetUser } from 'src/users/getuser.decorator';

export class SamplePlan {
    title: string
    description: string
    poster: string
    minMembers: number
    maxMembers: number
    ticketPrice: number
    category: string
    planTime: number
}

export class SampleCafe {
    city: string
    address: string
    location: Double[]
    placeName: string
}

@Controller('sampleCafes')
export class SampleCafesController {
    constructor(private sampleCafesService: SampleCafesService) { }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    addNewSampleCafe(
        @GetUser() user: Users,
        @Body() sample: SampleCafe
    ): Promise<any> {
        return this.sampleCafesService.addSampleCafe(user, sample)
    }

}
