import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, Double } from 'typeorm';

@Entity('sampleCafes')
export class SampleCafes extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    city: string;

    @Column()
    address: string;

    @Column({ type: 'double precision', array: true })
    location: Double[];

    @Column()
    placeName: string;

}