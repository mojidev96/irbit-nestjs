import { UsersModule } from 'src/users/users.module';
import { Module } from '@nestjs/common';
import { SampleCafesService } from './sample-cafes.service';
import { SampleCafesController } from './sample-cafes.controller';

@Module({
  imports: [UsersModule],
  providers: [SampleCafesService],
  controllers: [SampleCafesController]
})
export class SampleCafesModule {}
