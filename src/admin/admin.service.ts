import { TutorialDTO } from './../tutorials/tutorials.dto';
import { Tutorials } from './../tutorials/tutorials.entity';
import { TutorialCategories } from './../tutorial-categories/tutorials-categories.entity';
import { NewsDTO } from './../news/news.dto';
import { News } from './../news/news.entity';
import { NewsCategories } from './../news-categories/news-categories.entity';
import { SampleCafes } from './../sample-cafes/samle-cafes.entity';
import { SampleCafe } from './../sample-cafes/sample-cafes.controller';
import { SamplePlans } from './../sample-plans/sample-plans.entity';
import { Plans } from './../plans/plans.entity';
import { SocietyCategories } from './../society-categories/society-categories.entity';;
import { Messages } from './../messages/messages.entity';
import { Admin } from './admin.entity';
import { getManager, MoreThan } from 'typeorm';
import { Users } from './../users/users.entity';
import { Injectable, NotFoundException, Logger, HttpService, UnauthorizedException } from '@nestjs/common';

import axios from 'axios'
import { firebaseServerKey } from 'src/app.module';
import { AddSocietyCatDTO, PlanDTO } from './admin.dto';

@Injectable()
export class AdminService {

    constructor(private http: HttpService) { }



    sendChangeStoreStateNotification(title: string, body: string, getterToken: string, getterWebFCMToken: string, getterId: number, status: number) {
        axios.post('https://fcm.googleapis.com/fcm/send', {
            'notification': {
                'body': body,
                'title': title
            },
            'priority': 'high', 'data': {
                'click_action': 'FLUTTER_NOTIFICATION_CLICK',
                'type': 'store-state',
                'fromId': getterId,
                'status': status
            },
            'to': getterToken
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': firebaseServerKey,
            }
        }).catch(error => {
            Logger.error('fcm send order state:: ' + error)
        })

        axios.post('https://fcm.googleapis.com/fcm/send', {
            'notification': {
                'body': body,
                'title': title
            },
            'priority': 'high', 'data': {
                'click_action': 'FLUTTER_NOTIFICATION_CLICK',
                'type': 'store-state',
                'fromId': getterId,
                'status': status
            },
            'to': getterWebFCMToken
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': firebaseServerKey,
            }
        }).catch(error => {
            Logger.error('fcm send order state:: ' + error)
        })
    }


    async checkUserIsAdmin(phone: string): Promise<boolean> {
        const admin = await getManager().findOne(Admin, { phone: phone })
        return admin ? true : false
    }


    async addSocietyCategory(user: Users, addDTO: AddSocietyCatDTO, iconFilePath: string): Promise<any> {
        const isAdmin = await this.checkUserIsAdmin(user.phone)
        if (isAdmin) {
            const cat = new SocietyCategories()
            cat.title = addDTO.title
            cat.description = addDTO.description
            cat.index = addDTO.index
            cat.topicsCount = 0
            cat.icon = iconFilePath
            await cat.save()
            return cat
        } else {
            throw new UnauthorizedException('you are not admin')
        }
    }

    async addPlan(user: Users, planDTO: PlanDTO, posterPath: string): Promise<any> {
        const isAdmin = await this.checkUserIsAdmin(user.phone)
        if (isAdmin) {
            const plan = new Plans()
            plan.title = planDTO.title
            plan.description = planDTO.description
            plan.city = planDTO.city
            plan.category = planDTO.category
            plan.address = planDTO.address
            plan.location = planDTO.location
            plan.poster = posterPath
            plan.minMembers = planDTO.minMembers
            plan.maxMembers = planDTO.maxMembers
            plan.date = planDTO.date
            plan.planTime = planDTO.planTime
            plan.ticketPrice = planDTO.ticketPrice
            plan.placeName = planDTO.placeName

            await plan.save()
            return { message: 'job done' }
        } else {
            throw new UnauthorizedException('not admin')
        }
    }

    async setPlansFromSamples(user: Users, upId: number): Promise<any> {
        const isAdmin = await this.checkUserIsAdmin(user.phone)
        if (isAdmin) {
            const samplePlans = await getManager().find(SamplePlans)
            const sampleCafes = await getManager().find(SampleCafes, { id: MoreThan(upId) })

            const date = new Date()
            const times = ['11:30:00.000Z', '10:30:00.000Z', '17:30:00.000Z']
            const dates = []
            for (let index = 0; index < 100; index++) {
                var d = new Date()
                d.setDate(d.getDate() + index)
                dates.push(d)
            }

            for await (const cafe of sampleCafes) {
                for await (const date of dates) {
                    const plan = new Plans()
                    const randomPlan = samplePlans[Math.floor(Math.random() * samplePlans.length)]

                    plan.address = cafe.address
                    plan.description = randomPlan.description
                    plan.category = randomPlan.category
                    plan.city = cafe.city
                    plan.date = new Date(date.toISOString().substring(0, 10) + 'T' + times[Math.floor(Math.random() * times.length)])
                    plan.location = cafe.location
                    plan.maxMembers = randomPlan.maxMembers
                    plan.minMembers = randomPlan.minMembers
                    plan.placeName = cafe.placeName
                    plan.planTime = randomPlan.planTime
                    plan.poster = randomPlan.poster
                    plan.ticketPrice = randomPlan.ticketPrice
                    plan.title = randomPlan.title
                    plan.seenCount = 0
                    plan.membersCount = 0
                    await plan.save()
                }
            }

        } else {
            throw new UnauthorizedException('not admin')
        }
    }

    async addOrRemoveNewsCategory(user: Users, title: string, removeId: number): Promise<any> {

        const isAdmin = await this.checkUserIsAdmin(user.phone)
        if (isAdmin) {
            if (title) {
                const newCategory = new NewsCategories
                newCategory.title = title
                await newCategory.save()
            }

            if (removeId) {
                await getManager().update(News, { categoryId: removeId }, { categoryId: 0 })
                await getManager().delete(NewsCategories, { id: removeId })
            }

            return 'job done'

        } else {
            throw new UnauthorizedException('not admin')
        }

    }

    async addNews(user: Users, news: NewsDTO, posterPath: string): Promise<any> {

        const isAdmin = await this.checkUserIsAdmin(user.phone)
        if (isAdmin) {
            const category = await getManager().findOne(NewsCategories, { id: news.categoryId })
            if (!category) {
                throw new NotFoundException('دسته بندی پیدا نشد')
            }

            const n = new News()
            n.title = news.title
            n.description = news.description
            n.author = news.author
            n.dateCreate = new Date()
            n.content = news.content
            n.categoryId = news.categoryId
            n.sourceUrl = news.sourceUrl
            n.poster = posterPath

            await n.save()

            return 'job done'
        }
        else {
            throw new UnauthorizedException('not admin')
        }
    }

    async addOrRemoveTutorialCategory(user: Users, title: string, removeId: number): Promise<any> {

        const isAdmin = await this.checkUserIsAdmin(user.phone)
        if (isAdmin) {
            if (title) {
                const newCategory = new TutorialCategories
                newCategory.title = title
                await newCategory.save()
            }

            if (removeId) {
                await getManager().update(Tutorials, { categoryId: removeId }, { categoryId: 0 })
                await getManager().delete(TutorialCategories, { id: removeId })
            }

            return 'job done'

        } else {
            throw new UnauthorizedException('not admin')
        }

    }

    async addTutorial(user: Users, tutorial: TutorialDTO, posterPath: string): Promise<any> {

        const isAdmin = await this.checkUserIsAdmin(user.phone)
        if (isAdmin) {
            const category = await getManager().findOne(TutorialCategories, { id: tutorial.categoryId })
            if (!category) {
                throw new NotFoundException('دسته بندی پیدا نشد')
            }

            const t = new Tutorials()
            t.title = tutorial.title
            t.description = tutorial.description
            t.creator = tutorial.creator
            t.dateCreate = new Date()
            t.content = tutorial.content
            t.categoryId = tutorial.categoryId
            t.poster = posterPath

            await t.save()

            return 'job done'
        }
        else {
            throw new UnauthorizedException('not admin')
        }
    }
}
