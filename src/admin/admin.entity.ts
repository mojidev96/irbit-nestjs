import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('admin')
export class Admin extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    phone: string

    @Column()
    dateCreate: Date
}