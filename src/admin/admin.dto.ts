import { Double } from 'typeorm';
export class AddSocietyCatDTO{
    title: string
    description: string
    index: number
}

export class PlanDTO {
    title: string
    description: string
    city: string
    address: string
    category: string
    location: Double[]
    minMembers: number
    maxMembers: number
    date: Date
    planTime: number
    ticketPrice: number
    placeName: string
}