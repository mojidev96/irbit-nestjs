import { TutorialDTO } from './../tutorials/tutorials.dto';
import { NewsDTO } from './../news/news.dto';
import { FilesInterceptor, FileInterceptor } from '@nestjs/platform-express';
import { Users } from 'src/users/users.entity';
import { GetUser } from './../users/getuser.decorator';
import { AdminService } from './admin.service';
import { Controller, Post, UseGuards, Body, UseInterceptors, UploadedFiles, UploadedFile } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { userInfo } from 'os';
import { AddSocietyCatDTO, PlanDTO } from './admin.dto';

@Controller('admin')
export class AdminController {

    constructor(private adminService: AdminService) { }

    @Post('addPlan')
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileInterceptor('poster'))
    addPlan(
        @GetUser() user: Users,
        @Body() addPlan: PlanDTO,
        @UploadedFile() file
    ): Promise<{ message: string }> {
        return this.adminService.addPlan(user, addPlan, file.path)
    }

    @Post('addOrRemoveNewsCategory')
    @UseGuards(AuthGuard('jwt'))
    addOrRemoveNewsCategory(
        @GetUser() user: Users,
        @Body('title') title: string,
        @Body('removeId') removeId: number,
    ): Promise<{ message: string }> {
        return this.adminService.addOrRemoveNewsCategory(user, title, removeId)
    }

    @Post('addNews')
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileInterceptor('poster'))
    addNews(
        @GetUser() user: Users,
        @Body() news: NewsDTO,
        @UploadedFile() file
    ): Promise<{ message: string }> {
        return this.adminService.addNews(user, news, file.path)
    }

    @Post('addOrRemoveTutorialCategory')
    @UseGuards(AuthGuard('jwt'))
    addOrRemoveTutorialCategory(
        @GetUser() user: Users,
        @Body('title') title: string,
        @Body('removeId') removeId: number,
    ): Promise<{ message: string }> {
        return this.adminService.addOrRemoveTutorialCategory(user, title, removeId)
    }

    @Post('addTutorial')
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileInterceptor('poster'))
    addTutorial(
        @GetUser() user: Users,
        @Body() tutorial: TutorialDTO,
        @UploadedFile() file
    ): Promise<{ message: string }> {
        return this.adminService.addTutorial(user, tutorial, file.path)
    }

    @Post('addSocietyCategory')
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileInterceptor('image'))
    addSocietyCategory(
        @GetUser() user: Users,
        @Body() addDTO: AddSocietyCatDTO,
        @UploadedFile() file
    ): Promise<{ message: string }> {
        return this.adminService.addSocietyCategory(user, addDTO, file == null ? '' : file.path)
    }

    @Post('setPlansFromSamples')
    @UseGuards(AuthGuard('jwt'))
    setPlansFromSamples(
        @GetUser() user: Users,
        @Body('upId') upId: number
    ): Promise<any> {
        return this.adminService.setPlansFromSamples(user, upId)
    }

}
