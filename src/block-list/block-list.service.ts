import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Users } from '../users/users.entity';
import { getManager } from 'typeorm';
import { BlockList } from './block-list.entity';

@Injectable()
export class BlockListService {

    async blockUnblockUser(token: string, blockUserId: number): Promise<{isBlocked: boolean, fromUserId: number}> {
        const em = getManager()
        const user = await em.findOne(Users, {token: token})
        if(!user){
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.')
        }
        var isBlocked = false;
        const blockBefore = await em.findOne(BlockList, { userId: user.id, blockUserId: blockUserId })
        if (!blockBefore) {
            const block = new BlockList()
            block.userId = user.id
            block.blockUserId = blockUserId
            block.dateCreate = new Date()
            await block.save()
            await getManager().createQueryBuilder()
            .update(Users)
            .set({
              blockCount: () => '"blockCount" + 1',
              blockCountInDay: () => '"blockCountInDay" + 1',
              blockCountInMonth: () => '"blockCountInMonth" + 1'
            })
            .where('id = ' + blockUserId)
            .execute()
            isBlocked = true
        }else{
            await em.delete(BlockList, { userId: user.id, blockUserId: blockUserId })
            isBlocked = false
        }

        return {isBlocked, fromUserId: user.id}
    }

    // async unblockUser(token: string, blockUserId: number): Promise<{ message: string }> {    const em = getManager()
    //     const user = await em.findOne(Users, {token: token})
    //     if(!user){
    //         throw new UnauthorizedException()
    //     }
    //     await em.delete(BlockList, { userId: user.id, blockUserId: blockUserId })
    //     return { message: 'انجام شد.' }
    // }

    async getUserBlockList(user: Users): Promise<Users[]> {
        const users = await getManager().createQueryBuilder(Users, 'u')
            .innerJoin(BlockList, 'b', 'u.id = b.blockUserId')
            .select('u.name', 'name')
            .addSelect('u.avatar', 'avatar')
            .addSelect('u.username', 'username')
            .addSelect('u.id', 'id')
            .where('b.userId = ' + user.id)
            .getRawMany()
        return users
    }

}
