import { Controller, Post, UseGuards, Body, Get } from '@nestjs/common';
import { BlockListService } from './block-list.service';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/users/getuser.decorator';
import { Users } from '../users/users.entity';

@Controller('blockList')
export class BlockListController {

    constructor(private blockListService: BlockListService) { }

    // @Post('block')
    // @UseGuards(AuthGuard('jwt'))
    // blockUser(
    //     @GetUser() user: Users,
    //     @Body('blockUserId') blockUserId: number
    // ): Promise<{ message: string }> {
    //     return this.blockListService.blockUser(user, blockUserId)
    // }

    // @Post('unblock')
    // @UseGuards(AuthGuard('jwt'))
    // unblockUser(
    //     @GetUser() user: Users,
    //     @Body('blockUserId') blockUserId: number
    // ): Promise<{ message: string }> {
    //     return this.blockListService.unblockUser(user, blockUserId)
    // }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    getUserBlockList(
        @GetUser() user: Users
    ): Promise<Users[]> {
        return this.blockListService.getUserBlockList(user)
    }
}
