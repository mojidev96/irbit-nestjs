import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn, Double } from 'typeorm';


@Entity('posts')
export class Posts extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userId: number;

    @Column()
    title: string;

    @Column()
    caption: string;

    @Column()
    likesCount: number;

    @Column()
    commentsCount: number;

    @Column()
    posterUrl: string;

    @Column()
    videoUrl: number;

    @Column()
    dateCreate: Date
}