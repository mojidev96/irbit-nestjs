import { PostDTO } from './posts.dto';
import { PostsService } from './posts.service';
import { Users } from 'src/users/users.entity';
import { Body, Controller, Get, Headers, Post, Put, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileFieldsInterceptor, FileInterceptor } from '@nestjs/platform-express/multer';
import { GetUser } from 'src/users/getuser.decorator';
import { diskStorage } from 'multer'
import { Posts } from './posts.entity';

@Controller('posts')
export class PostsController {

    constructor(private postsService: PostsService) { }

    @Put()
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'video', maxCount: 1 },
        { name: 'poster', maxCount: 1 },
    ], { storage: diskStorage({ destination: './files', limits: { fieldSize: 100000000 } }) }))
    addPost(
        @GetUser() user: Users,
        @Body() postDTO: PostDTO,
        @UploadedFiles() files: any
    ): Promise<any> {
        return this.postsService.addPost(user, postDTO, files)
    }

    @Post('byId')
    getPost(
        @Body('id') id: number,
        @Headers('token') token: string
    ): Promise<any> {
        return this.postsService.getPostById(token, id)
    }

    @Post('explore')
    @UseGuards(AuthGuard('jwt'))
    getExplorer(
        @GetUser() user: Users,
        @Body('page') page: number
    ): Promise<any>{
        return this.postsService.getExplorer(user, page)
    }
}
