import { Posts } from './posts.entity';
import { PostDTO } from './posts.dto';
import { Users } from 'src/users/users.entity';
import { Injectable, Logger, Post } from '@nestjs/common';
import { getManager } from 'typeorm';
import ThumbnailGenerator from 'video-thumbnail-generator';
import { UsersRepository } from 'src/users/users.repository';
import { PostLikes } from 'src/post-likes/post-likes.entity';
import { Comments } from 'src/comments/comments.entity';
import { Follows } from 'src/follows/follows.entity';

@Injectable()
export class PostsService {

    async addPost(user: Users, postDTO: PostDTO, files: any): Promise<any> {

        const newPost = new Posts()
        newPost.userId = user.id
        newPost.title = postDTO.title
        newPost.caption = postDTO.caption
        newPost.dateCreate = new Date()
        newPost.posterUrl = null

        if (files['poster'] && files['poster'][0].path) {
            newPost.posterUrl = files['poster'][0].path
        }

        user.postsCount += 1
        await user.save()

        if (files['video'] && files['video'][0].path) {
            newPost.videoUrl = files['video'][0].path
            newPost.posterUrl = await this.getVideoThumbnail(files['video'][0].path)
            await newPost.save()
            return newPost

        } else {
            await newPost.save()
            return newPost
        }
    }

    async getVideoThumbnail(path: string): Promise<string> {
        var url = ''
        const tg = new ThumbnailGenerator({
            sourcePath: path,
            thumbnailPath: './files',
            tmpDir: './files'
        });

        return new Promise(function (resolve) {
            tg.generateOneByPercentCb(50, (err, result) => {
                url = 'files/' + result.toString()
                resolve(url)
            });
        });
    }

    async getPostById(token: string, id: number): Promise<any> {
        const em = getManager()
        var iLike = false
        if (token) {
            const user = await em.getCustomRepository(UsersRepository).getUserByToken(token)
            const likeBefore = await em.findOne(PostLikes, { userId: user.id, postId: id })
            iLike = likeBefore != undefined
        }

        const post = await em.findOne(Posts, { id })
        // const commentsCount = await em.createQueryBuilder(Comments, 'c')
        //     .select('count(*)', 'count')
        //     .where('c.contentType = 3 and c.contentId = ' + id + ' and c.isAccepted is true and c.isDeleted is false')
        //     .getRawOne()
        // post.commentsCount = commentsCount.count

        return { ...post, iLike }
    }

    async getExplorer(user: Users, page: number): Promise<any> {
        return await getManager().createQueryBuilder(Posts, 'p')
            .leftJoin(Follows, 'f', 'f.followerId = ' + user.id)
            .leftJoin(Users, 'u', 'u.id = f.followingId')
            .select('p.id', 'id')
            .addSelect('p.title', 'title')
            .addSelect('p.caption', 'caption')
            .addSelect('p.videoUrl', 'videoUrl')
            .addSelect('p.posterUrl', 'posterUrl')
            .addSelect('p.dateCreate', 'dateCreate')
            .addSelect('p.likesCount', 'likesCount')
            .addSelect('p.commentsCount', 'commentsCount')
            .addSelect('(select id is not null as "iLike" from "postLikes" where "userId" = '+user.id+' and "postId" = p.id)')
            .orderBy('p.id', 'DESC')
            .limit(10)
            .offset(10 * (page - 1))
            .getRawMany()
    }

}
