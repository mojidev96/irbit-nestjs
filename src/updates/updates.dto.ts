export class AddUpdateDTO{
    code: number
    name: string
    osType: number
    description: string
    isForce: boolean
    downloadLink: string
}