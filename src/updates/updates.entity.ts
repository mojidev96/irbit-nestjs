import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity({ name: 'updates' })
export class Updates extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: number;

    @Column()
    name: string;

    @Column()
    osType: number;

    @Column()
    description: string;

    @Column('bool')
    isForce: boolean;

    @Column()
    downloadLink: string

}