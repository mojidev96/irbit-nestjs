import { AddUpdateDTO } from './updates.dto';
import { AuthGuard } from '@nestjs/passport';
import { UpdatesService } from './updates.service';
import { Controller, Post, UseGuards, Body, Delete } from '@nestjs/common';
import { GetUser } from 'src/users/getuser.decorator';
import { Users } from 'src/users/users.entity';

@Controller('updates')
export class UpdatesController {

    constructor(private updatesService: UpdatesService){}

    @Post()
    @UseGuards(AuthGuard('jwt'))
    addUpdate(@GetUser() user: Users,
    @Body() addDTO: AddUpdateDTO): Promise<any>{
        return this.updatesService.addUpdate(user, addDTO)
    }

    @Delete()
    @UseGuards(AuthGuard('jwt'))
    deleteUpdate(@GetUser() user: Users,
    @Body('id') id: number): Promise<any>{
        return this.updatesService.deleteUpdate(user, id)
    }

}
