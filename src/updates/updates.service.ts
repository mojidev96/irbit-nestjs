import { AddUpdateDTO } from './updates.dto';
import { Users } from './../users/users.entity';
import { Updates } from './updates.entity';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { getManager } from 'typeorm';
import { Admin } from 'src/admin/admin.entity';

@Injectable()
export class UpdatesService {

    async addUpdate(user: Users, addDTO: AddUpdateDTO): Promise<any>{
        const isAdmin = await this.checkUserIsAdmin(user.phone)
        if(isAdmin){
            const newUpdate = new Updates()
            newUpdate.code = addDTO.code
            newUpdate.description = addDTO.description
            newUpdate.isForce = addDTO.isForce
            newUpdate.name = addDTO.name
            newUpdate.osType = addDTO.osType
            newUpdate.downloadLink = addDTO.downloadLink
            await newUpdate.save()
            return true
        }else{
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.')
        }
    }

    async deleteUpdate(user: Users, id: number): Promise<any> {
        const isAdmin = await this.checkUserIsAdmin(user.phone)
        if(isAdmin){
            await getManager().delete(Updates, {id: id})
            return true
        }else{
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.')
        }
    }

    async checkUserIsAdmin(phone: string): Promise<boolean> {
        const admin = await getManager().findOne(Admin, { phone: phone })
        return admin ? true : false
    }
}
