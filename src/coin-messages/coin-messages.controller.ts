import { CoinMessagesService } from './coin-messages.service';
import { Controller, Post, UseGuards, UseInterceptors, Body, Logger, UploadedFiles, Get, Delete, UnauthorizedException, Headers } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { Chats } from 'src/chats/chats.entity';
import { MessagesService } from 'src/messages/messages.service';
import { GetUser } from 'src/users/getuser.decorator';
import { Users } from 'src/users/users.entity';
import { CoinMessages } from './coin-messages.entity';
import { diskStorage } from 'multer'

@Controller('coinMessages')
export class CoinMessagesController {
    constructor(private messageService: CoinMessagesService) { }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileFieldsInterceptor([
        { name: 'image', maxCount: 1 },
        { name: 'file', maxCount: 1 },
        { name: 'voice', maxCount: 1 },
    ], { storage: diskStorage({ destination: './messagesFiles', limits: { fieldSize: 25600000 } }) }))
    async uploadMessageFiles(
        @GetUser() user: Users,
        @UploadedFiles() files): Promise<any> {
        if (!user) {
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.');
        }
        var imagePath: string = ''
        var voicePath: string = ''
        var filePath: string = ''
        var file_filename: string = ''
        var file_fileSize: number = 0

        if (files && files.image && files.image.length > 0) {
            imagePath = files.image[0].path
        }
        if (files && files.voice && files.voice.length > 0) {
            voicePath = files.voice[0].path
            // درست کار نکرد
            // var mp3Duration = await require('mp3-duration');
            // await mp3Duration(voicePath, function (err, duration) {
            //     if (err) return console.log(err.message);
            //     Logger.log('oooii:: ' + duration)
            //     voiceDuration = duration.toFixed(0)
            // });
        }
        if (files && files.file && files.file.length > 0) {
            filePath = files.file[0].path
            file_filename = files.file[0].originalname
            file_fileSize = files.file[0].size
        }

        Logger.log('sfasdas :: ' + voicePath)

        return { imagePath, voicePath, filePath, 'filename': file_filename, 'filesize': file_fileSize }

    }

    @Post('friendInfo')
    getFriendInfo(@Body('friendId') friendId: number
    ): Promise<any> {
        return this.messageService.getFriendInfo(friendId)
    }

    @Post('conversation')
    // @UseGuards(AuthGuard('jwt'))
    getConversationMessages(
        @Body('coinId') coinId: number,
        @Body('page') page: number
    ): Promise<{ blockStatus: number, messages: CoinMessages[] }> {
        return this.messageService.getConversationMessages(coinId, page)
    }

    @Post('getById')
    @UseGuards(AuthGuard('jwt'))
    getMessageById(
        @Body('messageId') messageId: number,
    ): Promise<CoinMessages> {
        return this.messageService.getMessageById(messageId)
    }

    // @Delete('delete')
    // @UseGuards(AuthGuard('jwt'))
    // deleteMessage(@GetUser() user: Users,
    //     @Body('messageId') messageId: number): Promise<{ message: string }> {
    //     return this.messageService.deleteMessage(user, messageId)
    // }

    @Delete('deleteChat')
    @UseGuards(AuthGuard('jwt'))
    deleteChat(@GetUser() user: Users,
        @Body('friendId') friendId: number): Promise<{ message: string }> {
        return this.messageService.deleteChat(user, friendId)
    }

    // @Post('block')
    // @UseGuards(AuthGuard('jwt'))
    // blockUser(@GetUser() user: Users,
    //     @Body('page') page: number): Promise<CoinMessages[]> {
    //     return this.messageService.getChats(user, page)
    // }
}

