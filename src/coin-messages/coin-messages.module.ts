import { Module } from '@nestjs/common';
import { CoinMessagesService } from './coin-messages.service';
import { CoinMessagesController } from './coin-messages.controller';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [UsersModule],
  providers: [CoinMessagesService],
  controllers: [CoinMessagesController]
})
export class CoinMessagesModule { }
