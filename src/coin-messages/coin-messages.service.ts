import { firebaseServerKey } from './../app.module';
import { Chats } from './../chats/chats.entity';
import { Injectable, ConflictException, UnauthorizedException, Logger, NotFoundException, HttpService } from '@nestjs/common';
import { Users } from '../users/users.entity';
import { getManager } from 'typeorm';
import { BlockList } from '../block-list/block-list.entity';
import { Socket, Server } from 'socket.io';
import axios from 'axios'
import { UsersRepository } from 'src/users/users.repository';
import { CoinMessages } from './coin-messages.entity';
import { CoinMessageDTO } from './coin-messages.dto';

@Injectable()
export class CoinMessagesService {

    constructor(private http: HttpService) { }

    public socket: Server = null;

    async sendMessage(
        messageDTO: CoinMessageDTO,): Promise<any> {

        const em = getManager()
        const validUser = await em.getCustomRepository(UsersRepository).getUserByToken(messageDTO.senderToken)
        if (!validUser) {
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.')
        }

        // const sender = await em.findOne(Users, { id: messageDTO.senderUserId })

        // const isSenderBlock = await em
        //     .createQueryBuilder(BlockList, 'b')
        //     .where('(b.userId = ' + messageDTO.senderUserId + ' and b.blockUserId = ' + messageDTO.getterUserId + ') or (b.blockUserId = ' + messageDTO.senderUserId + ' and b.userId = ' + messageDTO.getterUserId + ')')
        //     .getOne()
        // if (isSenderBlock) {
        //     throw new ConflictException('مشکل بلاک دارید.')
        // }

        const newM = new CoinMessages()

        const dateCreate = new Date(messageDTO.dateCreate)
        newM.dateCreate = dateCreate

        newM.coinId = messageDTO.coinId
        newM.message = messageDTO.message
        newM.senderUserId = messageDTO.senderUserId
        if (messageDTO.imagePath && messageDTO.imagePath.length > 0) {
            newM.imagePath = messageDTO.imagePath
        }
        if (messageDTO.voicePath && messageDTO.voicePath.length > 0) {
            newM.voicePath = messageDTO.voicePath
            newM.voiceDuration = messageDTO.voiceDuration
        }
        if (messageDTO.filePath && messageDTO.filePath.length > 0) {
            newM.filePath = messageDTO.filePath
            newM.fileName = messageDTO.filename
            newM.fileSize = messageDTO.fileSize
        }
        if (messageDTO.replyMessageId && messageDTO.replyMessageId != 0) {
            newM.isReply = true
            newM.replyMessageId = messageDTO.replyMessageId
            newM.replyMessage = messageDTO.replyMessage
        }

        await newM.save()

        return { ...newM, senderAvatar: validUser.avatar, senderName: validUser.name }
    }

    async getFriendInfo(friendId: number): Promise<any> {
        const data = await getManager().createQueryBuilder(Users, 'u')
            .select('u.name', 'name')
            .addSelect('u.avatar', 'avatar')
            .addSelect('case when u.isShowingLastOnline then u.isOnline else null end as "isOnline"')
            .addSelect('case when u.isShowingLastOnline then u.dateLastOnline else null end as "dateLastOnline"')
            .where('u.id = ' + friendId)
            .getRawOne()
        return data
    }

    async getMessageById(messageId: number): Promise<CoinMessages> {
        const mes = await getManager().findOne(CoinMessages, {
            id: messageId
        })
        return mes
    }

    //blockStatus: 1=none block, 2=block by user.id, 3=block by friend
    async getConversationMessages(coinId: number, page: number): Promise<any> {

        const em = getManager()

        const messages = await em.createQueryBuilder(CoinMessages, 'm')
            .leftJoin(Users, 'u', 'u.id = m.senderUserId')
            .select('m.id', 'id')
            .addSelect('m.senderUserId', 'senderUserId')
            .addSelect('u.name', 'senderName')
            .addSelect('u.avatar', 'senderAvatar')
            .addSelect('m.message', 'message')
            .addSelect('m.dateCreate', 'dateCreate')
            .addSelect('m.isReply', 'isReply')
            .addSelect('m.filePath', 'filePath')
            .addSelect('m.fileName', 'fileName')
            .addSelect('m.fileSize', 'fileSize')
            .addSelect('m.imagePath', 'imagePath')
            .addSelect('m.voicePath', 'voicePath')
            .addSelect('m.isEdited', 'isEdited')

            .addSelect('m.voiceDuration', 'voiceDuration')
            .addSelect('m.replyMessageId', 'replyMessageId')
            .addSelect('m.replyMessage', 'replyMessage')
            .where('m.coinId = ' + coinId + ' and m.status != 2')
            // .andWhere('m.senderUserId = :senderId and m.getterUserId = :getterId', { senderId: user.id, getterId: byUserId })
            // .orWhere('m.senderUserId = :getterId and m.getterUserId = :senderId', { senderId: user.id, getterId: byUserId })
            .orderBy('m.id', 'DESC')
            .limit(20)
            .offset(20 * (page - 1))
            .getRawMany()

        return { blockStatus: 0, messages }
    }

    async ediMessages(token: string, messageId: number, text: string): Promise<CoinMessages> {
        const em = getManager()
        const user = await em.getCustomRepository(UsersRepository).getUserByToken(token) //em .findOne(Users, { token: token })
        if (!user) {
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.')
        }
        const message = await em.findOne(CoinMessages, { id: messageId, senderUserId: user.id })
        if (!message) {
            throw new NotFoundException()
        } else {
            message.dateLastEdit = new Date()
            message.isEdited = true
            message.message = text
            await message.save()
            return message
        }
    }

    async deleteMessage(token: string, messageId: number): Promise<any> {
        const em = getManager()
        const user = await em.getCustomRepository(UsersRepository).getUserByToken(token)//e m.findOne(Users, { token: token })
        if (!user) {
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.')
        }
        const message = await em.findOne(CoinMessages, { id: messageId, senderUserId: user.id })

        if (message) {
            message.status = 2
            await message.save()
            return message.coinId
        } else {
            throw new ConflictException('این پیام پیدا نشد.')
        }
    }

    async deleteChat(user: Users, friendId: number): Promise<{ message: string }> {
        await getManager().update(CoinMessages,
            { getterUserId: user.id, senderUserId: friendId, status: 1 },
            { status: 3 })
        await getManager().update(CoinMessages,
            { senderUserId: user.id, getterUserId: friendId },
            { status: 2 })

        return { message: 'انجام شد.' }

    }

}
