import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('coinMessages')
export class CoinMessages extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    senderUserId: number

    @Column()
    coinId: number

    @Column()
    message: string

    @Column()
    dateCreate: Date

    @Column()
    dateRemoved: Date

    @Column()
    filePath: string

    @Column()
    fileName: string

    @Column()
    fileSize: number

    @Column()
    imagePath: string

    @Column()
    voicePath: string

    @Column()
    voiceDuration: number

    @Column('bool')
    isForward: boolean

    @Column()
    forwardMessageId: number

    @Column()
    forwardUserName: string

    @Column()
    forwardUserId: number

    @Column()
    status: number

    @Column('bool')
    isReply: boolean

    @Column()
    replyMessageId: number

    @Column()
    replyMessage: string

    @Column()
    replyUserId: number

    @Column()
    replyUserName: string

    @Column()
    dateLastEdit: Date

    @Column('bool')
    isEdited: boolean
}