// export class CoinMessageDTO {
//     senderUserId: number
//     coinId: number
//     message: string
//     filePath: string
//     filename: string
//     fileSize: number
//     imagePath: string
//     voicePath: string
//     voiceDuration: number
//     isForward: boolean
//     forwardMessageId: number
//     forwardUserName: string
//     forwardUserId: number
//     status: number
//     isReply: boolean
//     replyMessageId: number
//     replyUserId: number
//     replyUserName: string
//     isEdited: boolean
// }

export class CoinMessageDTO {
    coinId: number
    senderUserId: number
    senderToken: string
    message: string
    replyMessageId?: number
    replyMessage?: string
    imagePath?: string
    voicePath?: string
    voiceDuration?: number
    filePath?: string
    filename?: string
    fileSize?: number
    dateCreate: string
}