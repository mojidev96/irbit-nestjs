import { CoinsService } from './coins.service';
import { Body, Controller, Get, Param, Post } from '@nestjs/common';

@Controller('coins')
export class CoinsController {


    constructor(private coinsService: CoinsService) {}

    @Post()
    getCoins(
        @Body('page') page : number,
        @Body('name') name : string,
    ): Promise<any>{
        return this.coinsService.getCoins(page, name)
    }

    @Post('data')
    getCoinData(
        @Body('id') id : number,
    ): Promise<any>{
        return this.coinsService.getCoinData(id)
    }

}
