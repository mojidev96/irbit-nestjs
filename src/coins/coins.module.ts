import { UsersModule } from './../users/users.module';
import { Module } from '@nestjs/common';
import { CoinsService } from './coins.service';
import { CoinsController } from './coins.controller';

@Module({
  imports: [UsersModule],
  providers: [CoinsService],
  controllers: [CoinsController]
})
export class CoinsModule {}
