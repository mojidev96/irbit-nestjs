import { Coins } from './coins.entity';
import { getManager } from 'typeorm';
import { Injectable, Logger } from '@nestjs/common';

@Injectable()
export class CoinsService {

    async getCoins(page: number, name: string): Promise<any> {
        var where = 'true'
        if (name) {
            where = "c.name ilike '%" + name + "%' or c.symbol ilike '%" + name + "%'"
        }
        const list = await getManager().createQueryBuilder(Coins, 'c')
            .where(where)
            .limit(35)
            .offset(35 * (page - 1))
            .orderBy('c.id', 'ASC')
            .getMany()

        return list
    }

    async getCoinData(id: number): Promise<any> {
        const coin = await getManager().createQueryBuilder(Coins, 'c')
            .select('c.name', 'name')
            .addSelect('c.logoUrl', 'logoUrl')
            .addSelect('c.price', 'price')
            .addSelect('c.oneDayChange', 'oneDayChange')
            .where('c.id = ' + id)
            .getRawOne()

        return coin
    }


}
