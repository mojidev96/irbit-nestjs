export class CoinDTO {
    name: string
    symbol: string
    price: number
    oneDayChange: number
    info: JSON
}