import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('coins')
export class Coins extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    symbol: string

    @Column()
    logoUrl: string

    @Column()
    price: number

    @Column()
    marketCap: number

    @Column()
    high: number

    @Column()
    highTimestamp: Date

    @Column()
    priceDate: Date

    @Column()
    oneDayChange: number

    @Column('json')
    info: JSON

}