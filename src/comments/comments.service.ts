import { Comments } from './comments.entity';
import { News } from './../news/news.entity';
import { Tutorials } from './../tutorials/tutorials.entity';
import { getManager } from 'typeorm';
import { CommentDTO } from './comments.dto';
import { Users } from 'src/users/users.entity';
import { Injectable, NotFoundException, Logger } from '@nestjs/common';
import { UsersRepository } from 'src/users/users.repository';
import { Posts } from 'src/posts/posts.entity';

@Injectable()
export class CommentsService {

    async addComment(user: Users, comment: CommentDTO) {
        var content = null
        const em = getManager()
        if (comment.contentType == 1) { // tutorial
            content = await em.findOne(Tutorials, { id: comment.contentId })
        } else if (comment.contentType == 2) { // news
            content = await em.findOne(News, { id: comment.contentId })
        }else if (comment.contentType == 3) { // posts
            content = await em.findOne(Posts, { id: comment.contentId })
        }

        if (!content) {
            throw new NotFoundException('محتوا پیدا نشد.')
        }

        const com = new Comments()
        com.comment = comment.comment
        com.userId = user.id
        com.contentId = comment.contentId
        com.contentType = comment.contentType
        com.dateCreate = new Date()
        if (comment.isReply) {
            com.isReply = comment.isReply
            Logger.log('gwew:: ' + comment.mainCommentId)
            const mainComment = await em.findOne(Comments, { id: comment.mainCommentId })
            const senderMainComment = await em.createQueryBuilder(Users, 'u')
                .select('u.name', 'name')
                .where('u.id = ' + mainComment.userId)
                .getRawOne()

            if (!mainComment || !senderMainComment) {
                throw new NotFoundException('احتمالا این نظر پاک شده باشد... نمی توانید برای آن پاسخ ارسال کنید.')
            }

            com.replyToId = mainComment.userId
            com.replyToName = senderMainComment.name

        }

        if (comment.replyId) {
            com.replyId = comment.replyId
        }

        await com.save()

        return { message: 'با موفقیت ثبت شد و پس از تایید منتشر می شود.' }
    }

    async deleteComment(user: Users, id: number): Promise<any> {
        const comment = await getManager().findOne(Comments, { id, userId: user.id })
        if (comment) {
            await getManager().update(Comments, { id }, { isDeleted: true })
            await getManager().update(Comments, { replyId: id }, { isDeleted: true }) // جوابا به این کامنت هم پاک میشن
            return { message: 'با موفقیت حذف شد.' }
        } else {
            throw new NotFoundException('نظر شما پیدا نشد.')
        }
    }

    async getComments(token: string, page: number, contentId: number, contentType: number, isForSub: boolean, commentId: number): Promise<any> {

        const em = getManager()

        var selectIUp = 'false as "iUp"'
        var selectIDown = 'false as "iDown"'

        const user = await em.getCustomRepository(UsersRepository).getUserByToken(token)

        // var whereImNotIn = 'true'
        if (user) {
            selectIUp = '(select "isUp" from "upAndDowns" where "userId" = ' + user.id + ' and "contentType" = 3 and "contentId" = n.id and "isUp" is true) as "iUp"'
            selectIDown = '(select "isUp" is false from "upAndDowns" where "userId" = ' + user.id + ' and "contentType" = 3 and "contentId" = n.id and "isUp" is false) as "iDown"'

            // whereImNotIn = 'n.userId != ' + user.id
        }

        if (isForSub && String(isForSub) == 'true') {
            const replies = await getManager()
                .createQueryBuilder(Comments, 'n')
                .leftJoin(Users, 'u', 'u.id = n.userId')
                .select('n.id', 'id')
                .addSelect('u.id', 'commenterId')
                .addSelect('u.name', 'name')
                .addSelect('u.avatar', 'avatar')
                .addSelect('n.comment', 'comment')
                .addSelect('n.dateCreate', 'dateCreate')
                .addSelect('n.ups', 'ups')
                .addSelect('n.downs', 'downs')
                .addSelect('n.replyToId', 'replyToId')
                .addSelect('n.replyToName', 'replyToName')
                .addSelect(selectIUp)
                .addSelect(selectIDown)
                .where('n.contentId = ' + contentId + ' and n.contentType = ' + contentType + ' and n.isReply is true and n.isAccepted is true and n.replyId = ' + commentId)
                .andWhere('n.isDeleted is false')
                .orderBy('n.id', 'ASC')
                .offset(5 * (page - 1))
                .limit(5)
                .getRawMany()

                return replies
        } else {
            var replies = []
            const comments = await em.createQueryBuilder(Comments, 'n')
                .leftJoin(Users, 'u', 'u.id = n.userId')
                .select('n.id', 'id')
                .addSelect('u.name', 'name')
                .addSelect('u.avatar', 'avatar')
                .addSelect('u.id', 'commenterId')
                .addSelect('n.comment', 'comment')
                .addSelect('n.dateCreate', 'dateCreate')
                .addSelect('n.ups', 'ups')
                .addSelect('n.downs', 'downs')
                .addSelect('(select count(*) from comments where "isAccepted" is true and "isDeleted" is false and "isReply" is true and "replyId" = n.id) as "repliesCount"')
                .addSelect(selectIUp)
                .addSelect(selectIDown)
                .where('n.contentId = ' + contentId + ' and n.contentType = ' + contentType + ' and n.isReply is false and n.isAccepted is true')
                // .andWhere(whereImNotIn)
                .andWhere('n.isDeleted is false')
                .orderBy('n.id', 'DESC')
                .offset(10 * (page - 1))
                .limit(10)
                .getRawMany()

            replies = comments.map(async comment => {
                comment.replies = await getManager()
                    .createQueryBuilder(Comments, 'n')
                    .leftJoin(Users, 'u', 'u.id = n.userId')
                    .select('n.id', 'id')
                    .addSelect('u.id', 'commenterId')
                    .addSelect('u.name', 'name')
                    .addSelect('u.avatar', 'avatar')
                    .addSelect('n.comment', 'comment')
                    .addSelect('n.dateCreate', 'dateCreate')
                    .addSelect('n.ups', 'ups')
                    .addSelect('n.downs', 'downs')
                    .addSelect('n.replyToId', 'replyToId')
                    .addSelect('n.replyToName', 'replyToName')
                    .addSelect(selectIUp)
                    .addSelect(selectIDown)
                    .where('n.contentId = ' + contentId + ' and n.contentType = ' + contentType + ' and n.isReply is true and n.isAccepted is true and n.replyId = ' + comment.id)
                    .andWhere('n.isDeleted is false')
                    .orderBy('n.id', 'ASC')
                    .limit(2)
                    .getRawMany()
            })

            await Promise.all(replies)

            return comments
        }

    }

    async getCommentReplies(token: string, page: number, commentId: number): Promise<any> {

        const em = getManager()

        var selectIUp = 'false as "iUp"'
        var selectIDown = 'false as "iDown"'

        const user = await em.getCustomRepository(UsersRepository).getUserByToken(token)
        if (user) {
            selectIUp = '(select "isUp" from "upAndDowns" where "userId" = ' + user.id + ' and "contentType" = 3 and "contentId" = n.id and "isUp" is true) as "iUp"'
            selectIDown = '(select "isUp" is false from "upAndDowns" where "userId" = ' + user.id + ' and "contentType" = 3 and "contentId" = n.id and "isUp" is false) as "iDown"'
        }

        const replies = await em.createQueryBuilder(Comments, 'n')
            .leftJoin(Users, 'u', 'u.id = n.userId')
            .select('n.id', 'id')
            .addSelect('u.name', 'name')
            .addSelect('u.avatar', 'avatar')
            .addSelect('u.id', 'commenterId')
            .addSelect('n.comment', 'comment')
            .addSelect('n.dateCreate', 'dateCreate')
            .addSelect('n.ups', 'ups')
            .addSelect('n.downs', 'downs')
            .addSelect(selectIUp)
            .addSelect(selectIDown)
            .where('n.replyId = ' + commentId + ' and n.isAccepted is true')
            .offset(10 * (page - 1))
            .limit(10)
            .getRawMany()

        return replies
    }
}
