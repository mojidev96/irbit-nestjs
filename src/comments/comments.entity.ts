import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn, Double } from 'typeorm';


@Entity()
export class Comments extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userId: number

    @Column()
    contentId: number

    // 1 = tutorial
    // 2 = news
    @Column()
    contentType: number

    @Column()
    comment: string;

    @Column()
    dateCreate: Date

    @Column()
    ups: number;

    @Column()
    downs: number;

    @Column('bool')
    isReply: boolean;

    @Column()
    replyId: number;

    @Column('bool')
    isAccepted: boolean;

    @Column()
    replyToId: number

    @Column()
    replyToName: string

    @Column('bool')
    isDeleted: boolean
}