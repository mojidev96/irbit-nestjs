import { Users } from 'src/users/users.entity';
import { AuthGuard } from '@nestjs/passport';
import { CommentDTO } from './comments.dto';
import { CommentsService } from './comments.service';
import { Body, Controller, Post, UseGuards, Delete, Headers, Logger } from '@nestjs/common';
import { GetUser } from 'src/users/getuser.decorator';

@Controller('comments')
export class CommentsController {

    constructor(private commentsService: CommentsService) { }
    @Post('list')
    getComments(
        @Body('page') page: number,
        @Body('contentId') contentId: number,
        @Body('contentType') contentType: number,
        @Body('isForSub') isForSub: boolean,
        @Body('commentId') commentId: number,
        @Headers('token') token: string,
    ): Promise<any> {
        return this.commentsService.getComments(token, page, contentId, contentType, isForSub, commentId)
    }

    @Post('replies')
    getReplies(
        @Body('page') page: number,
        @Body('commentId') commentId: number,
        @Headers('token') token: string,
    ): Promise<any> {
        return this.commentsService.getCommentReplies(token, page, commentId)
    }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    addComment(
        @GetUser() user: Users,
        @Body() comment: CommentDTO
    ): Promise<any> {
        return this.commentsService.addComment(user, comment)
    }

    @Delete()
    @UseGuards(AuthGuard('jwt'))
    deleteComment(
        @GetUser() user: Users,
        @Body('id') id: number
    ): Promise<any> {
        return this.commentsService.deleteComment(user, id)
    }
}
