export class CommentDTO {
    contentId: number
    contentType: number
    comment: string
    isReply: boolean
    replyId: number
    mainCommentId: number // for reply
}