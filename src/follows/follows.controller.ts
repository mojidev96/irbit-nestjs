import { Body, Controller, Headers, Logger, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/users/getuser.decorator';
import { Users } from 'src/users/users.entity';
import { FollowsService } from './follows.service';

@Controller('follows')
export class FollowsController {

    constructor(private followsService: FollowsService) { }

    // type: 1=follow, 2=unfollow
    @Post()
    @UseGuards(AuthGuard('jwt'))
    followUnfollow(
        @GetUser() user: Users,
        @Body('id') id: number,
        @Body('type') type: number,
    ): Promise<any> {
        return this.followsService.followUnfollow(user, id, type)
    }

    @Post('list')
    getFollowers(
        @Body('id') id: number,
        @Body('page') page: number,
        @Body('type') type: number,
        @Headers('token') token: string
    ): Promise<any> {
        return this.followsService.getFollowers(id, type, page, token)
    }
}
