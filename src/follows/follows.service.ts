import { ConflictException, Injectable, Logger } from '@nestjs/common';
import { Users } from 'src/users/users.entity';
import { UsersRepository } from 'src/users/users.repository';
import { getManager } from 'typeorm';
import { Follows } from './follows.entity';

@Injectable()
export class FollowsService {
  
  async followUnfollow(user: Users, id: number, type: number): Promise<any> {
    const em = getManager();
    const followBefore = await em.findOne(Follows, {
      followingId: id,
      followerId: user.id,
    });
    if (type === 1) {
      // follow
      // check mikonim ghablan follow nakarde bashe:
      if (followBefore) {
        throw new ConflictException('قبلا فالو کردید.');
      } else {
        const newFollowing = new Follows();
        newFollowing.dateCreate = new Date();
        newFollowing.followerId = user.id;
        newFollowing.followingId = id;
        await newFollowing.save();
        await em.update(Users, { id }, { followersCount: () => '"followersCount" + 1' })
        user.followingCount = user.followingCount + 1;
      }
    } else if (type === 2) {
      //unfollow
      if (!followBefore) {
        throw new ConflictException('فالو ندارید.');
      } else {
        user.followingCount = user.followingCount > 0 ? user.followingCount - 1 : 0;
        await em.update(Users, { id }, { followersCount: () => '"followersCount" - 1' })
        await followBefore.remove();
      }
    }
    await user.save();
    return { message: 'انجام شد', id, type };
  }

  async getFollowers(id: number, type: number, page: number, token: string): Promise<any> {
    const em = getManager();
    const user = await em
      .getCustomRepository(UsersRepository)
      .getUserByToken(token);
    const list = await getManager().createQueryBuilder(Follows, 'f')
      .leftJoin(Users, 'u', type == 1 ? 'u.id = f."followerId"' : 'u.id = f."followingId"')
      .select('u.id', 'id')
      .addSelect('u.name', 'name')
      .addSelect('u.avatar', 'avatar')
      .addSelect('u.username', 'username')
      .addSelect(user ? '(select id > 0 as "isFollowed" from follows where "followingId" = u.id and "followerId" = ' + user.id + ')' : 'false as "isFollowed"')
      .where('u.status = 1 and ' + (type == 1 ? 'f."followingId" = ' : 'f."followerId" = ') + id)
      .limit(10)
      .offset(10 * (page - 1))
      .getRawMany()

    return list
  }
}
