import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn, Double } from 'typeorm';


@Entity()
export class Follows extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    followerId: number

    @Column()
    followingId: number

    @Column()
    dateCreate: Date

}