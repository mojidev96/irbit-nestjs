import { Module } from '@nestjs/common';
import { UsersModule } from 'src/users/users.module';
import { PlanersController } from './planers.controller';
import { PlanersService } from './planers.service';

@Module({
  imports: [UsersModule],
  controllers: [PlanersController],
  providers: [PlanersService]
})
export class PlanersModule { }
