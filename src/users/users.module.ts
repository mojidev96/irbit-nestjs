import { AppGateway } from './../app.gateway';
import { Module, HttpModule } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersRepository } from './users.repository';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { MulterModule } from '@nestjs/platform-express';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    TypeOrmModule.forFeature([UsersRepository]),
    JwtModule.register({
      secret: 'mojidev1996'
    }),
    PassportModule.register({
      defaultStrategy: 'jwt'
    }),
    MulterModule.register({
      dest: './files',
    }),
    HttpModule.register({
      timeout: 120000,
      maxRedirects: 5,
    })
  ],
  controllers: [UsersController],
  providers: [UsersService,
    JwtStrategy,
    HttpModule,
  ],
  exports: [
    JwtStrategy,
    PassportModule,
    HttpModule,
    MulterModule.register({
      dest: './files',
    }),
  ],
})
export class UsersModule { }
