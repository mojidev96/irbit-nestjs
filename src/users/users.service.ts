import { Tokens } from './../tokens/tokens.entity';
import {
    Injectable,
    HttpService,
    NotFoundException,
    ConflictException,
    UnauthorizedException,
    Logger,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersRepository } from './users.repository';
import { JwtService } from '@nestjs/jwt';
import { Users } from './users.entity';
import { EntityManager, getManager, Double } from 'typeorm';
import { EditProfileDTO, SignUpUserDTO } from './user.dto';
import { JwtPayload } from './jwt-payload.interface';

import { Locations } from '../locations/locations.entity';
import { Delete } from '@nestjs/common';
import e = require('express');
import { Posts } from 'src/posts/posts.entity';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(UsersRepository) private usersRepository: UsersRepository,
        private jwtService: JwtService,
        private http: HttpService,
    ) { }

    // این انلاین بودن یا نبودن یک یوزر رو ثبت میکنه همنطور میگه اخرین بار کی حضور داشه
    // تو برگشتش میگه حق داری لست انلاین بودنشو نشون بدی یا نه
    async changeOnlineStatement(
        userId: number,
        token: string,
        isOnline: boolean,
        date: Date,
        isFromGatewaySSWE?: boolean,
    ): Promise<boolean> {
        if (!userId || userId == 0) {
            throw new NotFoundException();
        }
        const em = getManager();
        if (isFromGatewaySSWE && String(isFromGatewaySSWE) == 'true') {
            await em.update(
                Users,
                { id: userId },
                { isOnline: isOnline, dateLastOnline: date },
            );
        } else {
            await em.update(
                Users,
                { id: userId, token: token },
                { isOnline: isOnline, dateLastOnline: date },
            );
        }

        const canShowIsOnline = await em
            .createQueryBuilder(Users, 'u')
            .select('u.isShowingLastOnline', 'isShowingLastOnline')
            .where('u.id = ' + userId)
            .getRawOne();
        if (!canShowIsOnline) {
            throw new UnauthorizedException('لطفا دوباره وارد حساب کاربری خود شوید.');
        }
        if (canShowIsOnline.isShowingLastOnline) {
            return true;
        } else {
            return false;
        }
    }

    // دریافت آخرین وضعیت آنلاین بودن یا نبودن یک کاربر
    async getLatestOnlineStatementOfUser(
        userId: number,
    ): Promise<{
        isOnline: boolean;
        canShowingLastOnline: boolean;
        dateLastOnline: string;
    }> {
        const data = await getManager()
            .createQueryBuilder(Users, 'u')
            .select('u.isOnline', 'isOnline')
            .addSelect('u.isShowingLastOnline', 'isShowingLastOnline')
            .addSelect('u.dateLastOnline', 'dateLastOnline')
            .where('u.id = ' + userId)
            .getRawOne();
        if (!data) {
            throw new NotFoundException();
        } else {
            return {
                isOnline: data.isOnline,
                canShowingLastOnline: data.isShowingLastOnline,
                dateLastOnline: data.dateLastOnline,
            };
        }
    }

    async usernameIsAvailable(username: string): Promise<boolean> {
        const userId = await getManager()
            .createQueryBuilder(Users, 'u')
            .select('u.id', 'id')
            .where('u.username = ' + "'" + username + "'")
            .getRawOne();
        return !userId;
    }

    async isUserValidUserId(userId: number, token: string): Promise<boolean> {
        const isValid = await getManager()
            .createQueryBuilder(Tokens, 't')
            .select('t.id', 'id')
            .where('t.uid = ' + userId + ' and t.token = ' + "'" + token + "'")
            .limit(1)
            .getRawOne();
        return isValid.id != null;
    }

    async setClientIdToUser(userId: number, clientId: string): Promise<any> {
        await getManager().update(
            Users,
            { id: userId },
            { clientId, isOnline: true },
        );
        return 'done';
    }

    async setClientDisconected(clientId: string): Promise<any> {
        await getManager().update(
            Users,
            { clientId },
            { clientId: '', isOnline: false },
        );
        return 'done';
    }

    // others requested to see someone's profile
    async getProfileById(token: string, userId: number): Promise<any> {
        const em = getManager();

        const user = await em
            .getCustomRepository(UsersRepository)
            .getUserByToken(token);
        const userHadLocation = user && user.location && user.location.length > 0;
        const posts = await em.createQueryBuilder(Posts, 'p')
        .select('p.id', 'id')
        .addSelect('p.posterUrl', 'posterUrl')
        .addSelect('p.videoUrl', 'videoUrl')
        .where('p.userId = ' + userId)
        .orderBy('p.id', 'DESC')
        .getRawMany()
        const profile = await em
            .createQueryBuilder(Users, 'u')
            .select([
                'u.id as id',
                'u.name as name',
                'u.username as username',
                'u.phone as phone',
                'u.job as job',
                'u.avatar as avatar',
                'u.about as about',
                'u.city as city',
                'u.followersCount as "followersCount"',
                'u.followingCount as "followingCount"',
                'u.postsCount as "postsCount"',
                'u.district as district',
                'u.seenCountByOthers as "seenCountByOthers"',
                userHadLocation
                    ? 'case when (u.location is null or u.isAccessibleLocation = false) then 0 else earth_distance(ll_to_earth(u.location[1], u.location[2]), ll_to_earth(' +
                    user.location[0] +
                    ', ' +
                    user.location[1] +
                    '))::integer end as distance'
                    : '-1 as distance',
                'case when u.isShowingLastOnline = true then u.dateLastOnline else null end as "dateLastOnline"',
                'case when u.isAccessiblePhone = true then u.phone else null end as phone',
                'u.username as username',
            ])
            .addSelect(user ? '(select id > 0 as "isFollowed" from follows where "followingId" = u.id and "followerId" = ' + user.id + ')' : 'false as "isFollowed"')
            .where('u.id = :id', { id: userId })
            .andWhere('u.status = :status', { status: 1 })
            .getRawOne();
        if (!profile) {
            throw new NotFoundException('این کاربر پیدا نشد.');
        } else {
            if (user && user.id == userId) {
                await em.update(
                    Users,
                    { id: userId },
                    { seenCountBySelf: () => '"seenCountBySelf" + 1' },
                );
            } else {
                await em.update(
                    Users,
                    { id: userId },
                    { seenCountByOthers: () => '"seenCountByOthers" + 1' },
                );
            }
        }
        return { profile, posts };
    }

    // when owner requested too see profile
    async getProfile(
        userReceived: Users,
    ): Promise<{ user: Users; storeStatus: number }> {
        // const em = getManager();
        // const user = await em.findOne(Users, userReceived.id);
        // if (!user) {
        //     throw new NotFoundException('این کاربر پیدا نشد.');
        // }
        delete userReceived.smsCode;
        delete userReceived.token;
        delete userReceived.fcmToken;

        delete userReceived.smsCode;
        return { user: userReceived, storeStatus: 0 };
    }

    async editProfile(
        user: Users,
        editDTO: EditProfileDTO,
        avatarFileName?: string,
    ): Promise<Users> {
        const em = getManager();
        if (editDTO.username) {
            const alreadyExist = await em.findOne(Users, {
                username: editDTO.username,
            });
            if (!alreadyExist || alreadyExist.id == user.id) {
                user.username = editDTO.username;
            } else {
                throw new ConflictException('این نام کاربری در حال استفاده است.');
            }
        }
        if (!user.name || user.name.length == 0) {
            user.dateCompleteRegister = new Date();
        }
        if (editDTO.name) {
            user.name = editDTO.name;
        }
        user.email = editDTO.email;
        user.job = editDTO.job;
        user.about = editDTO.about;
        if (avatarFileName) {
            user.avatar = 'files/' + avatarFileName;
        }

        user.city = editDTO.city;
        // user.district = editDTO.district;
        user.city = editDTO.city;
        user.gender = editDTO.gender;
        user.isAccessibleUnivocal = editDTO.isAccessibleUnivocal;
        user.isAccessibleLocation = editDTO.isAccessibleLocation;
        user.isAccessiblePhone = editDTO.isAccessiblePhone;
        user.isShowingLastOnline = editDTO.isShowingLastOnline;
        user.isAccessibleMarketing = editDTO.isAccessibleMarketing;
        user.isHideNotifContent = editDTO.isHideNotifContent;

        await user.save();
        return user;
    }

    // async signUp(user: Users, addUserDto: SignUpUserDTO, avatarFileName?: string): Promise<Users> {
    //     const em = getManager();
    //     if (addUserDto.username) {
    //         const alreadyExist = await em.findOne(Users, { where: { username: addUserDto.username } });
    //         if (!alreadyExist || alreadyExist.id == user.id) {
    //             user.username = addUserDto.username;
    //         } else {
    //             throw new ConflictException('این نام کاربری در حال استفاده است.');
    //         }
    //     }
    //     user.about = addUserDto.about;
    //     user.email = addUserDto.email;
    //     user.name = addUserDto.name;
    //     user.job = addUserDto.job;
    //     user.city = addUserDto.city;
    //     user.gender = addUserDto.gender;
    //     user.district = addUserDto.district;
    //     user.status = 1;
    //     if (avatarFileName) {
    //         user.avatar = 'files/' + avatarFileName;
    //     }
    //     user.dateCompleteRegister = new Date();
    //     user.dateLastOnline = new Date();
    //     user.isAccessibleLocation = addUserDto.isAccessibleLocation;
    //     user.isAccessiblePhone = addUserDto.isAccessiblePhone;
    //     user.isAccessibleUnivocal = addUserDto.isAccessibleUnivocal;
    //     user.isShowingLastOnline = addUserDto.isShowingLastOnline;
    //     user.isAccessibleMarketing = addUserDto.isAccessibleMarketing;
    //     user.isHideNotifContent = addUserDto.isHideNotifContent;
    //     await user.save();
    //     return user;
    // }

    async checkPhoneIsInCommunity(
        phone: string,
    ): Promise<{ userId: number; avatar: string; isOnline: boolean }> {
        const user = await getManager()
            .createQueryBuilder(Users, 'u')
            .select('u.id as id')
            .addSelect('u.avatar as avatar')
            .addSelect(
                'case when u.isShowingLastOnline then u.isOnline else null end as "isOnline"',
            )
            .where("u.phone = '" + phone + "'")
            .getRawOne();
        if (user) {
            return { userId: user.id, avatar: user.avatar, isOnline: user.isOnline };
        } else {
            return { userId: 0, avatar: '', isOnline: false };
        }
    }

    async logout(user: Users, isWeb: boolean): Promise<any> {
        if (isWeb && String(isWeb) == 'true') {
            user.webFCMToken = '';
        } else {
            user.fcmToken = '';
        }
        await getManager().delete(Tokens, { token: user.token });
        user.dateLastOnline = new Date();
        await user.save();
        return true;
    }

    // check phoneNumber and sendedCode by smsCode field in users table
    async signIn(
        phoneReceived: string,
        receivedSmsCode: number,
        fcmToken: string,
        isWeb: boolean,
    ): Promise<Users> {
        const em = getManager();
        const userExist = await em.findOne(Users, { phone: phoneReceived });
        if (userExist) {
            // tslint:disable-next-line:triple-equals
            if (receivedSmsCode != null && userExist.smsCode == receivedSmsCode) {
                const payload: JwtPayload = { phone: phoneReceived };
                const tokenCreated = this.jwtService.sign(payload);
                if (isWeb && String(isWeb) == 'true') {
                    userExist.webFCMToken = fcmToken;
                } else {
                    userExist.fcmToken = fcmToken;
                }

                const newToken = new Tokens();
                newToken.uid = userExist.id;
                newToken.token = tokenCreated;
                await newToken.save();

                userExist.token = tokenCreated;
                userExist.smsCode = null;
                if (userExist.status == 3) {
                    userExist.status = 1;
                }
                await userExist.save();
                delete userExist.smsCode;
                return userExist;
            } else {
                throw new UnauthorizedException('کد وارد شده صحیح نیست.');
            }
        } else {
            throw new NotFoundException('کاربری یافت نشد لطفا دوباره تلاش کنید.');
        }
    }

    async updateFCMToken(user: Users, fcm: string): Promise<{ message: string }> {
        user.fcmToken = fcm;
        await user.save();
        return { message: 'job done' };
    }

    async updateWebFCMToken(
        user: Users,
        fcm: string,
    ): Promise<{ message: string }> {
        user.webFCMToken = fcm;
        await user.save();
        return { message: 'job done' };
    }

    // Send SMS And Save GeneratedCode and Phone To Database
    async sendSMS(
        phoneReceived: string,
        location: Double[],
    ): Promise<{ message: string }> {
        const em = getManager();
        const userExist = await em.findOne(Users, { phone: phoneReceived });
        const smsCode = Math.floor(Math.random() * 9999) + 1000;
        if (userExist != null && userExist.smsCode == null) {
            userExist.smsCode = smsCode;
        }
        this.http
            .get(
                encodeURI(
                    'https://api.kavenegar.com/v1/4362695A3879324943585372326F7A493574483068413D3D/verify/lookup.json?receptor=' +
                    phoneReceived +
                    '&token=' +
                    (userExist == undefined ? smsCode : userExist.smsCode) +
                    '&template=irbitlogin',
                ),
            )
            .subscribe(res => {
                Logger.log('kavenegar: ' + res);
            });
        //hellohello
        if (userExist) {
            if (location != null) {
                userExist.location = location;
            }
            Logger.log('code 1 : ' + smsCode);
            await userExist.save();
        } else {
            Logger.log('code 2 : ' + smsCode + ' --- ' + phoneReceived);
            const user = new Users();
            user.phone = phoneReceived;
            user.smsCode = smsCode;
            if (location != null) {
                user.location = location;
            }
            user.dateFirstRegister = new Date();
            await user.save();
        }

        return { message: 'ارسال شد' };
    }

    async getNearestUsers(
        userId: number,
        location: Double[],
        page: number,
    ): Promise<Users[]> {
        var andWhere = 'true';
        if (userId && userId > 0) {
            andWhere = 'u.id != ' + userId;
        }
        const users = await getManager()
            .createQueryBuilder(Users, 'u')
            .leftJoin('u.store', 'store')
            .select('u.id', 'id')
            .addSelect('u.name', 'name')
            .addSelect('u.job', 'job')
            .addSelect('u.avatar', 'avatar')
            .addSelect(
                'case when u."isShowingLastOnline" is true THEN u."isOnline" ELSE null end',
                'isOnline',
            )
            .addSelect(
                'case when u.location is null or u.isAccessibleLocation = false then 0 else earth_distance(ll_to_earth(u.location[1], u.location[2]), ll_to_earth(' +
                location[0] +
                ', ' +
                location[1] +
                '))::integer end as distance',
            )
            .addSelect('u.about', 'about')
            .addSelect('u.isShowingLastOnline', 'isShowingLastOnline')
            .addSelect(
                'case when "isShowingLastOnline" is true THEN "dateLastOnline" ELSE null end',
                'dateLastOnline',
            )
            .addSelect('store.name', 'storeName')
            .addSelect('store.id', 'storeId')
            .addSelect('store.status', 'storeStatus')
            .groupBy(
                'u.id, u.name, u.job, u.avatar, u.isOnline, store.status, u.about, u.isShowingLastOnline, distance, u.dateLastOnline, store.id, store.name, u.location',
            )
            .where('u.location != :loc', { loc: location })
            .andWhere("u.location != '{NULL, NULL}'")
            .andWhere('u.isAccessibleLocation = :isAccess', { isAccess: true })
            .andWhere('u.status = :status', { status: 1 })
            .andWhere(andWhere)
            .orderBy('distance', 'ASC')
            .limit(10)
            .offset(10 * (page - 1))
            .getRawMany();

        return users;
    }

    async getMyColleagues(user: Users, page: number): Promise<Users[]> {
        if (!user.job || user.job.length == 0) {
            return [];
        }

        const userHadLocation =
            user != null && user.location != null && user.location.length > 0;

        var myJob = user.job.replace("'", '');

        const users = await getManager()
            .createQueryBuilder(Users, 'u')
            .leftJoin('u.store', 'store')
            .select('u.id', 'id')
            .addSelect('u.name', 'name')
            .addSelect('u.job', 'job')
            .addSelect('u.avatar', 'avatar')
            .addSelect(
                'case when u."isShowingLastOnline" is true THEN u."isOnline" ELSE null end',
                'isOnline',
            )
            .addSelect(
                userHadLocation
                    ? 'case when u.location is null or u.isAccessibleLocation = false then 0 else earth_distance(ll_to_earth(u.location[1], u.location[2]), ll_to_earth(' +
                    user.location[0] +
                    ', ' +
                    user.location[1] +
                    '))::integer end as distance'
                    : '-1 as distance',
            )
            .addSelect('u.about', 'about')
            .addSelect(
                'case when u."isShowingLastOnline" is true THEN u."dateLastOnline" ELSE null end',
                'dateLastOnline',
            )
            .addSelect('store.name', 'storeName')
            .addSelect('store.id', 'storeId')
            .addSelect('store.status', 'storeStatus')
            .groupBy(
                'u.id, u.seenCountByOthers, u.name, u.job, store.status, u.avatar, u.about, u.isOnline, u.isShowingLastOnline, distance, u.dateLastOnline, store.id, store.name, u.location, u.email, store.ordersCount',
            )
            .where('u.id != :userId', { userId: user.id })
            .andWhere('u.status = :status', { status: 1 })
            .andWhere("u.job like '%" + myJob + "%'")
            .orderBy(userHadLocation ? 'distance' : 'true', 'ASC')
            .addOrderBy('u.seenCountByOthers', 'DESC')
            .limit(10)
            .offset(10 * (page - 1))
            .getRawMany();

        return users;
    }

    async updateLocation(
        user: Users,
        location: Double[],
    ): Promise<{ message: string }> {
        user.location = location;
        await user.save();
        await getManager().insert(Locations, {
            userId: user.id,
            location: location,
            dateCreate: new Date(),
        });
        return { message: 'انجام شد.' };
    }

    async getList(token: string, page: number, word: string): Promise<any> {
        const em = getManager();

        const user = await em
            .getCustomRepository(UsersRepository)
            .getUserByToken(token);
        const userHadLocation =
            user != null && user.location != null && user.location.length > 0;

        var where = 'true';
        if (word && word.length > 0) {
            where =
                "u.job ilike '%" +
                word +
                "%' or u.name ilike '%" +
                word +
                "%' or u.about ilike '%" +
                word +
                "%'";
        }

        const users = await em
            .createQueryBuilder(Users, 'u')
            .select('u.id', 'id')
            .addSelect('u.name', 'name')
            .addSelect('u.job', 'job')
            .addSelect('u.avatar', 'avatar')
            .addSelect(
                'case when u."isShowingLastOnline" is true THEN u."isOnline" ELSE null end',
                'isOnline',
            )
            .addSelect(
                userHadLocation
                    ? 'case when u.location is null or u.isAccessibleLocation = false then 0 else earth_distance(ll_to_earth(u.location[1], u.location[2]), ll_to_earth(' +
                    user.location[0] +
                    ', ' +
                    user.location[1] +
                    '))::integer end as distance'
                    : '-1 as distance',
            )
            .addSelect('u.about', 'about')
            .addSelect(
                'case when u."isShowingLastOnline" is true THEN u."dateLastOnline" ELSE null end',
                'dateLastOnline',
            )
            .groupBy(
                'u.id, u.seenCountByOthers, u.name, u.job, u.avatar, u.about, u.isOnline, u.isShowingLastOnline, distance, u.dateLastOnline, u.location, u.email',
            )
            .where('u.id != :userId', { userId: user != null ? user.id : 0 })
            .andWhere('u.name is not null')
            .andWhere('u.status = :status', { status: 1 })
            .andWhere(where)
            .orderBy(userHadLocation ? 'distance' : 'true', 'ASC')
            .addOrderBy('u.seenCountByOthers', 'DESC')
            .limit(15)
            .offset(15 * (page - 1))
            .getRawMany();

        return users;
    }

    // // SignIn: check code and generatedCode in database
    // async signIn(phoneReceived: string, receivedSmsCode: number): Promise<{ token: string }> {
    //     return await this.userRepository.signIn(phoneReceived, receivedSmsCode, this.jwtService);
    // }
}
