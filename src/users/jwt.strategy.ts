import { Users } from './users.entity';
import { UsersRepository } from './users.repository';
import { JwtPayload } from './jwt-payload.interface';
import { Strategy, ExtractJwt } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(@InjectRepository(UsersRepository) private userRepository: UsersRepository) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: 'mojidev1996',
    });
  }

  async validate(payload: JwtPayload): Promise<Users> {
    const { phone } = payload;
    const user = await this.userRepository.findOne({ phone });
    if (!user) {
      throw new UnauthorizedException('لطفا دوباره وارد حساب کابری خود شوید.');
    }
    return user;
  }
}
