export class SignUpUserDTO {
    name: string
    username: string
    avatar: string
    job: string
    about: string
    email: string
    city: string
    district: string
    gender: number
    isAccessibleUnivocal: boolean
    isAccessibleLocation: boolean
    isAccessiblePhone: boolean
    isShowingLastOnline: boolean
    isAccessibleMarketing: boolean
    isHideNotifContent: boolean
}

export class EditProfileDTO {
    name: string
    username: string
    avatar: string
    job: string
    about: string
    email: string
    city: string
    district: string
    gender: number
    isAccessibleUnivocal: boolean
    isAccessibleLocation: boolean
    isAccessiblePhone: boolean
    isShowingLastOnline: boolean
    isAccessibleMarketing: boolean
    isHideNotifContent: boolean
}