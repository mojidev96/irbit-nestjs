import { UnauthorizedException, NotFoundException } from '@nestjs/common';
import { Tokens } from "src/tokens/tokens.entity";
import { EntityRepository, getManager, Repository } from "typeorm";
import { Users } from './users.entity';

@EntityRepository(Users)
export class UsersRepository extends Repository<Users> {

    async getUserByToken(token: string): Promise<Users> {
        const tokenData = await getManager().createQueryBuilder(Tokens, 't')
            .where('t.token = ' + "'" + token + "'")
            .limit(1)
            .getOne()
        if (tokenData) {
            const user = await getManager().findOne(Users, { id: tokenData.uid })
            if (user) {
                return user
            } else {
                throw new NotFoundException('چنین کاربری یافت نشد.')
            }
        }
    }

}