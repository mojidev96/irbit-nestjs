import { Controller, Get, UseGuards, Body, Post, UseInterceptors, UploadedFile, Patch, Headers } from '@nestjs/common';
import { UsersService } from './users.service';
import { AuthGuard } from '@nestjs/passport';
import { Users } from './users.entity';
import { EditProfileDTO, SignUpUserDTO } from './user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { GetUser } from './getuser.decorator';

import { Double } from 'typeorm';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService) { }

    @Post('getById')
    getProfileById(
        @Headers('token') token: string,
        @Body('userId') userId: number
    ): Promise<any> {
        return this.usersService.getProfileById(token, userId);
    }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    getProfile(@GetUser() user: Users): Promise<{ user: Users, storeStatus: number }> {
        return this.usersService.getProfile(user);
    }

    @Post('list')
    getList(
        @Body('word') word: string,
        @Body('page') page: number,
        @Headers('token') token: string
    ): Promise<{ user: Users, storeStatus: number }> {
        return this.usersService.getList(token, page, word);
    }

    @Post('edit')
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileInterceptor('avatar'))
    editProfile(@GetUser() user: Users, @Body() editDTO: EditProfileDTO, @UploadedFile() file): Promise<Users> {
        return this.usersService.editProfile(user, editDTO, file ? file.filename : '');
    }

    @Post('sendSMS')
    sendSms(@Body('phone') phone: string, @Body('location') location: Double[]): Promise<{ message: string }> {
        return this.usersService.sendSMS(phone, location);
    }

    @Post('updateFCM')
    @UseGuards(AuthGuard('jwt'))
    updateFcmToken(@GetUser() user: Users, @Body('fcm') fcm: string): Promise<{ message: string }> {
        return this.usersService.updateFCMToken(user, fcm);
    }

    @Post('updateWebFCMToken')
    @UseGuards(AuthGuard('jwt'))
    updateWebFCMToken(@GetUser() user: Users, @Body('fcm') fcm: string): Promise<{ message: string }> {
        return this.usersService.updateWebFCMToken(user, fcm);
    }

    @Post('logout')
    @UseGuards(AuthGuard('jwt'))
    logout(@GetUser() user: Users,
        @Body('isWeb') isWeb: boolean): Promise<any> {
        return this.usersService.logout(user, isWeb);
    }

    @Post('signIn')
    signIn(@Body('phone') phone: string, @Body('code') code: number, @Body('isWeb') isWeb: boolean, @Body('fcmToken') fcmToken: string): Promise<Users> {
        return this.usersService.signIn(phone, code, fcmToken, isWeb);
    }

    // @Post('signUp')
    // @UseGuards(AuthGuard('jwt'))
    // @UseInterceptors(FileInterceptor('avatar'))
    // signUp(
    //     @GetUser() user: Users,
    //     @Body() addUserDto: SignUpUserDTO,
    //     @UploadedFile() file
    // ): Promise<Users> {
    //     return this.usersService.signUp(user, addUserDto, file ? file.filename : '');
    // }

    @Post('nearestUsers')
    getNearestUsers(
        @Body('location'
        ) location: Double[],
        @Body('page') page: number,
        @Body('userId') userId: number
    ): Promise<Users[]> {
        return this.usersService.getNearestUsers(userId, location, page)
    }


    @Post('colleagues')
    @UseGuards(AuthGuard('jwt'))
    getColleagues(
        @GetUser() user: Users,
        @Body('page') page: number
    ): Promise<Users[]> {
        return this.usersService.getMyColleagues(user, page)
    }

    @Post('updateLocation')
    @UseGuards(AuthGuard('jwt'))
    updateLocation(@GetUser() user: Users, @Body('location') location: Double[]): Promise<{ message: string }> {
        return this.usersService.updateLocation(user, location)
    }


}
