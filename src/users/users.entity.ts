import {
    BaseEntity,
    Column,
    PrimaryGeneratedColumn,
    Entity,
    JoinTable,
    OneToMany,
    ManyToOne,
    JoinColumn,
    OneToOne,
    Double,
} from 'typeorm';

import { Messages } from '../messages/messages.entity';
import { Reports } from '../reports/reports.entity';
import { Locations } from '../locations/locations.entity';
import { BlockList } from '../block-list/block-list.entity';

@Entity()
export class Users extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    fcmToken: string;

    @Column()
    webFCMToken: string;

    @Column()
    token: string;

    @Column()
    name: string;

    @Column()
    username: string;

    @Column()
    wallet: number;

    @Column()
    reportsCount: number;

    @Column()
    email: string;

    @Column()
    phone: string;

    @Column()
    avatar: string;

    @Column()
    job: string;

    @Column()
    about: string;

    @Column()
    city: string;

    @Column()
    district: string;

    @Column()
    storeId: number;

    @Column()
    seenCountBySelf: number;

    // cancel requests
    @Column()
    cancelCountWhenAccepted: number;

    // cancel requests
    @Column()
    cancelCountWhenSending: number;

    @Column()
    ordersDeliveredCount: number;

    @Column({ type: 'double precision', array: true })
    location: Double[];

    @Column('bool')
    isAccessiblePhone: boolean;

    @Column('bool')
    isAccessibleMarketing: boolean;

    @Column('bool')
    isHideNotifContent: boolean;

    @Column('bool')
    isAccessibleLocation: boolean;

    @Column('bool')
    isShowingLastOnline: boolean;

    @Column('bool')
    isAccessibleUnivocal: boolean;

    // 1 = male
    // 2 = female
    @Column()
    gender: number;

    @Column()
    dateLastOnline: Date;

    // first date of receiving the SMS
    @Column()
    dateFirstRegister: Date;

    @Column()
    dateCompleteRegister: Date;

    @Column()
    isOnline: boolean;

    // 1 = accepted
    // 2 = banned
    // 3 = unavailable by self
    @Column()
    status: number;

    @Column()
    seenCountByOthers: number;

    @Column()
    dateBlockReleased: Date;

    @Column()
    dateBlockCounterReset: Date;

    @Column()
    blockCount: number;

    @Column()
    blockCountInMonth: number;

    @Column()
    blockCountInDay: number;

    @Column()
    smsCode: number;

    // 1 = android
    // 2 = ios
    @Column()
    deviceType: number;

    @Column()
    appVersion: number;

    @Column()
    clientId: string;

    @OneToMany(
        type => Messages,
        message => message.senderUser,
    )
    sendMessages: Messages[];

    @OneToMany(
        type => Messages,
        message => message.getterUser,
    )
    getMessages: Messages[];

    // reports received
    // @OneToMany(type => Reports, report => report.user)
    // reports: Reports[];

    @OneToMany(
        type => Locations,
        location => location.user,
    )
    locations: Locations[];

    @Column()
    followersCount: number;

    @Column()
    followingCount: number;

    @Column()
    postsCount: number;
}
