import { UsersModule } from 'src/users/users.module';
import { Module } from '@nestjs/common';
import { SocietyMessagesService } from './society-messages.service';
import { SocietyMessagesController } from './society-messages.controller';

@Module({
  imports: [UsersModule],
  providers: [SocietyMessagesService],
  controllers: [SocietyMessagesController]
})
export class SocietyMessagesModule {}
