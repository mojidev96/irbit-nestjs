import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, Double, JoinColumn } from 'typeorm';

@Entity('societyMessages')
export class SocietyMessages extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userId: number;

    @Column()
    topicId: number;

    @Column()
    message: string;

    @Column()
    image: string;

    @Column()
    dateCreate: Date;

    @Column()
    ups: number;

    @Column()
    downs: number;

    @Column()
    replyCreatorName: string;

    @Column()
    replyCreatorId: number;

    @Column()
    replyImage: string;

    @Column()
    replyMessage: string;

    @Column()
    replyId: number;
}