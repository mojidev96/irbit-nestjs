import { SocietyTopics } from './../society-topics/society-topics.entity';
import { getManager } from 'typeorm';
import { SocietyMessages } from './society-messages.entity';
import { SocietyMesDTO } from './society-messages.dto';
import { Users } from 'src/users/users.entity';
import { Injectable, ConflictException, NotFoundException } from '@nestjs/common';

@Injectable()
export class SocietyMessagesService {

    async addNewMessage(user: Users, addDTO: SocietyMesDTO, filePath: string): Promise<any> {
        if (user.status != 1) {
            throw new ConflictException('حساب کاربری شما معلق می باشد.')
        }
        if (user.name && user.name.length > 0) {
            const newMes = new SocietyMessages()
            newMes.dateCreate = new Date()
            newMes.message = addDTO.message
            newMes.topicId = addDTO.topicId
            if (filePath) {
                newMes.image = filePath
            }
            newMes.userId = user.id

            if (String(addDTO.isReply) == 'true') {
                newMes.replyCreatorId = addDTO.replyCreatorId
                newMes.replyCreatorName = addDTO.replyCreatorName
                newMes.replyId = addDTO.replyId
                newMes.replyImage = addDTO.replyImage
                newMes.replyMessage = addDTO.replyMessage
            }

            await newMes.save()

            await getManager().update(SocietyTopics, { id: addDTO.topicId }, { messagesCount: () => '"messagesCount" + 1' })

            return newMes
        } else {
            throw new ConflictException('لطفا ابتدا نسبت به تکمیل اطلاعات حساب کاربری خود اقدام نمایید.')
        }
    }

    async deleteMessage(user: Users, id: number): Promise<any> {
        const mes = await getManager().findOne(SocietyMessages, { id, userId: user.id })
        if (!mes) {
            throw new NotFoundException('چنین پیامی از برای شما یافت نشد...')
        } else {
            await getManager().update(SocietyTopics, {id: mes.topicId}, {messagesCount: () => '"messagesCount" - 1'})
            await mes.remove()
            return { message: 'انجام شد.' }
        }
    }
}
