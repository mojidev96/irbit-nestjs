export class SocietyMesDTO {
    topicId: number
    message: string
    replyId: number
    replyCreatorId: number
    replyMessage: string
    replyImage: string
    replyCreatorName: string
    isReply: boolean
}