import { SocietyMesDTO } from './society-messages.dto';
import { SocietyMessagesService } from './society-messages.service';
import { SocietyTopicsService } from './../society-topics/society-topics.service';
import { Body, Controller, Post, UploadedFile, UseGuards, UseInterceptors, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { TopicDTO } from 'src/society-topics/society-topics.dto';
import { GetUser } from 'src/users/getuser.decorator';
import { Users } from 'src/users/users.entity';

@Controller('societyMessages')
export class SocietyMessagesController {

    constructor(private messagesService: SocietyMessagesService) {}

    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileInterceptor('image'))
    addNewMessage(
        @GetUser() user: Users,
        @Body() addDTO: SocietyMesDTO,
        @UploadedFile() file
    ): Promise<any>{
        return this.messagesService.addNewMessage(user, addDTO, file == null ? '' : file.path)
    }

    @Delete()
    @UseGuards(AuthGuard('jwt'))
    deleteMessage(
        @GetUser() user: Users,
        @Body('id') id: number,
    ): Promise<any>{
        return this.messagesService.deleteMessage(user, id)
    }

}
