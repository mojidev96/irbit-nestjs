import { UsersModule } from './../users/users.module';
import { Module } from '@nestjs/common';
import { FreePlanCodesService } from './free-plan-codes.service';
import { FreePlanCodesController } from './free-plan-codes.controller';

@Module({
  imports: [UsersModule],
  providers: [FreePlanCodesService],
  controllers: [FreePlanCodesController]
})
export class FreePlanCodesModule { }
