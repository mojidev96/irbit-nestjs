import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from 'typeorm';


@Entity('freePlanCodes')
export class FreePlanCodes extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string;

    @Column()
    useTimes: number;

    @Column('bool')
    isAvailable: boolean;

}