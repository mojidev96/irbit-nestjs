import { PaymentDTO } from './payments.dto';
import { Users } from './../users/users.entity';
import { AuthGuard } from '@nestjs/passport';
import { Controller, Post, UseGuards, Body, Get } from '@nestjs/common';
import { PaymentsService } from './payments.service';
import { GetUser } from 'src/users/getuser.decorator';

@Controller('payments')
export class PaymentsController {

    constructor(private paymentSevice: PaymentsService) { }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    pay(
        @GetUser() user: Users,
        @Body() payDTO: PaymentDTO
    ): Promise<any>{
        return this.paymentSevice.pay(user, payDTO)
    }

    @Post('list')
    @UseGuards(AuthGuard('jwt'))
    getMyPayments(
        @GetUser() user: Users,
        @Body('page') page: number
    ): Promise<any>{
        return this.paymentSevice.getMyPayments(user, page)
    }

}
