import { PaymentDTO } from './payments.dto';
import { getManager } from 'typeorm';
import { Payments } from './payments.entity';
import { Users } from './../users/users.entity';
import { Injectable, NotFoundException, ConflictException, Logger } from '@nestjs/common';

@Injectable()
export class PaymentsService {

    async pay(user: Users, payDTO: PaymentDTO) {
        const em = getManager()
        const datePaid = new Date()

        if (String(payDTO.isSuccess) == 'false') {
            const payment = new Payments()
            payment.amount = payDTO.amount
            payment.transCode = payDTO.transcode
            payment.isSuccess = payDTO.isSuccess
            payment.userId = user.id
            payment.datePaid = datePaid
            await payment.save()

            throw new ConflictException('پرداخت انجام نشده.')
        }

       
    }

    async getMyPayments(user: Users, page: number): Promise<any> {
        const payments = await getManager().createQueryBuilder(Payments, 'p')
            .where('p.userId = ' + user.id)
            .limit(10)
            .offset(10 * (page - 1))
            .getMany()
        return payments
    }

}
