import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('payments')
export class Payments extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    userId: number

    @Column()
    datePaid: Date

    @Column()
    amount: number

    @Column()
    transCode: string

    @Column('bool')
    isSuccess: boolean

}