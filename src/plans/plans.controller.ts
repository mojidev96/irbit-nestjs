import { PlansService } from './plans.service';
import { Body, Controller, Post } from '@nestjs/common';

@Controller('plans')
export class PlansController {

    constructor(private plansService: PlansService) {}

    @Post()
    getPlans(
        @Body('page') page: number,
        @Body('city') city: string,
        @Body('category') category: string,
        @Body('dateFilterIndex') dateFilterIndex: number,
    ): Promise<any>{
        return this.plansService.getPlans(page, city, category, dateFilterIndex)
    }

    @Post('byId')
    getPlanById(
        @Body('id') id: number,
        @Body('planerId') planerId: number
    ): Promise<any>{
        return this.plansService.getPlanById(id, planerId)
    }
}
