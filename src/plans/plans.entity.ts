import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn, Double } from 'typeorm';


@Entity()
export class Plans extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    poster: string;

    @Column()
    city: string;

    @Column()
    category: string;

    @Column()
    address: string

    @Column({ type: 'double precision', array: true })
    location: Double[]

    @Column()
    minMembers: number

    @Column()
    seenCount: number

    @Column()
    maxMembers: number;

    @Column()
    membersCount: number

    @Column()
    date: Date

    // minutes count
    @Column()
    planTime: number;

    @Column()
    ticketPrice: number;

    @Column()
    placeName: string;

}