import { UsersModule } from './../users/users.module';
import { Module } from '@nestjs/common';
import { PlansService } from './plans.service';
import { PlansController } from './plans.controller';

@Module({
  imports: [UsersModule],
  providers: [PlansService],
  controllers: [PlansController]
})
export class PlansModule {}
