import { Planers } from './../planers/planers.entity';
import { PlanParts } from './../plan-parts/planParts.entity';
import { Plans } from './plans.entity';
import { getManager } from 'typeorm';
import { Injectable, NotFoundException, Logger } from '@nestjs/common';

@Injectable()
export class PlansService {

    async getPlans(page: number, city: string, category: string, dateFilterIndex: number): Promise<any> {

        var dateWhere = 'true'
        switch (Number(dateFilterIndex)) {
            case 0: // امروز
                dateWhere = "p.date < Date(CURRENT_DATE) + 1 and p.date >= now()"
                break;
            case 1: // فردا
                dateWhere = "p.date <= Date(CURRENT_DATE) + 2 and p.date >= now()"
                break;
            case 2: // این هفته
                dateWhere = "p.date >= now() and p.date < Date(CURRENT_DATE) + 7"
                break;
            default:
                break;
        }

        const list = await getManager().createQueryBuilder(Plans, 'p')
            .select('p.id', 'id')
            .addSelect('p.title', 'title')
            .addSelect('p.description', 'description')
            .addSelect('p.poster', 'poster')
            .addSelect('p.city', 'city')
            .addSelect('p.category', 'category')
            .addSelect('p.address', 'address')
            .addSelect('p.location', 'location')
            .addSelect('p.minMembers', 'minMembers')
            .addSelect('p.maxMembers', 'maxMembers')
            .addSelect('p.membersCount', 'membersCount')
            .addSelect('p.date', 'date')
            .addSelect('p.planTime', 'planTime')
            .addSelect('p.ticketPrice', 'ticketPrice')
            .addSelect('p.placeName', 'placeName')
            .addSelect('p.date < now() as "isExpired"')
            .addSelect('(p.membersCount >= p.maxMembers)', 'isFull')
            .where("p.city = '" + city + "'")
            .andWhere("p.category ilike '%" + category + "%'")
            .andWhere(dateWhere)
            // .andWhere('p.date > Date(CURRENT_DATE) - 1')
            .andWhere('p.date >= now()')
            .orderBy('p.date', 'ASC')
            .offset(10 * (page - 1))
            .limit(10)
            .getRawMany()

        const planers = list.map(async plan => {
            plan.planers = await getManager().createQueryBuilder(PlanParts, 'pp')
                .leftJoin(Planers, 'p', 'pp.planerId = p.id')
                .select('p.id', 'id')
                .addSelect('p.name', 'name')
                .addSelect('p.avatar', 'avatar')
                .addSelect('p.about', 'about')
                .where('pp.planId = ' + plan.id + ' and pp.isPayed = true')
                .groupBy('p.id, p.name, p.avatar, p.about')
                .getRawMany()
        })

        await Promise.all(planers)

        return list
    }

    async getPlanById(id: number, planerId: number): Promise<any> {
        const plan = await getManager().createQueryBuilder(Plans, 'p')
            .select('p.id', 'id')
            .addSelect('p.title', 'title')
            .addSelect('p.description', 'description')
            .addSelect('p.poster', 'poster')
            .addSelect('p.city', 'city')
            .addSelect('p.category', 'category')
            .addSelect('p.address', 'address')
            .addSelect('p.location', 'location')
            .addSelect('p.minMembers', 'minMembers')
            .addSelect('p.maxMembers', 'maxMembers')
            .addSelect('p.membersCount', 'membersCount')
            .addSelect('p.date', 'date')
            .addSelect('p.planTime', 'planTime')
            .addSelect('p.ticketPrice', 'ticketPrice')
            .addSelect('p.placeName', 'placeName')
            .addSelect('p.date < now() as "isExpired"')
            .addSelect('(p.membersCount >= p.maxMembers)', 'isFull')
            .where('p.id = ' + id)
            .getRawOne()

        await getManager().update(Plans, { id }, { seenCount: () => '"seenCount" + 1' })

        const planers = await getManager().createQueryBuilder(PlanParts, 'pp')
            .leftJoin(Planers, 'p', 'pp.planerId = p.id')
            .select('p.id', 'id')
            .addSelect('p.name', 'name')
            .addSelect('p.avatar', 'avatar')
            .addSelect('p.about', 'about')
            .where('pp.planId = ' + plan.id + ' and pp.isPayed = true')
            .groupBy('p.id, p.name, p.avatar, p.about')
            .getRawMany()

        await Promise.all(planers)

        const buyDetail = await getManager().createQueryBuilder(PlanParts, 'pp')
            .where('pp.planerId = ' + planerId + ' and pp.planId = ' + plan.id)
            .getOne()
        const planer = await getManager().findOne(Planers, { id: planerId })
        if (!plan) {
            throw new NotFoundException('برنامه مورد نظر پیدا نشد...')
        } else {
            return { plan, planer, planers, isBought: buyDetail != null }
        }
    }

}

