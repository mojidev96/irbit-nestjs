import { Module } from '@nestjs/common';
import { TutorialCategoriesService } from './tutorial-categories.service';
import { TutorialCategoriesController } from './tutorial-categories.controller';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [UsersModule],
  providers: [TutorialCategoriesService],
  controllers: [TutorialCategoriesController]
})
export class TutorialCategoriesModule {}
