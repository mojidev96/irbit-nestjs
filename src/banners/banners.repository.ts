import { EntityRepository, Repository } from 'typeorm';
import { Banners } from 'src/banners/banners.entity';

@EntityRepository(Banners)
export class BannersRepository extends Repository<Banners> {

}
