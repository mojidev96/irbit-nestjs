import { Module } from '@nestjs/common';
import { BannersService } from './banners.service';
import { BannersController } from './banners.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BannersRepository } from './banners.repository';
import { Banners } from './banners.entity';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    UsersModule
  ],
  providers: [BannersService],
  controllers: [BannersController]
})
export class BannersModule { }
