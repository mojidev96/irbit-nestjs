import { Entity, BaseEntity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Banners extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column('bool')
    isMainGallery: boolean;

    @Column()
    imagePath: string;

    @Column()
    actionUrl: string;

    @Column()
    index: number;

    @Column({ type: 'text', array: true })
    cityNames: string[];

    @Column()
    clickCount: number;

    @Column()
    maxClickCount: number;

    @Column()
    dateCreate: Date;

    @Column()
    dateStart: Date;

    @Column()
    dateEnd: Date;

    // 24h
    @Column()
    startClock: number;

    // 24h
    @Column()
    endClock: number;
}