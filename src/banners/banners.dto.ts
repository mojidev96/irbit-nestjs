export class AddBannerDTO {
    isMainGallery: boolean
    actionUrl: string
    dateStart: Date
    dateEnd: Date
    maxClickCount: number
    cityNames: string[]
    startClock: number
    endClock: number
}