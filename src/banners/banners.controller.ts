import { Controller, Post, UseGuards, Body, UseInterceptors, UploadedFile, ConflictException, Delete } from '@nestjs/common';
import { BannersService } from './banners.service';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/users/getuser.decorator';
import { Users } from '../users/users.entity';
import { AddBannerDTO } from './banners.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('banners')
export class BannersController {

    constructor(private bannersService: BannersService) { }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(FileInterceptor('image'))
    addBanner(@GetUser() user: Users, @Body() addDTO: AddBannerDTO, @UploadedFile() file): Promise<{ message: string }> {
        if (!file) {
            throw new ConflictException('تصویر الزامیست.')
        }
        return this.bannersService.addBanner(user, addDTO, file == null ? '' : file.path)
    }

    @Delete()
    @UseGuards(AuthGuard('jwt'))
    deleteBannerById(@GetUser() user: Users, @Body('bannerId') bannerId: number): Promise<{ message: string }> {
        return this.bannersService.deleteBannerById(user, bannerId)
    }
}
