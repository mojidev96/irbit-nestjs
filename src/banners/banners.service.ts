import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AddBannerDTO } from './banners.dto';
import { Users } from '../users/users.entity';
import { getManager } from 'typeorm';
import { Admin } from '../admin/admin.entity';
import { Banners } from './banners.entity';

@Injectable()
export class BannersService {

    async addBanner(user: Users, addDTO: AddBannerDTO, imagePath: string): Promise<{ message: string }> {
        const admin = await getManager().findOne(Admin, { phone: user.phone })
        if (admin) {
            const banner = new Banners()
            banner.dateCreate = new Date()
            banner.dateStart = addDTO.dateStart
            banner.dateEnd = addDTO.dateEnd
            banner.startClock = addDTO.startClock
            banner.endClock = addDTO.endClock
            banner.isMainGallery = addDTO.isMainGallery
            banner.actionUrl = addDTO.actionUrl
            banner.imagePath = imagePath
            banner.maxClickCount = addDTO.maxClickCount
            banner.cityNames = addDTO.cityNames

            await banner.save()
        } else {
            throw new UnauthorizedException('کاربر مورد نظر دسترسی لازم را ندارد.')
        }
        return { message: 'انجام شد.' }
    }

    async deleteBannerById(user: Users, bannerId: number): Promise<{ message: string }> {
        const admin = await getManager().findOne(Admin, { phone: user.phone })
        if (admin) {
            await getManager().delete(Banners, { id: bannerId })
        } else {
            throw new UnauthorizedException('کاربر مورد نظر دسترسی لازم را ندارد.')
        }
        return { message: 'انجام شد.' }
    }

}
