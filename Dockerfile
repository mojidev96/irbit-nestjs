FROM keymetrics/pm2:10-alpine
MAINTAINER irancoolkids@gmail.com

RUN apk update && apk add tzdata && \
    rm -rf /etc/localtime && \
    cp /usr/share/zoneinfo/Asia/Tehran /etc/localtime && \
    echo "Asia/Tehran" >  /etc/timezone

WORKDIR /forusha

COPY package.json /forusha
RUN npm install
COPY ./dist /forusha
COPY process.json /forusha
VOLUME /forusha/files
VOLUME /forusha/legalFiles
VOLUME /forusha/messagesFiles
VOLUME /forusha/markets

EXPOSE 3000
CMD ["pm2-runtime", "process.json"]